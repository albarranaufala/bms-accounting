<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bank_id')->nullable();
            $table->date('date')->nullable();
            $table->text('description')->nullable();
            $table->string('invoice_num')->nullable();
            $table->bigInteger('bill_value')->nullable();
            $table->bigInteger('payment_value')->nullable();
            $table->text('information')->nullable();
            $table->foreignId('debit_account_id')->nullable();
            $table->foreignId('credit_account_id')->nullable();
            $table->foreignId('company_id')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashes');
    }
}
