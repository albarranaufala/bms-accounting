<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(! User::role('superadmin')->first())
        {
            $superAdmin = User::create([
                'name' => 'superadmin',
                'username' => 'superadmin',
                'email' => 'superadmin@bms.com',
                'password' => Hash::make(123123123)
            ]);
            $superAdmin->assignRole('superadmin');

            $superVisor = User::create([
                'name' => 'Supervisor',
                'username' => 'supervisor',
                'email' => 'supervisor@bms.com',
                'password' => Hash::make(123123123),
                'company_id' => 1,
            ]);
            $superVisor->assignRole('supervisor');
        }
    }
}
