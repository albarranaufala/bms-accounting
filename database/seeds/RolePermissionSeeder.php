<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = Role::findOrCreate('superadmin');
        $supervisor = Role::findOrCreate('supervisor');
        $piutang = Role::findOrCreate('Divisi Piutang');
        $hutang = Role::findOrCreate('Divisi Hutang');
        $kas = Role::findOrCreate('Divisi Kas');
        $akuntansi = Role::findOrCreate('Divisi Akuntansi');

        $userPermissions = [];
        $piutangPermissions = [];
        $hutangPermissions = [];
        $kasPermissions = [];
        $akuntansiPermissions = [];

        $userPermissions[] = Permission::findOrCreate('Lihat Users');
        $userPermissions[] = Permission::findOrCreate('Edit Users');

        $hutangPermissions[] = Permission::findOrCreate('Lihat Hutang');
        $hutangPermissions[] = Permission::findOrCreate('Edit Hutang');

        $piutangPermissions[] = Permission::findOrCreate('Lihat Piutang');
        $piutangPermissions[] = Permission::findOrCreate('Edit Piutang');

        $kasPermissions[] = Permission::findOrCreate('Lihat Kas');
        $kasPermissions[] = Permission::findOrCreate('Edit Kas');

        $akuntansiPermissions[] = Permission::findOrCreate('Lihat Akuntansi');
        $akuntansiPermissions[] = Permission::findOrCreate('Edit Akuntansi');

        $companyPermissions[] = Permission::findOrCreate('Lihat Perusahaan Mitra');
        $companyPermissions[] = Permission::findOrCreate('Edit Perusahaan Mitra');

        $superadmin->syncPermissions($userPermissions);
        $supervisor->syncPermissions([$userPermissions,$hutangPermissions,$piutangPermissions,$kasPermissions,$akuntansiPermissions]);
        $piutang->syncPermissions($piutangPermissions);
        $hutang->syncPermissions($hutangPermissions);
        $kas->syncPermissions($kasPermissions);
        $akuntansi->syncPermissions($akuntansiPermissions);
    }
}
