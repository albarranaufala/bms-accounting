@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-cog"></i> Ganti Password</h1>
        </div>

        <div class="row">
            <div class="col-md-6">
                <form action="{{ route('users.update.password', ['user' => Auth::user()]) }}" class="card" method="POST">
                    @csrf @method('PUT')
                    <div class="card-body">
                        <x-inputs.input
                            required
                            label="Password Lama"
                            type="password"
                            placeholder="Masukkan Password Lama"
                            id="current_password"
                            name="current_password"
                        />
                        <x-inputs.input
                            required
                            label="Password Baru"
                            type="password"
                            placeholder="Masukkan Password Baru"
                            id="password"
                            name="password"
                        />
                        <x-inputs.input
                            required
                            label="Konfirmasi Password Baru"
                            type="password"
                            placeholder="Konfirmasi Password Baru"
                            id="password_confirmation"
                            name="password_confirmation"
                        />
                        <button class="btn btn-primary float-right">
                            Ganti Password
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection