@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Laporan Keuangan</h1>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <a class="card mb-3 card-link shadow-sm" href="{{route('financial_statements.show', 'balance_sheet')}}" data-toggle="tooltip" data-placement="bottom" title="Laporan keuangan yang menyajikan aset, kewajiban, dan ekuitas.">
                    <div class="card-body">
                        <h2 class="h6 font-weight-bold text-gray-500 mb-2">Neraca</h2>
                        <p class="mb-0 text-truncate">Laporan keuangan yang menyajikan aset, kewajiban, dan ekuitas.</p>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <a class="card mb-3 card-link shadow-sm" href="{{route('financial_statements.show', 'income_statement')}}" data-toggle="tooltip" data-placement="bottom" title="Laporan keuangan yang menyajikan pendapatan dan biaya perusahaan.">
                    <div class="card-body">
                        <h2 class="h6 font-weight-bold text-gray-500 mb-2">Laba dan Rugi</h2>
                        <p class="mb-0 text-truncate">Laporan keuangan yang menyajikan pendapatan dan biaya perusahaan.</p>
                    </div>
                </a>
            </div>
{{--            <div class="col-12 col-md-6 col-lg-4">--}}
{{--                <a class="card mb-3 card-link shadow-sm" href="{{route('financial_statements.show', 'income_statement')}}" data-toggle="tooltip" data-placement="bottom" title="Kertas kerja yang berisi dari saldo awal, saldo jurnal, dll.">--}}
{{--                    <div class="card-body">--}}
{{--                        <h2 class="h6 font-weight-bold text-gray-500 mb-2">Neraca Lajur</h2>--}}
{{--                        <p class="mb-0 text-truncate">Kertas kerja yang berisi dari saldo awal, saldo jurnal, dll.</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
        </div>

        <hr class="my-5">

{{--        JURNAL UMUM--}}
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Jurnal Umum</h1>
        </div>

        <div class="mb-4 d-flex flex-column align-items-end flex-md-row justify-content-md-between align-items-md-center">
            <div class="align-self-start">
                <x-period-filter :route="route('financial_statements.index')" />
            </div>
            <div class="mt-3 mt-md-0 d-flex">
                <x-btn-export :route="route('financial_statements.general_entries.export.excel')"/>
            </div>
        </div>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Uraian</th>
                            <th class="text-right">Jumlah</th>
                            <th class="text-center">Debit</th>
                            <th class="text-center">Kredit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($transactions as $transaction)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    {{(new Carbon\Carbon($transaction->date))->format('d M Y')}}
                                </td>
                                <td>{{$transaction->description ?? '-'}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{($transaction->bill_value ?? $transaction->payment_value) ?? 0}}</span></td>
                                <td>{{$transaction->debitAccount->name}}</td>
                                <td>{{$transaction->creditAccount->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
