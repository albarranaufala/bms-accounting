@extends('layouts.dashboard')

@section('dashboard-content')
<div class="p-4">
    <!-- Page Heading -->
    <div class="mb-4 d-flex align-items-center justify-content-between">
        <h1 class="h3 text-primary font-weight-bold mb-0">Laporan Neraca</h1>
    </div>

    <div class="mb-4 d-flex flex-column align-items-end flex-md-row justify-content-md-between align-items-md-center">
        <div class="align-self-start">
            <x-period-filter :route="route('financial_statements.show', 'balance_sheet')"></x-period-filter>
        </div>
        <div class="mt-3 mt-md-0 d-flex">
            <x-btn-export :route="route('financial_statements.balance_sheet.export.excel')" class="mr-1"></x-btn-export>
        </div>
    </div>

    @if ($sumTotalBalanceAsset != $sumTotalBalanceLiabilityAndEquity)
    <div class="mb-4">
        <div class="alert alert-warning" role="alert">
            Total Aset <b>tidak sama</b> dengan Total Kewajiban + Ekuitas!
        </div>
    </div>
    @endif

    <div class="card shadow-sm">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr class="text-uppercase small text-primary">
                        <th>Keterangan</th>
                        <th>Awal Periode</th>
                        <th>Periode Berjalan</th>
                        <th>Akhir Periode</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="font-weight-bold" colspan="4">ASET</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$currentAssets->name}}</td>
                    </tr>
                    @foreach ($currentAssets->accounts as $curAsset)
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$curAsset->name}}</td>
                        </tr>
                        @foreach ($curAsset->accounts as $curAss)
                            <tr>
                                <td>{{$curAss->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$curAss->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$curAss->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$curAss->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceCurrentAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceCurrentAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceCurrentAsset}}</span></td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$fixedAssets->name}}</td>
                    </tr>
                    @foreach ($fixedAssets->accounts as $fixAsset)
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$fixAsset->name}}</td>
                        </tr>
                        @foreach ($fixAsset->accounts as $fixAss)
                            <tr>
                                <td>{{$fixAss->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$fixAss->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$fixAss->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$fixAss->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceFixedAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceFixedAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceFixedAsset}}</span></td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$otherAssets->name}}</td>
                    </tr>
                    @foreach ($otherAssets->accounts as $othAsset)
                        <tr>
                            <td>{{$othAsset->name}}</td>
                            <td class="text-right">Rp <span class="mask-money">{{$othAsset->previousBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$othAsset->currentBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$othAsset->totalBalance()}}</span></td>
                        </tr>
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceOtherAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceOtherAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceOtherAsset}}</span></td>
                    </tr>
                    <tr class="font-weight-bold">
                        <td class="text-right">TOTAL ASET</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceAsset}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceAsset}}</span></td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold" colspan="4">KEWAJIBAN</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$currentLiabilities->name}}</td>
                    </tr>
                    @foreach ($currentLiabilities->accounts as $curLiab)
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$curLiab->name}}</td>
                        </tr>
                        @foreach ($curLiab->accounts as $curLi)
                            <tr>
                                <td>{{$curLi->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$curLi->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$curLi->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$curLi->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceCurrentLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceCurrentLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceCurrentLiability}}</span></td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$longTermLiabilities->name}}</td>
                    </tr>
                    @foreach ($longTermLiabilities->accounts as $ltLiab)
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$ltLiab->name}}</td>
                        </tr>
                        @foreach ($ltLiab->accounts as $ltLi)
                            <tr>
                                <td>{{$ltLi->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$ltLi->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$ltLi->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$ltLi->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceLongTermLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceLongTermLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceLongTermLiability}}</span></td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$otherLiabilities->name}}</td>
                    </tr>
                    @foreach ($otherLiabilities->accounts as $otherLiab)
                        <tr>
                            <td>{{$otherLiab->name}}</td>
                            <td class="text-right">Rp <span class="mask-money">{{$otherLiab->previousBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$otherLiab->currentBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$otherLiab->totalBalance()}}</span></td>
                        </tr>
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceOtherLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceOtherLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceOtherLiability}}</span></td>
                    </tr>
                    <tr class="font-weight-bold">
                        <td class="text-right">TOTAL KEWAJIBAN</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceLiability}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceLiability}}</span></td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold" colspan="4">{{$equity->name}}</td>
                    </tr>
                    @foreach ($equity->accounts as $eq)
                        <tr>
                            <td>{{$eq->name}}</td>
                            <td class="text-right">Rp <span class="mask-money">{{$eq->previousBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$eq->currentBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$eq->totalBalance()}}</span></td>
                        </tr>
                    @endforeach
                    <tr class="font-weight-bold">
                        <td class="text-right">Jumlah</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceEquity}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceEquity}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceEquity}}</span></td>
                    </tr>
                    <tr class="font-weight-bold">
                        <td class="text-right">TOTAL KEWAJIBAN DAN EKUITAS</td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceLiabilityAndEquity}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceLiabilityAndEquity}}</span></td>
                        <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceLiabilityAndEquity}}</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
