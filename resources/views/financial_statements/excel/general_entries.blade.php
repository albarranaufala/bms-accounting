<table>
    <tbody>
    <tr>
        <td colspan="7">
            <strong>Jurnal Umum Akuntansi</strong>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <strong>{{$rangeDate}}</strong>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <td>No.</td>
        <td>Tanggal</td>
        <td>Uraian</td>
        <td>Jumlah</td>
        <td>Debit</td>
        <td>Kredit</td>
    </tr>
    @foreach ($transactions as $transaction)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{(new Carbon\Carbon($transaction->date))->format('d M Y')}}</td>
            <td>{{$transaction->description ?? '-'}}</td>
            <td>{{($transaction->bill_value ?? $transaction->payment_value) ?? 0}}</td>
            <td>{{$transaction->debitAccount->name}}</td>
            <td>{{$transaction->creditAccount->name}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
