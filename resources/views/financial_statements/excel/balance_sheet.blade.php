<table>
    <tr>
        <td colspan="7">
            <strong>Laporan Neraca</strong>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <strong>{{$rangeDate}}</strong>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <th>Keterangan</th>
        <th>Awal Periode</th>
        <th>Periode Berjalan</th>
        <th>Akhir Periode</th>
    </tr>
    <tr>
        <td colspan="4">ASET</td>
    </tr>
    <tr>
        <td colspan="4">{{$data['currentAssets']->name}}</td>
    </tr>
    @foreach ($data['currentAssets']->accounts as $curAsset)
        <tr>
            <td class="font-weight-bold" colspan="4">{{$curAsset->name}}</td>
        </tr>
        @foreach ($curAsset->accounts as $curAss)
            <tr>
                <td>{{$curAss->name}}</td>
                <td class="text-right">{{$curAss->previousBalance()}}</td>
                <td class="text-right">{{$curAss->currentBalance()}}</td>
                <td class="text-right">{{$curAss->totalBalance()}}</td>
            </tr>
        @endforeach
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceCurrentAsset']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceCurrentAsset']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceCurrentAsset']}}</td>
    </tr>

    <tr>
        <td class="font-weight-bold" colspan="4">{{$data['fixedAssets']->name}}</td>
    </tr>
    @foreach ($data['fixedAssets']->accounts as $fixAsset)
        <tr>
            <td class="font-weight-bold" colspan="4">{{$fixAsset->name}}</td>
        </tr>
        @foreach ($fixAsset->accounts as $fixAss)
            <tr>
                <td>{{$fixAss->name}}</td>
                <td class="text-right">{{$fixAss->previousBalance()}}</td>
                <td class="text-right">{{$fixAss->currentBalance()}}</td>
                <td class="text-right">{{$fixAss->totalBalance()}}</td>
            </tr>
        @endforeach
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceFixedAsset']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceFixedAsset']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceFixedAsset']}}</td>
    </tr>

    <tr>
        <td class="font-weight-bold" colspan="4">{{$data['otherAssets']->name}}</td>
    </tr>
    @foreach ($data['otherAssets']->accounts as $othAsset)
        <tr>
            <td>{{$othAsset->name}}</td>
            <td class="text-right">{{$othAsset->previousBalance()}}</td>
            <td class="text-right">{{$othAsset->currentBalance()}}</td>
            <td class="text-right">{{$othAsset->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceOtherAsset']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceOtherAsset']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceOtherAsset']}}</td>
    </tr>
    <tr class="font-weight-bold">
        <td class="text-right">TOTAL ASET</td>
        <td class="text-right">{{$data['sumPreviousBalanceAsset']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceAsset']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceAsset']}}</td>
    </tr>

    <tr>
        <td class="font-weight-bold" colspan="4">KEWAJIBAN</td>
    </tr>
    <tr>
        <td class="font-weight-bold" colspan="4">{{$data['currentLiabilities']->name}}</td>
    </tr>
    @foreach ($data['currentLiabilities']->accounts as $curLiab)
        <tr>
            <td class="font-weight-bold" colspan="4">{{$curLiab->name}}</td>
        </tr>
        @foreach ($curLiab->accounts as $curLi)
            <tr>
                <td>{{$curLi->name}}</td>
                <td class="text-right">{{$curLi->previousBalance()}}</td>
                <td class="text-right">{{$curLi->currentBalance()}}</td>
                <td class="text-right">{{$curLi->totalBalance()}}</td>
            </tr>
        @endforeach
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceCurrentLiability']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceCurrentLiability']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceCurrentLiability']}}</td>
    </tr>

    <tr>
        <td class="font-weight-bold" colspan="4">{{$data['longTermLiabilities']->name}}</td>
    </tr>
    @foreach ($data['longTermLiabilities']->accounts as $ltLiab)
        <tr>
            <td class="font-weight-bold" colspan="4">{{$ltLiab->name}}</td>
        </tr>
        @foreach ($ltLiab->accounts as $ltLi)
            <tr>
                <td>{{$ltLi->name}}</td>
                <td class="text-right">{{$ltLi->previousBalance()}}</td>
                <td class="text-right">{{$ltLi->currentBalance()}}</td>
                <td class="text-right">{{$ltLi->totalBalance()}}</td>
            </tr>
        @endforeach
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceLongTermLiability']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceLongTermLiability']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceLongTermLiability']}}</td>
    </tr>

    <tr>
        <td class="font-weight-bold" colspan="4">{{$data['otherLiabilities']->name}}</td>
    </tr>
    @foreach ($data['otherLiabilities']->accounts as $otherLiab)
        <tr>
            <td>{{$otherLiab->name}}</td>
            <td class="text-right">{{$otherLiab->previousBalance()}}</td>
            <td class="text-right">{{$otherLiab->currentBalance()}}</td>
            <td class="text-right">{{$otherLiab->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceOtherLiability']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceOtherLiability']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceOtherLiability']}}</td>
    </tr>
    <tr class="font-weight-bold">
        <td class="text-right">TOTAL KEWAJIBAN</td>
        <td class="text-right">{{$data['sumPreviousBalanceLiability']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceLiability']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceLiability']}}</td>
    </tr>

    <tr>
        <td class="font-weight-bold" colspan="4">{{$data['equity']->name}}</td>
    </tr>
    @foreach ($data['equity']->accounts as $eq)
        <tr>
            <td>{{$eq->name}}</td>
            <td class="text-right">{{$eq->previousBalance()}}</td>
            <td class="text-right">{{$eq->currentBalance()}}</td>
            <td class="text-right">{{$eq->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr class="font-weight-bold">
        <td class="text-right">Jumlah</td>
        <td class="text-right">{{$data['sumPreviousBalanceEquity']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceEquity']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceEquity']}}</td>
    </tr>
    <tr class="font-weight-bold">
        <td class="text-right">TOTAL KEWAJIBAN DAN EKUITAS</td>
        <td class="text-right">{{$data['sumPreviousBalanceLiabilityAndEquity']}}</td>
        <td class="text-right">{{$data['sumCurrentBalanceLiabilityAndEquity']}}</td>
        <td class="text-right">{{$data['sumTotalBalanceLiabilityAndEquity']}}</td>
    </tr>
</table>
