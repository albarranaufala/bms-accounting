<table>
    <tr>
        <td colspan="7">
            <strong>Laporan Laba Rugi</strong>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <strong>{{$rangeDate}}</strong>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <th>Keterangan</th>
        <th>Awal Periode</th>
        <th>Periode Berjalan</th>
        <th>Akhir Periode</th>
    </tr>
{{--                        Pendapatan Usaha--}}
    <tr>
        <td colspan="4">{{$data['operatingRevenue']->name}}</td>
    </tr>
    @foreach ($data['operatingRevenue']->accounts as $opRevenue)
        <tr>
            <td>{{$opRevenue->name}}</td>
            <td>{{$opRevenue->previousBalance()}}</td>
            <td>{{$opRevenue->currentBalance()}}</td>
            <td>{{$opRevenue->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr>
        <td>Jumlah</td>
        <td>{{$data['sumPreviousBalanceOperatingRevenue']}}</td>
        <td>{{$data['sumCurrentBalanceOperatingRevenue']}}</td>
        <td>{{$data['sumTotalBalanceOperatingRevenue']}}</td>
    </tr>
{{--                        Biaya Langsung--}}
    <tr>
        <td colspan="4">{{$data['directCost']->name}}</td>
    </tr>
    @foreach ($data['directCost']->accounts as $dirCost)
        <tr>
            <td>{{$dirCost->name}}</td>
            <td>{{$dirCost->previousBalance()}}</td>
            <td>{{$dirCost->currentBalance()}}</td>
            <td>{{$dirCost->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr>
        <td>Jumlah</td>
        <td>{{$data['sumPreviousBalanceDirectCost']}}</td>
        <td>{{$data['sumCurrentBalanceDirectCost']}}</td>
        <td>{{$data['sumTotalBalanceDirectCost']}}</td>
    </tr>
{{--                        Laba Kotor--}}
    <tr>
        <td>LABA (RUGI) KOTOR</td>
        <td>{{$data['previousGrossProfit']}}</td>
        <td>{{$data['currentGrossProfit']}}</td>
        <td>{{$data['totalGrossProfit']}}</td>
    </tr>
{{--                        Biaya Umum--}}
    <tr>
        <td colspan="4">{{$data['generalCost']->name}}</td>
    </tr>
    @foreach ($data['generalCost']->accounts as $genCost)
        <tr>
            <td>{{$genCost->name}}</td>
            <td>{{$genCost->previousBalance()}}</td>
            <td>{{$genCost->currentBalance()}}</td>
            <td>{{$genCost->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr>
        <td>Jumlah</td>
        <td>{{$data['sumPreviousBalanceGeneralCost']}}</td>
        <td>{{$data['sumCurrentBalanceGeneralCost']}}</td>
        <td>{{$data['sumTotalBalanceGeneralCost']}}</td>
    </tr>
{{--                        Laba Usaha--}}
    <tr>
        <td>LABA (RUGI) USAHA</td>
        <td>{{$data['previousOperatingProfit']}}</td>
        <td>{{$data['currentOperatingProfit']}}</td>
        <td>{{$data['totalOperatingProfit']}}</td>
    </tr>
{{--                        Pendapatan Lain--}}
    <tr>
        <td colspan="4">{{$data['otherRevenue']->name}}</td>
    </tr>
    @foreach ($data['otherRevenue']->accounts as $othRev)
        <tr>
            <td>{{$othRev->name}}</td>
            <td>{{$othRev->previousBalance()}}</td>
            <td>{{$othRev->currentBalance()}}</td>
            <td>{{$othRev->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr>
        <td>Jumlah</td>
        <td>{{$data['sumPreviousBalanceOtherRevenue']}}</td>
        <td>{{$data['sumCurrentBalanceOtherRevenue']}}</td>
        <td>{{$data['sumTotalBalanceOtherRevenue']}}</td>
    </tr>
{{--                        Biaya Lain--}}
    <tr>
        <td colspan="4">{{$data['otherCost']->name}}</td>
    </tr>
    @foreach ($data['otherCost']->accounts as $othCost)
        <tr>
            <td>{{$othCost->name}}</td>
            <td>{{$othCost->previousBalance()}}</td>
            <td>{{$othCost->currentBalance()}}</td>
            <td>{{$othCost->totalBalance()}}</td>
        </tr>
    @endforeach
    <tr>
        <td>Jumlah</td>
        <td>{{$data['sumPreviousBalanceOtherCost']}}</td>
        <td>{{$data['sumCurrentBalanceOtherCost']}}</td>
        <td>{{$data['sumTotalBalanceOtherCost']}}</td>
    </tr>
{{--                        Laba Sebelum Pajak--}}
    <tr>
        <td>LABA (RUGI) SEBELUM PAJAK</td>
        <td>{{$data['previousProfitBeforeTax']}}</td>
        <td>{{$data['previousProfitBeforeTax']}}</td>
        <td>{{$data['previousProfitBeforeTax']}}</td>
    </tr>
{{--                        Pajak--}}
    <tr>
        <td>{{$data['tax']->name}}</td>
        <td>{{$data['tax']->previousBalance()}}</td>
        <td>{{$data['tax']->currentBalance()}}</td>
        <td>{{$data['tax']->totalBalance()}}</td>
    </tr>
{{--                        Laba Setelah Pajak--}}
    <tr>
        <td>LABA (RUGI) SETELAH PAJAK</td>
        <td>{{$data['previousProfitAfterTax']}}</td>
        <td>{{$data['previousProfitAfterTax']}}</td>
        <td>{{$data['previousProfitAfterTax']}}</td>
    </tr>
</table>