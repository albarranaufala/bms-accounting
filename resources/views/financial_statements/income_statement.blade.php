@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Laporan Laba dan Rugi</h1>
        </div>

        <div class="mb-4 d-flex flex-column align-items-end flex-md-row justify-content-md-between align-items-md-center">
            <div class="align-self-start">
                <x-period-filter :route="route('financial_statements.show', 'income_statement')"></x-period-filter>
            </div>
            <div class="mt-3 mt-md-0 d-flex">
                <x-btn-export :route="route('financial_statements.income_statement.export.excel')" class="mr-1"></x-btn-export>
            </div>
        </div>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" width="100%" cellspacing="0">
                        <thead>
                        <tr class="text-uppercase small text-primary">
                            <th>Keterangan</th>
                            <th>Awal Periode</th>
                            <th>Periode Berjalan</th>
                            <th>Akhir Periode</th>
                        </tr>
                        </thead>
                        <tbody>
{{--                        Pendapatan Usaha--}}
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$operatingRevenue->name}}</td>
                        </tr>
                        @foreach ($operatingRevenue->accounts as $opRevenue)
                            <tr>
                                <td>{{$opRevenue->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$opRevenue->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$opRevenue->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$opRevenue->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                        <tr class="font-weight-bold">
                            <td class="text-right">Jumlah</td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceOperatingRevenue}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceOperatingRevenue}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceOperatingRevenue}}</span></td>
                        </tr>
{{--                        Biaya Langsung--}}
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$directCost->name}}</td>
                        </tr>
                        @foreach ($directCost->accounts as $dirCost)
                            <tr>
                                <td>{{$dirCost->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$dirCost->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$dirCost->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$dirCost->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                        <tr class="font-weight-bold">
                            <td class="text-right">Jumlah</td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceDirectCost}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceDirectCost}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceDirectCost}}</span></td>
                        </tr>
{{--                        Laba Kotor--}}
                        <tr class="font-weight-bold">
                            <td class="text-right">LABA (RUGI) KOTOR</td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousGrossProfit}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$currentGrossProfit}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$totalGrossProfit}}</span></td>
                        </tr>
{{--                        Biaya Umum--}}
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$generalCost->name}}</td>
                        </tr>
                        @foreach ($generalCost->accounts as $genCost)
                            <tr>
                                <td>{{$genCost->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$genCost->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$genCost->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$genCost->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                        <tr class="font-weight-bold">
                            <td class="text-right">Jumlah</td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceGeneralCost}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceGeneralCost}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceGeneralCost}}</span></td>
                        </tr>
{{--                        Laba Usaha--}}
                        <tr class="font-weight-bold">
                            <td class="text-right">LABA (RUGI) USAHA</td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousOperatingProfit}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$currentOperatingProfit}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$totalOperatingProfit}}</span></td>
                        </tr>
{{--                        Pendapatan Lain--}}
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$otherRevenue->name}}</td>
                        </tr>
                        @foreach ($otherRevenue->accounts as $othRev)
                            <tr>
                                <td>{{$othRev->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$othRev->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$othRev->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$othRev->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                        <tr class="font-weight-bold">
                            <td class="text-right">Jumlah</td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceOtherRevenue}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceOtherRevenue}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceOtherRevenue}}</span></td>
                        </tr>
{{--                        Biaya Lain--}}
                        <tr>
                            <td class="font-weight-bold" colspan="4">{{$otherCost->name}}</td>
                        </tr>
                        @foreach ($otherCost->accounts as $othCost)
                            <tr>
                                <td>{{$othCost->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$othCost->previousBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$othCost->currentBalance()}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$othCost->totalBalance()}}</span></td>
                            </tr>
                        @endforeach
                        <tr class="font-weight-bold">
                            <td class="text-right">Jumlah</td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumPreviousBalanceOtherCost}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumCurrentBalanceOtherCost}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$sumTotalBalanceOtherCost}}</span></td>
                        </tr>
{{--                        Laba Sebelum Pajak--}}
                        <tr class="font-weight-bold">
                            <td class="text-right">LABA (RUGI) SEBELUM PAJAK</td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousProfitBeforeTax}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousProfitBeforeTax}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousProfitBeforeTax}}</span></td>
                        </tr>
{{--                        Pajak--}}
                        <tr class="font-weight-bold">
                            <td class="text-right">{{$tax->name}}</td>
                            <td class="text-right">Rp <span class="mask-money">{{$tax->previousBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$tax->currentBalance()}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$tax->totalBalance()}}</span></td>
                        </tr>
{{--                        Laba Setelah Pajak--}}
                        <tr class="font-weight-bold">
                            <td class="text-right">LABA (RUGI) SETELAH PAJAK</td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousProfitAfterTax}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousProfitAfterTax}}</span></td>
                            <td class="text-right">Rp <span class="mask-money">{{$previousProfitAfterTax}}</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
