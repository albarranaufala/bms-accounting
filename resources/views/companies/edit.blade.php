@extends('layouts.dashboard')

@section('dashboard-content')
<div class="p-4">
     <!-- Page Heading -->
     <div class="mb-4 d-flex align-items-center justify-content-between">
        <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-th-large"></i> {{ $company->name }}</h1>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card mb-4">
                <form action="{{ route('companies.update', ['company' => $company ]) }}" method="POST">
                    @csrf @method('PUT')
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Edit Data Perusahaan</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <x-inputs.input
                                    required
                                    label="Nama"
                                    name="name"
                                    value="{{ old('name',$company->name) }}"
                                    placeholder="Nama Perusahaan Mitra"
                                />
                                <x-inputs.input
                                    disabled
                                    label="Jenis Perusahaan"
                                    name="company_type"
                                    value="{{ $company->company_type }}"
                                />
                                <button class="btn btn-primary">Simpan Perubahan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @role('superadmin')
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-cogs"></i> Supervisor di {{ $company->name }}</h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#createSupervisorModal">
                <i class="fas fa-plus"></i>
                <span>Tambah Supervisor</span>
            </button>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover" id="supervisorTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($supervisors as $spv)
                                        <tr>
                                            <td>{{ $spv->id }}</td>
                                            <td>{{ $spv->name }}</td>
                                            <td>{{ $spv->username }}</td>
                                            <td>{{ $spv->email }}</td>
                                            <td class="text-center">
                                                <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v text-gray-500"></i>
                                                </a>
                                                <!-- Dropdown - User Information -->
                                                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                                <button class="dropdown-item" data-toggle="modal" data-target="#supervisorEditModal">
                                                    <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                                    Edit Data
                                                </button>
                                                <form action="{{ route('users.destroy', ['user' => $spv->id]) }}" method="POST">
                                                    @csrf @method('DELETE')
                                                    <button class="dropdown-item">
                                                        <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endrole()

    {{-- @role('supervisor')
    <div class="my-4 d-flex align-items-center justify-content-between">
        <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-users"></i> User di {{ $company->name }}</h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Divisi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->roles->map(function ($role){return $role->name;})->implode(', ') }}</td>
                                        <td class="text-center">
                                            <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v text-gray-500"></i>
                                            </a>
                                            <!-- Dropdown - User Information -->
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                            <a class="dropdown-item" href="{{ route('users.edit', ['user' => $user]) }}">
                                                <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Lihat User
                                            </a>
                                            <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST">
                                                @csrf @method('DELETE')
                                                <button class="dropdown-item">
                                                    <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endrole --}}
</div>
@endsection

@push('modals')
    {{-- Supervisor Edit Modal --}}
    <div class="modal fade" id="supervisorEditModal" tabindex="-1" role="dialog" aria-labelledby="supervisorEditModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{ route('users.update') }}" method="POST" class="modal-content">
                @csrf @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Akses Supervisor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <x-inputs.input
                        name="role_id"
                        value="2"
                        hidden
                    />
                    <x-inputs.input
                        name="user_id"
                        id="supervisor_user_id"
                        hidden
                    />
                    <x-inputs.input
                        label="Nama"
                        name="name"
                        id="supervisor_name"
                        placeholder="Masukkan nama superadmin"
                        required
                    />
                    <x-inputs.input
                        label="Username"
                        name="username"
                        id="supervisor_username"
                        placeholder="Masukkan username superadmin"
                        required
                    />
                    <x-inputs.input
                        required
                        label="Email"
                        name="email"
                        id="supervisor_email"
                        type="email"
                        placeholder="Masukkan email Superadmin"
                    />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>

    {{-- Create Supervisor Modal --}}
    <div class="modal fade" id="createSupervisorModal" tabindex="-1" role="dialog" aria-labelledby="createSupervisorModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{ route('users.store') }}" method="POST" class="modal-content">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Supervisor Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <x-inputs.input
                        name="role_id"
                        value="2"
                        hidden
                    />
                    <x-inputs.input
                        name="company_id"
                        value="{{ $company->id }}"
                        hidden
                    />
                    <x-inputs.input
                        label="Nama"
                        name="name"
                        placeholder="Masukkan Nama Supervisor"
                        required
                    />
                    <x-inputs.input
                        label="Username"
                        name="username"
                        placeholder="Masukkan Username Supervisor"
                        required
                    />
                    <x-inputs.input
                        required
                        label="Email"
                        name="email"
                        type="email"
                        placeholder="Masukkan Email supervisor"
                    />
                    <x-inputs.input
                        label="Default Password"
                        name="password"
                        disabled
                        value="SecurePassword2020"
                    />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Buat Akun</button>
                </div>
            </form>
        </div>
    </div>
@endpush

@push('scripts')
    <script>
        $(document).ready( function () {
            let table = $('#supervisorTable').DataTable();
            $('#supervisorTable tbody').on('click', 'tr', function () {
                data = table.row(this).data();
                $('#supervisorEditModal').on('show.bs.modal', function () {
                    $('#supervisor_user_id').val(data[0]);
                    $('#supervisor_name').val(data[1]);
                    $('#supervisor_username').val(data[2]);
                    $('#supervisor_email').val(data[3]);
                })
            })
        })
    </script>
@endpush