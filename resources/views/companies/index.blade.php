@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-th"></i> Data Perusahaan</h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#createCompany" aria-expanded="false" aria-controls="collapseForm">
                <i class="fas fa-plus"></i> Tambah Perusahaan Baru
            </button>
        </div>

        {{-- card table --}}
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Perusahaan</th>
                                <th>Jenis Perusahaan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($companies as $company)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $company->name }}</td>
                                    <td>{{ $company->company_type }}</td>
                                    <td class="text-center d-flex">
                                        <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v text-gray-500"></i>
                                        </a>
                                        <!-- Dropdown - Companies Information -->
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        <a class="dropdown-item" href="{{ route('companies.edit', ['company' => $company]) }}">
                                            <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Lihat Data Perusahaan
                                        </a>
                                        <form action="{{ route('companies.destroy', ['company' => $company]) }}" method="POST">
                                            @csrf @method('DELETE')
                                            <button class="dropdown-item">
                                                <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                Hapus Data
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modals')
    <!-- Modal -->
    <div class="modal fade" id="createCompany" tabindex="-1" role="dialog" aria-labelledby="createCompanyModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('companies.store') }}" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Perusahaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    @csrf
                    <div class="modal-body">
                        <x-inputs.input 
                            required
                            label="Nama"
                            name="name"
                            placeholder="Nama Perusahaan"
                        />
                        <x-inputs.select
                            required
                            label="Jenis Perusahaan"
                            name="company_type"
                            placeholder="Jenis Perusahaan"
                            :option-values="['Jasa','Dagang','Pabrik']"
                            :option-names="['Jasa','Dagang','Pabrik']"
                        />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endpush