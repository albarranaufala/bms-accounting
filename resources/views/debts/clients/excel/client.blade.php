<table>
    <thead>
        <tr>
            <th colspan="5">
                <strong>Kartu Supplier</strong>
            </th>
        </tr>
        <tr>
            <th colspan="5">
                <strong>{{$rangeDate}}</strong>
            </th>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>Supplier:</td>
            <td><strong>{{$client->name}}</strong></td>
        </tr>
        <tr>
            <td>Code:</td>
            <td><strong>{{$client->code}}</strong></td>
        </tr>
        <tr></tr>
        <tr>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>No. Bukti</th>
            <th>Tagihan</th>
            <th>Pembayaran</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($client->debts as $debt)
        <tr>
            <td>
                {{(new Carbon\Carbon($debt->date))->format('d M Y')}}
            </td>
            <td>
                {{$debt->description}}
            </td>
            <td>{{$debt->invoice_num ?? '-'}}</td>
            <td>{{$debt->bill_value ?? 0}}</td>
            <td>{{$debt->payment_value ?? 0}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th>Total</th>
            <th>{{$client->totalBill ?? 0}}</th>
            <th>{{$client->totalPayment ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Sisa Tagihan Saat Ini</th>
            <th></th>
            <th>{{$client->billRemaining ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Sisa Tagihan Sebelumnya</th>
            <th></th>
            <th>{{$client->billRemainingBefore ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Sisa Tagihan Total</th>
            <th></th>
            <th>{{($client->billRemaining + $client->billRemainingBefore) ?? 0}}</th>
        </tr>
    </tfoot>
</table>
