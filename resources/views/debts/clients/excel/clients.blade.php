<table>
    <tbody>
        <tr>
            <td colspan="7">
                <strong>Rekapitulasi Hutang</strong>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <strong>{{$rangeDate}}</strong>
            </td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Nama</td>
            <td>Tipe</td>
            <td>Sisa Tagihan Sebelumnya</td>
            <td>Tagihan</td>
            <td>Pembayaran</td>
            <td>Tagihan Total</td>
        </tr>
        @foreach ($clients as $client)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$client->name}} ({{$client->code}})
                </td>
                <td>{{$client->type->name}}</td>
                <td>{{$client->billRemainingBefore ?? 0}}</td>
                <td>{{$client->totalBill ?? 0}}</td>
                <td>{{$client->totalPayment ?? 0}}</td>
                <td>{{($client->billRemainingBefore + $client->billRemaining) ?? 0}}</td>
            </tr>
        @endforeach
        <tr>
            @php
                $billRemainingBefore = $clients->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                $totalBill = $clients->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                $totalPayment = $clients->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                $grandTotalBill = $clients->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
            @endphp
            <td></td>
            <td></td>
            <th>Total</th>
            <td>{{($billRemainingBefore) ?? 0}}</td>
            <td>{{($totalBill) ?? 0}}</td>
            <td>{{($totalPayment) ?? 0}}</td>
            <td>{{($grandTotalBill) ?? 0}}</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Nama Tipe Supplier</td>
            <td>Sisa Tagihan Sebelumnya</td>
            <td>Tagihan</td>
            <td>Pembayaran</td>
            <td>Tagihan Total</td>
        </tr>
        @foreach ($clientTypes as $type)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$type->name}}
                </td>
                @php
                    $type->billRemainingBefore = $type->clients->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                    $type->totalBill = $type->clients->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                    $type->totalPayment = $type->clients->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                    $type->billRemaining = $type->clients->reduce(fn($sum, $item)=>$sum+$item->billRemaining);
                    $type->grandTotalBill = $type->billRemaining + $type->billRemainingBefore;
                @endphp
                <td>{{($type->billRemainingBefore) ?? 0}}</td>
                <td>{{($type->totalBill) ?? 0}}</td>
                <td>{{($type->totalPayment) ?? 0}}</td>
                <td>{{($type->grandTotalBill) ?? 0}}</td>
            </tr>
        @endforeach
        <tr>
            @php
                $billRemainingBefore = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                $totalBill = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                $totalPayment = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                $grandTotalBill = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
            @endphp
            <td></td>
            <td>Total</td>
            <td>{{($billRemainingBefore) ?? 0}}</td>
            <td>{{($totalBill) ?? 0}}</td>
            <td>{{($totalPayment) ?? 0}}</td>
            <td>{{($grandTotalBill) ?? 0}}</td>
        </tr>
    </tbody>
</table>
