@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Rekap Data Supplier</h1>
        </div>
        <div class="mb-4 d-flex flex-column align-items-end flex-md-row justify-content-md-between align-items-md-center">
            <div class="align-self-start">
                <x-period-filter :route="route('debts.clients.index')" />
            </div>
            <div class="mt-3 mt-md-0 d-flex">
                <x-btn-export :route="route('debts.clients.index.export.excel')" class="mr-1"/>
                <a href="{{route('debts.clients.create')}}" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah Supplier
                </a>
            </div>
        </div>

        <div class="card shadow-sm mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                        <tr class="text-uppercase small text-primary">
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Tipe</th>
                            <th class="text-right">Sisa Tagihan Sebelumnya</th>
                            <th class="text-right">Tagihan</th>
                            <th class="text-right">Pembayaran</th>
                            <th class="text-right">Tagihan Total</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($clients as $client)
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>
                                <td>
                                    <a href="{{route('debts.clients.show', $client->id)}}">{{$client->name}}</a> <br>
                                    <small class="text-gray-500">{{$client->code}}</small>
                                </td>
                                <td>{{$client->type->name}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$client->billRemainingBefore ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$client->totalBill ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$client->totalPayment ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{($client->billRemainingBefore + $client->billRemaining) ?? 0}}</span></td>
                                <td class="text-center">
                                    <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v text-gray-500"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        <a class="dropdown-item" href="{{route('debts.clients.show', $client->id)}}">
                                            <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Lihat Kartu
                                        </a>
                                        <a class="dropdown-item" href="{{route('debts.clients.edit', $client->id)}}">
                                            <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Sunting
                                        </a>
                                        <a class="dropdown-item delete" href="{{route('debts.clients.destroy', $client->id)}}">
                                            <i class="fas fa-trash fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Hapus
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            @php
                                $billRemainingBefore = $clients->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                                $totalBill = $clients->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                                $totalPayment = $clients->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                                $grandTotalBill = $clients->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
                            @endphp
                            <th class="text-right" colspan="3">Total</th>
                            <th class="text-right">Rp <span class="mask-money">{{($billRemainingBefore) ?? 0}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{($totalBill) ?? 0}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{($totalPayment) ?? 0}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{($grandTotalBill) ?? 0}}</span></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card shadow-sm">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                        <tr class="text-uppercase small text-primary">
                            <th>No.</th>
                            <th>Nama Tipe Supplier</th>
                            <th class="text-right">Sisa Tagihan Sebelumnya</th>
                            <th class="text-right">Tagihan</th>
                            <th class="text-right">Pembayaran</th>
                            <th class="text-right">Tagihan Total</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($clientTypes as $type)
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>
                                <td>
                                    {{$type->name}}
                                </td>
                                @php
                                    $type->billRemainingBefore = $type->clients->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                                    $type->totalBill = $type->clients->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                                    $type->totalPayment = $type->clients->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                                    $type->billRemaining = $type->clients->reduce(fn($sum, $item)=>$sum+$item->billRemaining);
                                    $type->grandTotalBill = $type->billRemaining + $type->billRemainingBefore;
                                @endphp
                                <td class="text-right">Rp <span class="mask-money">{{($type->billRemainingBefore) ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{($type->totalBill) ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{($type->totalPayment) ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{($type->grandTotalBill) ?? 0}}</span></td>
                                <td class="text-center">
                                    <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v text-gray-500"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        <a data-type-name="{{$type->name}}" class="dropdown-item edit-client-type" href="{{route('debts.clients.types.update', $type->id)}}">
                                            <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Sunting
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            @php
                                $billRemainingBefore = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                                $totalBill = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                                $totalPayment = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                                $grandTotalBill = $clientTypes->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
                            @endphp
                            <th class="text-right" colspan="2">Total</th>
                            <th class="text-right">Rp <span class="mask-money">{{($billRemainingBefore) ?? 0}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{($totalBill) ?? 0}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{($totalPayment) ?? 0}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{($grandTotalBill) ?? 0}}</span></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
            $('body').on('click', '.edit-client-type', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#typeNameInput').attr('value', $(this).data('typeName'));
                $('#editTypeForm').attr('action', action);
                $('#editTypeModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Delete Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data ini dan data lain yang terhubung akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Edit Type Modal-->
        <div class="modal fade" id="editTypeModal" tabindex="-1" role="dialog" aria-labelledby="editTypeModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="editTypeForm" class="modal-content" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="editTypeModalLabel">Sunting Tipe Supplier</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <x-inputs.input
                            id="typeNameInput"
                            name="type_name"
                            label="Nama Tipe Supplier"
                            required
                            placeholder="Masukkan nama tipe supplier"/>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">
                            <i class="fas fa-save mr-1"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Modal -->
    </div>
@endpush
