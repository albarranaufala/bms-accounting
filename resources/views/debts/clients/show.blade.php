@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center">
            <h1 class="h3 text-primary font-weight-bold mb-0">Kartu Supplier</h1>
        </div>
        <x-period-filter :route="route('debts.clients.show', $client->id)" class="mb-4"/>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-12 d-flex justify-content-between flex-column flex-lg-row">
                        <div>
                            <h2 class="h4 font-weight-bold">{{$client->name}} ({{$client->code}})</h2>
                            <p class="text-gray-500 mb-0">{{$client->type->name}}</p>
                        </div>
                        <div class="mt-3 mt-lg-0 text-left text-lg-right">
                            <a href="{{route('debts.clients.destroy', $client->id)}}" class="btn btn-danger delete" type="button">
                                <i class="fas fa-trash"></i>
                                <span class="d-none d-lg-inline">
                                    Hapus
                                </span>
                            </a>
                            <a class="btn btn-success" href="{{route('debts.clients.edit', $client->id)}}">
                                <i class="fas fa-edit"></i>
                                <span class="d-none d-lg-inline">
                                    Sunting Supplier
                                </span>
                            </a>
                            <br>
                            <x-btn-export :route="route('debts.clients.show.export.excel', $client->id)" class="mt-1"/>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">NPWP</p>
                            <p class="font-weight-bold mb-0">{{$client->npwp ?? '-'}}</p>
                        </div>
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Email</p>
                            <p class="font-weight-bold mb-0">{{$client->email ?? '-'}}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Alamat</p>
                            <p class="font-weight-bold mb-0">{{$client->address ?? '-'}}</p>
                        </div>
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Telepon</p>
                            <p class="font-weight-bold mb-0">{{$client->phone ?? '-'}}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Narahubung</p>
                            <p class="font-weight-bold mb-0">{{$client->contact_person ?? '-'}}</p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover data-table" width="100%" cellspacing="0">
                                <thead class="text-uppercase small text-primary">
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Uraian</th>
                                    <th>No. Bukti</th>
                                    <th>Tagihan</th>
                                    <th>Pembayaran</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th class="text-right" colspan="3">Total</th>
                                    <th class="text-right">Rp <span class="mask-money">{{$client->totalBill ?? 0}}</span></th>
                                    <th class="text-right">Rp <span class="mask-money">{{$client->totalPayment ?? 0}}</th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="3">Sisa Tagihan Saat Ini</th>
                                    <th class="text-right" colspan="2">Rp <span class="mask-money">{{$client->billRemaining ?? 0}}</span></th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="3">Sisa Tagihan Sebelumnya</th>
                                    <th class="text-right" colspan="2">Rp <span class="mask-money">{{$client->billRemainingBefore ?? 0}}</span></th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="3">Sisa Tagihan Total</th>
                                    <th class="text-right" colspan="2">Rp <span class="mask-money">{{($client->billRemaining + $client->billRemainingBefore) ?? 0}}</span></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach ($client->debts as $debt)
                                    <tr>
                                        <td>
                                            {{(new Carbon\Carbon($debt->date))->format('d M Y')}}
                                        </td>
                                        <td>
                                            {{$debt->description}}
                                        </td>
                                        <td>{{$debt->invoice_num ?? '-'}}</td>
                                        <td class="text-right">Rp <span class="mask-money">{{$debt->bill_value ?? 0}}</span></td>
                                        <td class="text-right">Rp <span class="mask-money">{{$debt->payment_value ?? 0}}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Logout Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data ini dan data lain yang terhubung akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Topbar -->
    </div>
@endpush
