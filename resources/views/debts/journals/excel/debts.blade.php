<table>
    <tbody>
    <tr>
        <td colspan="7">
            <strong>Jurnal Hutang</strong>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <strong>{{$rangeDate}}</strong>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <td>No.</td>
        <td>Tanggal</td>
        <td>Nama Supplier</td>
        <td>Uraian</td>
        <td>Tagihan</td>
        <td>Pembayaran</td>
    </tr>
    @foreach ($debts as $debt)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>
                {{(new Carbon\Carbon($debt->date))->format('d M Y')}}
            </td>
            <td>
                @if ($debt->client)
                    {{$debt->client->name}} ({{$debt->client->code}})
                @endif
            </td>
            <td>{{$debt->description}}</td>
            <td>{{$debt->bill_value ?? 0}}</td>
            <td>{{$debt->payment_value ?? 0}}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{$debts->reduce(fn($sum,$item)=>$sum+$item->bill_value)}}</td>
        <td>{{$debts->reduce(fn($sum,$item)=>$sum+$item->payment_value)}}</td>
    </tr>
    </tbody>
</table>
