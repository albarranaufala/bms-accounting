@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Sunting Data Hutang</h1>
        </div>

        <div class="card shadow-sm">
            <form class="card-body" action="{{route('debts.journals.update', $debt->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Supplier"
                            name="client"
                            placeholder="Pilih supplier"
                            :value="$debt->client->id ?? null"
                            search
                            :option-values="$clients->map(fn($client)=>$client->id)"
                            :option-names="$clients->map(fn($client)=>$client->name)"
                        />
                        <x-inputs.input
                            label="Tanggal"
                            name="date"
                            type="date"
                            :value="$debt->date"
                            required/>
                        <x-inputs.textarea
                            label="Uraian"
                            name="description"
                            rows="3"
                            :value="$debt->description"
                            placeholder="Masukkan uraian"/>
                        <x-inputs.input
                            label="No. Bukti"
                            name="invoice_num"
                            :value="$debt->invoice_num"
                            placeholder="Masukkan no. bukti"/>
                    </div>
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="form-group col-6">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="billType" name="type" class="custom-control-input" value="bill" {{$debt->bill_value ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="billType">Tagihan</label>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="paymentType" name="type" class="custom-control-input" value="payment" {{$debt->payment_value ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="paymentType">Pembayaran</label>
                                </div>
                            </div>
                        </div>
                        @if ($debt->bill_value)
                            <x-inputs.input
                                label="Nilai Tagihan"
                                name="bill_value"
                                id="billValue"
                                type="number"
                                step="0.001"
                                :value="$debt->bill_value"
                                placeholder="Masukkan nilai tagihan"/>
                            <x-inputs.input
                                label="Nilai Pembayaran"
                                name="payment_value"
                                id="paymentValue"
                                type="number"
                                step="0.001"
                                disabled
                                :value="$debt->payment_value"
                                placeholder="Masukkan nilai pembayaran"/>
                        @else
                            <x-inputs.input
                                label="Nilai Tagihan"
                                name="bill_value"
                                id="billValue"
                                type="number"
                                step="0.001"
                                :value="$debt->bill_value"
                                disabled
                                placeholder="Masukkan nilai tagihan"/>
                            <x-inputs.input
                                label="Nilai Pembayaran"
                                name="payment_value"
                                id="paymentValue"
                                type="number"
                                step="0.001"
                                :value="$debt->payment_value"
                                placeholder="Masukkan nilai pembayaran"/>
                        @endif
                        <x-inputs.textarea
                            label="Keterangan"
                            name="information"
                            rows="3"
                            :value="$debt->information"
                            placeholder="Masukkan keterangan..."/>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Debit"
                            name="debit_account"
                            placeholder="Pilih akun"
                            search
                            :value="$debt->debitAccount->id ?? null"
                            :option-values="$accounts->map(fn($acc)=>$acc->id)"
                            :option-names="$accounts->map(fn($acc)=>$acc->account_num.' - '.$acc->name)"
                        />
                    </div>
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Kredit"
                            name="credit_account"
                            placeholder="Pilih akun"
                            search
                            :value="$debt->creditAccount->id ?? null"
                            :option-values="$accounts->map(fn($acc)=>$acc->id)"
                            :option-names="$accounts->map(fn($acc)=>$acc->account_num.' - '.$acc->name)"
                        />
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <a href="{{route('debts.journals.index')}}" class="btn btn-light mr-3">Batal</a>
                        <button class="btn btn-success"><i class="fas fa-save"></i> Simpan Perubahan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready( function () {
            const typeChecks = $("input[name='type']");
            typeChecks.change(function(){
                if(this.checked){
                    updateTransactionInput($(this).val());
                }
            });

            function updateTransactionInput(type){
                if(type === 'bill'){
                    $('#billValue').prop('disabled', false);
                    $('#paymentValue').prop('disabled', true);
                } else if(type === 'payment') {
                    $('#billValue').prop('disabled', true);
                    $('#paymentValue').prop('disabled', false);
                }
            }
        } );
    </script>
@endpush
