@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center">
            <h1 class="h3 text-primary font-weight-bold mb-0">Kartu Pelanggan</h1>
        </div>
        <x-period-filter :route="route('receivables.customers.show', $customer->id)" class="mb-4"/>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-12 d-flex justify-content-between flex-column flex-lg-row">
                        <div>
                            <h2 class="h4 font-weight-bold">{{$customer->name}} ({{$customer->code}})</h2>
                            <p class="text-gray-500 mb-0">{{$customer->type->name}}</p>
                        </div>
                        <div class="mt-3 mt-lg-0 text-left text-lg-right">
                            <a href="{{route('receivables.customers.destroy', $customer->id)}}" class="btn btn-danger delete" type="button">
                                <i class="fas fa-trash"></i>
                                <span class="d-none d-lg-inline">
                                    Hapus
                                </span>
                            </a>
                            <a class="btn btn-success" href="{{route('receivables.customers.edit', $customer->id)}}">
                                <i class="fas fa-edit"></i>
                                <span class="d-none d-lg-inline">
                                    Sunting Pelanggan
                                </span>
                            </a>
                            <br>
                            <x-btn-export :route="route('receivables.customers.show.export.excel', $customer->id)" class="mt-1"/>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">NPWP</p>
                            <p class="font-weight-bold mb-0">{{$customer->npwp ?? '-'}}</p>
                        </div>
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Email</p>
                            <p class="font-weight-bold mb-0">{{$customer->email ?? '-'}}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Alamat</p>
                            <p class="font-weight-bold mb-0">{{$customer->address ?? '-'}}</p>
                        </div>
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Telepon</p>
                            <p class="font-weight-bold mb-0">{{$customer->phone ?? '-'}}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Narahubung</p>
                            <p class="font-weight-bold mb-0">{{$customer->contact_person ?? '-'}}</p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover data-table" width="100%" cellspacing="0">
                                <thead class="text-uppercase small text-primary">
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Uraian</th>
                                        <th>No. Bukti</th>
                                        <th>Tagihan</th>
                                        <th>Pembayaran</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th class="text-right" colspan="3">Total</th>
                                        <th class="text-right">Rp <span class="mask-money">{{$customer->totalPayment ?? 0}}</th>
                                        <th class="text-right">Rp <span class="mask-money">{{$customer->totalBill ?? 0}}</span></th>
                                    </tr>
                                    <tr>
                                        <th class="text-right" colspan="3">Sisa Tagihan Saat Ini</th>
                                        <th class="text-right" colspan="2">Rp <span class="mask-money">{{$customer->billRemaining ?? 0}}</span></th>
                                    </tr>
                                    <tr>
                                        <th class="text-right" colspan="3">Sisa Tagihan Sebelumnya</th>
                                        <th class="text-right" colspan="2">Rp <span class="mask-money">{{$customer->billRemainingBefore ?? 0}}</span></th>
                                    </tr>
                                    <tr>
                                        <th class="text-right" colspan="3">Sisa Tagihan Total</th>
                                        <th class="text-right" colspan="2">Rp <span class="mask-money">{{($customer->billRemaining + $customer->billRemainingBefore) ?? 0}}</span></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                @foreach ($customer->receivables as $receivable)
                                    <tr>
                                        <td>
                                            {{(new Carbon\Carbon($receivable->date))->format('d M Y')}}
                                        </td>
                                        <td>
                                            {{$receivable->description}}
                                        </td>
                                        <td>{{$receivable->invoice_num ?? '-'}}</td>
                                        <td class="text-right">Rp <span class="mask-money">{{$receivable->payment_value ?? 0}}</span></td>
                                        <td class="text-right">Rp <span class="mask-money">{{$receivable->bill_value ?? 0}}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Logout Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data ini dan data lain yang terhubung akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Topbar -->
    </div>
@endpush
