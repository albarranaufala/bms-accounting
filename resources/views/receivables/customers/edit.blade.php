@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Sunting Data Pelanggan</h1>
        </div>

        <div class="card shadow-sm">
            <form class="card-body" action="{{route('receivables.customers.update', $customer->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.input
                            label="Nama"
                            name="name"
                            placeholder="Masukkan nama pelanggan"
                            :value="$customer->name"
                            required />
                        <x-inputs.select
                            required
                            label="Tipe"
                            name="type"
                            placeholder="Pilih tipe pelanggan"
                            :value="$customer->type->id"
                            search
                            :option-values="$customerTypes->map(fn($type)=>$type->id)"
                            :option-names="$customerTypes->map(fn($type)=>$type->name)"
                        />
                        <div class="w-100 text-right mb-3">
                            <a class="small" href="" data-toggle="modal" data-target="#addTypeModal">
                                + Tambah Tipe
                            </a>
                        </div>
                        <x-inputs.input
                            label="Email"
                            name="email"
                            type="email"
                            :value="$customer->email"
                            placeholder="Masukkan email pelanggan"/>
                        <x-inputs.input
                            label="NPWP"
                            name="npwp"
                            :value="$customer->npwp"
                            placeholder="Masukkan NPWP pelanggan"/>
                    </div>
                    <div class="col-md-6">
                        <x-inputs.input
                            label="Kode"
                            required
                            name="code"
                            :value="$customer->code"
                            placeholder="Masukkan kode pelanggan"/>
                        <x-inputs.input
                            label="No. Telepon"
                            name="phone"
                            type="number"
                            :value="$customer->phone"
                            placeholder="Masukkan no. telepon pelanggan"/>
                        <x-inputs.input
                            label="Narahubung"
                            name="contact_person"
                            :value="$customer->contact_person"
                            placeholder="Masukkan narahubung pelanggan"/>
                        <x-inputs.textarea
                            label="Alamat"
                            name="address"
                            rows="3"
                            :value="$customer->address"
                            placeholder="Masukkan alamat pelanggan..."/>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.input
                            required
                            label="Saldo Awal"
                            name="balance"
                            type="number"
                            step="0.001"
                            :value="$customer->balance"
                            placeholder="Masukkan saldo awal pelanggan"/>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <a href="{{route('receivables.customers.index')}}" class="btn btn-light mr-3">Batal</a>
                        <button class="btn btn-success"><i class="fas fa-save"></i> Simpan Perubahan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal Add Type -->
    <div class="modal fade" id="addTypeModal" tabindex="-1" role="dialog" aria-labelledby="addTypeModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{route('receivables.customers.types.store')}}" class="modal-content" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="addTypeModalTitle">Tambah Tipe Pelanggan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <x-inputs.input
                        required
                        label="Nama Tipe"
                        name="type_name"
                        placeholder="Masukkan nama tipe pelanggan"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save mr-2"></i>Tambah Tipe
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
