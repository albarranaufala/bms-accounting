<table>
    <tbody>
        <tr>
            <td colspan="7">
                <strong>Rekapitulasi Piutang dan Pembayaran</strong>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <strong>{{$rangeDate}}</strong>
            </td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Nama</td>
            <td>Tipe</td>
            <td>Sisa Tagihan Sebelumnya</td>
            <td>Tagihan</td>
            <td>Pembayaran</td>
            <td>Tagihan Total</td>
        </tr>
        @foreach ($customers as $customer)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$customer->name}} ({{$customer->code}})
                </td>
                <td>{{$customer->type->name}}</td>
                <td>{{$customer->billRemainingBefore ?? 0}}</td>
                <td>{{$customer->totalPayment ?? 0}}</td>
                <td>{{$customer->totalBill ?? 0}}</td>
                <td>{{($customer->billRemainingBefore + $customer->billRemaining) ?? 0}}</td>
            </tr>
        @endforeach
        <tr>
            @php
                $billRemainingBefore = $customers->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                $totalBill = $customers->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                $totalPayment = $customers->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                $grandTotalBill = $customers->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
            @endphp
            <td></td>
            <td></td>
            <th>Total</th>
            <td>{{($billRemainingBefore) ?? 0}}</td>
            <td>{{($totalBill) ?? 0}}</td>
            <td>{{($totalPayment) ?? 0}}</td>
            <td>{{($grandTotalBill) ?? 0}}</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Nama Tipe Pelanggan</td>
            <td>Sisa Tagihan Sebelumnya</td>
            <td>Tagihan</td>
            <td>Pembayaran</td>
            <td>Tagihan Total</td>
        </tr>
        @foreach ($customerTypes as $type)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$type->name}}
                </td>
                @php
                    $type->billRemainingBefore = $type->customers->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                    $type->totalBill = $type->customers->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                    $type->totalPayment = $type->customers->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                    $type->billRemaining = $type->customers->reduce(fn($sum, $item)=>$sum+$item->billRemaining);
                    $type->grandTotalBill = $type->billRemaining + $type->billRemainingBefore;
                @endphp
                <td>{{($type->billRemainingBefore) ?? 0}}</td>
                <td>{{($type->totalBill) ?? 0}}</td>
                <td>{{($type->totalPayment) ?? 0}}</td>
                <td>{{($type->grandTotalBill) ?? 0}}</td>
            </tr>
        @endforeach
        <tr>
            @php
                $billRemainingBefore = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                $totalBill = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                $totalPayment = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                $grandTotalBill = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
            @endphp
            <td></td>
            <td>Total</td>
            <td>{{($billRemainingBefore) ?? 0}}</td>
            <td>{{($totalBill) ?? 0}}</td>
            <td>{{($totalPayment) ?? 0}}</td>
            <td>{{($grandTotalBill) ?? 0}}</td>
        </tr>
    </tbody>
</table>
