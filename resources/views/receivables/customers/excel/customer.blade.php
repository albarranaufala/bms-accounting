<table>
    <thead>
        <tr>
            <th colspan="5">
                <strong>Kartu Customer</strong>
            </th>
        </tr>
        <tr>
            <th colspan="5">
                <strong>{{$rangeDate}}</strong>
            </th>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>Customer:</td>
            <td><strong>{{$customer->name}}</strong></td>
        </tr>
        <tr>
            <td>Code:</td>
            <td><strong>{{$customer->code}}</strong></td>
        </tr>
        <tr></tr>
        <tr>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>No. Bukti</th>
            <th>Tagihan</th>
            <th>Pembayaran</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($customer->receivables as $receivable)
        <tr>
            <td>
                {{(new Carbon\Carbon($receivable->date))->format('d M Y')}}
            </td>
            <td>
                {{$receivable->description}}
            </td>
            <td>{{$receivable->invoice_num ?? '-'}}</td>
            <td>{{$receivable->bill_value ?? 0}}</td>
            <td>{{$receivable->payment_value ?? 0}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th>Total</th>
            <th>{{$customer->totalBill ?? 0}}</th>
            <th>{{$customer->totalPayment ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Sisa Tagihan Saat Ini</th>
            <th></th>
            <th>{{$customer->billRemaining ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Sisa Tagihan Sebelumnya</th>
            <th></th>
            <th>{{$customer->billRemainingBefore ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Sisa Tagihan Total</th>
            <th></th>
            <th>{{($customer->billRemaining + $customer->billRemainingBefore) ?? 0}}</th>
        </tr>
    </tfoot>
</table>
