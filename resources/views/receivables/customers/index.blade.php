@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Rekap Data Pelanggan</h1>
        </div>
        <div class="mb-4 d-flex flex-column align-items-end flex-md-row justify-content-md-between align-items-md-center">
            <div class="align-self-start">
                <x-period-filter :route="route('receivables.customers.index')" />
            </div>
            <div class="mt-3 mt-md-0 d-flex">
                <x-btn-export :route="route('receivables.customers.index.export.excel')" class="mr-1"/>
                <a href="{{route('receivables.customers.create')}}" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah Pelanggan
                </a>
            </div>
        </div>

        <div class="card shadow-sm mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-uppercase small text-primary">
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Tipe</th>
                                <th class="text-right">Sisa Tagihan Sebelumnya</th>
                                <th class="text-right">Tagihan</th>
                                <th class="text-right">Pembayaran</th>
                                <th class="text-right">Tagihan Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        <a href="{{route('receivables.customers.show', $customer->id)}}">{{$customer->name}}</a> <br>
                                        <small class="text-gray-500">{{$customer->code}}</small>
                                    </td>
                                    <td>{{$customer->type->name}}</td>
                                    <td class="text-right">Rp <span class="mask-money">{{$customer->billRemainingBefore ?? 0}}</span></td>
                                    <td class="text-right">Rp <span class="mask-money">{{$customer->totalPayment ?? 0}}</span></td>
                                    <td class="text-right">Rp <span class="mask-money">{{$customer->totalBill ?? 0}}</span></td>
                                    <td class="text-right">Rp <span class="mask-money">{{($customer->billRemainingBefore + $customer->billRemaining) ?? 0}}</span></td>
                                    <td class="text-center">
                                        <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v text-gray-500"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                            <a class="dropdown-item" href="{{route('receivables.customers.show', $customer->id)}}">
                                                <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Lihat Kartu
                                            </a>
                                            <a class="dropdown-item" href="{{route('receivables.customers.edit', $customer->id)}}">
                                                <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Sunting
                                            </a>
                                            <a class="dropdown-item delete" href="{{route('receivables.customers.destroy', $customer->id)}}">
                                                <i class="fas fa-trash fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Hapus
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                @php
                                    $billRemainingBefore = $customers->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                                    $totalBill = $customers->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                                    $totalPayment = $customers->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                                    $grandTotalBill = $customers->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
                                @endphp
                                <th class="text-right" colspan="3">Total</th>
                                <th class="text-right">Rp <span class="mask-money">{{($billRemainingBefore) ?? 0}}</span></th>
                                <th class="text-right">Rp <span class="mask-money">{{($totalBill) ?? 0}}</span></th>
                                <th class="text-right">Rp <span class="mask-money">{{($totalPayment) ?? 0}}</span></th>
                                <th class="text-right">Rp <span class="mask-money">{{($grandTotalBill) ?? 0}}</span></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card shadow-sm">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-uppercase small text-primary">
                                <th>No.</th>
                                <th>Nama Tipe Pelanggan</th>
                                <th class="text-right">Sisa Tagihan Sebelumnya</th>
                                <th class="text-right">Tagihan</th>
                                <th class="text-right">Pembayaran</th>
                                <th class="text-right">Tagihan Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customerTypes as $type)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        {{$type->name}}
                                    </td>
                                    @php
                                        $type->billRemainingBefore = $type->customers->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                                        $type->totalBill = $type->customers->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                                        $type->totalPayment = $type->customers->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                                        $type->billRemaining = $type->customers->reduce(fn($sum, $item)=>$sum+$item->billRemaining);
                                        $type->grandTotalBill = $type->billRemaining + $type->billRemainingBefore;
                                    @endphp
                                    <td class="text-right">Rp <span class="mask-money">{{($type->billRemainingBefore) ?? 0}}</span></td>
                                    <td class="text-right">Rp <span class="mask-money">{{($type->totalBill) ?? 0}}</span></td>
                                    <td class="text-right">Rp <span class="mask-money">{{($type->totalPayment) ?? 0}}</span></td>
                                    <td class="text-right">Rp <span class="mask-money">{{($type->grandTotalBill) ?? 0}}</span></td>
                                    <td class="text-center">
                                        <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v text-gray-500"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                            <a data-type-name="{{$type->name}}" class="dropdown-item edit-customer-type" href="{{route('receivables.customers.types.update', $type->id)}}">
                                                <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Sunting
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                @php
                                    $billRemainingBefore = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                                    $totalBill = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                                    $totalPayment = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                                    $grandTotalBill = $customerTypes->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
                                @endphp
                                <th class="text-right" colspan="2">Total</th>
                                <th class="text-right">Rp <span class="mask-money">{{($billRemainingBefore) ?? 0}}</span></th>
                                <th class="text-right">Rp <span class="mask-money">{{($totalBill) ?? 0}}</span></th>
                                <th class="text-right">Rp <span class="mask-money">{{($totalPayment) ?? 0}}</span></th>
                                <th class="text-right">Rp <span class="mask-money">{{($grandTotalBill) ?? 0}}</span></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
            $('body').on('click', '.edit-customer-type', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#typeNameInput').attr('value', $(this).data('typeName'));
                $('#editTypeForm').attr('action', action);
                $('#editTypeModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Delete Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data ini dan data lain yang terhubung akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Edit Type Modal-->
        <div class="modal fade" id="editTypeModal" tabindex="-1" role="dialog" aria-labelledby="editTypeModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="editTypeForm" class="modal-content" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="editTypeModalLabel">Sunting Tipe Pelanggan</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <x-inputs.input
                            id="typeNameInput"
                            name="type_name"
                            label="Nama Tipe Pelanggan"
                            required
                            placeholder="Masukkan nama tipe pelanggan"/>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">
                            <i class="fas fa-save mr-1"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Modal -->
    </div>
@endpush
