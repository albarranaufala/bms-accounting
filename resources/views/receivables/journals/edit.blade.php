@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Sunting Data Piutang</h1>
        </div>

        <div class="card shadow-sm">
            <form class="card-body" action="{{route('receivables.journals.update', $receivable->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Pelanggan"
                            name="customer"
                            placeholder="Pilih pelanggan"
                            :value="$receivable->customer->id ?? null"
                            search
                            :option-values="$customers->map(fn($customer)=>$customer->id)"
                            :option-names="$customers->map(fn($customer)=>$customer->name)"
                        />
                        <x-inputs.input
                            label="Tanggal"
                            name="date"
                            type="date"
                            :value="$receivable->date"
                            required/>
                        <x-inputs.textarea
                            label="Uraian"
                            name="description"
                            rows="3"
                            :value="$receivable->description"
                            placeholder="Masukkan uraian"/>
                        <x-inputs.input
                            label="No. Bukti"
                            name="invoice_num"
                            :value="$receivable->invoice_num"
                            placeholder="Masukkan no. bukti"/>
                    </div>
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="form-group col-6">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="billType" name="type" class="custom-control-input" value="bill" {{$receivable->payment_value ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="billType">Tagihan</label>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="paymentType" name="type" class="custom-control-input" value="payment" {{$receivable->bill_value ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="paymentType">Pembayaran</label>
                                </div>
                            </div>
                        </div>
                        @if ($receivable->payment_value)
                            <x-inputs.input
                                label="Nilai Tagihan"
                                name="payment_value"
                                id="billValue"
                                type="number"
                                step="0.001"
                                :value="$receivable->payment_value"
                                placeholder="Masukkan nilai tagihan"/>
                            <x-inputs.input
                                label="Nilai Pembayaran"
                                name="bill_value"
                                id="paymentValue"
                                type="number"
                                step="0.001"
                                disabled
                                :value="$receivable->bill_value"
                                placeholder="Masukkan nilai pembayaran"/>
                        @else
                            <x-inputs.input
                                label="Nilai Tagihan"
                                name="payment_value"
                                id="billValue"
                                type="number"
                                step="0.001"
                                :value="$receivable->payment_value"
                                disabled
                                placeholder="Masukkan nilai tagihan"/>
                            <x-inputs.input
                                label="Nilai Pembayaran"
                                name="bill_value"
                                id="paymentValue"
                                type="number"
                                step="0.001"
                                :value="$receivable->bill_value"
                                placeholder="Masukkan nilai pembayaran"/>
                        @endif
                        <x-inputs.textarea
                            label="Keterangan"
                            name="information"
                            rows="3"
                            :value="$receivable->information"
                            placeholder="Masukkan keterangan..."/>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Debit"
                            name="debit_account"
                            placeholder="Pilih akun"
                            search
                            :value="$receivable->debitAccount->id ?? null"
                            :option-values="$accounts->map(fn($acc)=>$acc->id)"
                            :option-names="$accounts->map(fn($acc)=>$acc->account_num.' - '.$acc->name)"
                        />
                    </div>
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Kredit"
                            name="credit_account"
                            placeholder="Pilih akun"
                            search
                            :value="$receivable->creditAccount->id ?? null"
                            :option-values="$accounts->map(fn($acc)=>$acc->id)"
                            :option-names="$accounts->map(fn($acc)=>$acc->account_num.' - '.$acc->name)"
                        />
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <a href="{{route('receivables.journals.index')}}" class="btn btn-light mr-3">Batal</a>
                        <button class="btn btn-success"><i class="fas fa-save"></i> Simpan Perubahan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready( function () {
            const typeChecks = $("input[name='type']");
            typeChecks.change(function(){
                if(this.checked){
                    updateTransactionInput($(this).val());
                }
            });

            function updateTransactionInput(type){
                if(type === 'bill'){
                    $('#billValue').prop('disabled', false);
                    $('#paymentValue').prop('disabled', true);
                } else if(type === 'payment') {
                    $('#billValue').prop('disabled', true);
                    $('#paymentValue').prop('disabled', false);
                }
            }
        } );
    </script>
@endpush
