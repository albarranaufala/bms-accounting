@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Tambah Data Piutang</h1>
        </div>

        <div class="card shadow-sm">
            <form class="card-body" action="{{route('receivables.journals.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            required
                            label="Pelanggan"
                            name="customer"
                            placeholder="Pilih pelanggan"
                            search
                            :option-values="$customers->map(fn($customer)=>$customer->id)"
                            :option-names="$customers->map(fn($customer)=>$customer->name)"
                        />
                        <x-inputs.input
                            label="Tanggal"
                            name="date"
                            type="date"
                            :value="date('Y-m-d')"
                            required/>
                        <x-inputs.textarea
                            label="Uraian"
                            name="description"
                            rows="3"
                            placeholder="Masukkan uraian"/>
                        <x-inputs.input
                            label="No. Bukti"
                            name="invoice_num"
                            placeholder="Masukkan no. bukti"/>
                    </div>
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="form-group col-6">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="billType" name="type" class="custom-control-input" value="bill" checked>
                                    <label class="custom-control-label" for="billType">Tagihan</label>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="paymentType" name="type" class="custom-control-input" value="payment">
                                    <label class="custom-control-label" for="paymentType">Pembayaran</label>
                                </div>
                            </div>
                        </div>
                        <x-inputs.input
                            label="Nilai Tagihan"
                            name="payment_value"
                            id="billValue"
                            type="number"
                            step="0.001"
                            placeholder="Masukkan nilai tagihan"/>
                        <x-inputs.input
                            label="Nilai Pembayaran"
                            name="bill_value"
                            id="paymentValue"
                            type="number"
                            step="0.001"
                            disabled
                            placeholder="Masukkan nilai pembayaran"/>
                        <x-inputs.textarea
                            label="Keterangan"
                            name="information"
                            rows="3"
                            placeholder="Masukkan keterangan..."/>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Debit"
                            name="debit_account"
                            placeholder="Pilih akun"
                            search
                            :option-values="$accounts->map(fn($acc)=>$acc->id)"
                            :option-names="$accounts->map(fn($acc)=>$acc->account_num.' - '.$acc->name)"
                        />
                    </div>
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Kredit"
                            name="credit_account"
                            placeholder="Pilih akun"
                            search
                            :option-values="$accounts->map(fn($acc)=>$acc->id)"
                            :option-names="$accounts->map(fn($acc)=>$acc->account_num.' - '.$acc->name)"
                        />
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <a href="{{route('receivables.journals.index')}}" class="btn btn-light mr-3">Batal</a>
                        <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah Data Piutang</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready( function () {
            const typeChecks = $("input[name='type']");
            typeChecks.change(function(){
                if(this.checked){
                    updateTransactionInput($(this).val());
                }
            });

            function updateTransactionInput(type){
                if(type === 'bill'){
                    $('#billValue').prop('disabled', false);
                    $('#paymentValue').prop('disabled', true);
                } else if(type === 'payment') {
                    $('#billValue').prop('disabled', true);
                    $('#paymentValue').prop('disabled', false);
                }
            }
        } );
    </script>
@endpush
