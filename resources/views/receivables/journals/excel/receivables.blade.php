<table>
    <tbody>
    <tr>
        <td colspan="7">
            <strong>Jurnal Piutang</strong>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <strong>{{$rangeDate}}</strong>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <td>No.</td>
        <td>Tanggal</td>
        <td>Nama Pelanggan</td>
        <td>Uraian</td>
        <td>Tagihan</td>
        <td>Pembayaran</td>
    </tr>
    @foreach ($receivables as $receivable)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>
                {{(new Carbon\Carbon($receivable->date))->format('d M Y')}}
            </td>
            <td>
                @if ($receivable->customer)
                    {{$receivable->customer->name}} ({{$receivable->customer->code}})
                @endif
            </td>
            <td>{{$receivable->description}}</td>
            <td>{{$receivable->bill_value ?? 0}}</td>
            <td>{{$receivable->payment_value ?? 0}}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{$receivables->reduce(fn($sum,$item)=>$sum+$item->bill_value)}}</td>
        <td>{{$receivables->reduce(fn($sum,$item)=>$sum+$item->payment_value)}}</td>
    </tr>
    </tbody>
</table>
