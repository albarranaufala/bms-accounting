@extends('layouts.app')

@section('content')
<main class="bg-secondary w-100 d-flex flex-column align-items-center justify-content-center p-4" style="min-height: 100vh">
    <h1 class="h3 font-weight-bold mb-4 text-uppercase text-white">
        Selamat Datang
    </h1>
    <div class="card shadow rounded-lg" style="width: 350px">
        <form action="{{route('login')}}" class="card-body p-4" method="POST">
            @csrf
            <x-inputs.input
                label="Username"
                name="username"
                placeholder="Masukkan username anda"
                required/>

            <x-inputs.input
                label="Password"
                name="password"
                type="password"
                placeholder="Masukkan password anda"
                required/>

            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        Ingat saya
                    </label>
                </div>
            </div>

            <button class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> Login</button>

            <div class="mt-3">
                @if (Route::has('password.request'))
                    <a class="text-primary" href="{{ route('users.forgot-password') }}">
                        {{ __('Lupa password?') }}
                    </a>
                @endif
            </div>
        </form>
    </div>
</main>
@endsection
