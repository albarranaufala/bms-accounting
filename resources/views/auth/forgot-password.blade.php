@extends('layouts.app')

@section('content')
<main class="bg-secondary w-100 d-flex flex-column align-items-center justify-content-center p-4" style="min-height: 100vh">
    <h1 class="h3 font-weight-bold mb-4 text-uppercase text-white">
        Lupa Password
    </h1>
    <div class="card shadow rounded-lg" style="width: 350px">
        <form action="{{route('users.reset-request')}}" class="card-body p-4" method="POST">
            @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>
            @endif
            @csrf
            <x-inputs.input
                label="Username"
                name="username"
                placeholder="Masukkan username anda"
                required/>

            <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> Kirim Reset Request</button>
        </form>
    </div>
</main>
@endsection
