<!-- Sidebar -->
<ul class="navbar-nav bg-primary sidebar sidebar-dark accordion sticky-top" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="d-flex w-100 px-5 py-3" href="{{route('home')}}">
        <img src="{{ asset('images/bms-logo-white.png') }}" alt="BMS Logo" class="w-100">
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Route::is('home') ? ' active' : '' }}">
        <a class="nav-link" href="{{route('home')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    @role('role:superadmin|supervisor')
    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Manejemen
    </div>
    @endrole

    @role('superadmin')
        <li class="nav-item {{ Route::is('superadmins.index') ? ' active' : '' }}">
            <a href="{{ route('superadmins.index') }}" class="nav-link">
                <i class="fas fa-users-cog"></i>
                <span>Superadmin</span>
            </a>
        </li>
        <li class="nav-item {{ Route::is('companies.*') ? ' active' : '' }}">
            <a href="{{ route('companies.index') }}" class="nav-link">
                <i class="fas fa-cogs"></i>
                <span>Perusahaan</span>
            </a>
        </li>
    @endrole()

    @role('supervisor')
        <li class="nav-item {{ Route::is('companies.edit') ? ' active' : '' }}">
            <a href="{{ route('companies.edit', ['company' => Auth::user()->company_id]) }}" class="nav-link">
                <i class="fas fa-cogs"></i>
                Perusahaan
            </a>
        </li>
        <li class="nav-item {{ Route::is('users.*') ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('users.index') }}">
                <i class="fas fa-users"></i>
                <span>Pegawai</span>
            </a>
        </li>
    @endrole

    {{-- <li class="nav-item {{ Route::is('roles.*') ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('roles.index') }}">
            <i class="fas fa-key"></i>
            <span>Manajemen Akses</span>
        </a>
    </li>

    <li class="nav-item {{ Route::is('companies.*') ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('companies.index') }}">
            <i class="fas fa-th-large"></i>
            <span>Manajemen Mitra</span>
        </a>
    </li> --}}

    
    @role('Divisi Akuntansi|supervisor')
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        Akuntansi
    </div>

    <!-- Nav Item - Laporan Keuangan -->
    <li class="nav-item {{ Route::is('financial_statements.*') ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('financial_statements.index') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Laporan Keuangan</span></a>
    </li>
    
    <!-- Nav Item - Akun -->
    <li class="nav-item {{ Route::is('accounts.*') ? ' active' : '' }}">
        <a class="nav-link" href="{{route('accounts.index')}}">
            <i class="fas fa-fw fa-users"></i>
            <span>Akun</span></a>
        </li>
        
    @endrole

    @role('role:Divisi Akuntansi|Divisi Hutang|Divisi Piutang|Divisi Kas|supervisor')
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Modul
    </div>
    @endrole

    @role('Divisi Akuntansi|Divisi Piutang|supervisor')
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Route::is('receivables.*') ? ' active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReceivable" aria-expanded="true" aria-controls="collapseReceivable">
            <i class="fas fa-fw fa-money-check-alt"></i>
            <span>Piutang</span>
        </a>
        <div id="collapseReceivable" class="collapse {{ Route::is('receivables.*') ? ' show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ Route::is('receivables.journals.*') ? ' bg-secondary text-white font-weight-bold' : '' }}" href="{{route('receivables.journals.index')}}">Jurnal</a>
                <a class="collapse-item {{ Route::is('receivables.customers.*') ? ' bg-secondary text-white font-weight-bold' : '' }}" href="{{route('receivables.customers.index')}}">Pelanggan</a>
            </div>
        </div>
    </li>
    @endrole

    @role('Divisi Akuntansi|Divisi Hutang|supervisor')
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Route::is('debts.*') ? ' active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDebt" aria-expanded="true" aria-controls="collapseDebt">
            <i class="fas fa-fw fa-hand-holding-usd"></i>
            <span>Hutang</span>
        </a>
        <div id="collapseDebt" class="collapse {{ Route::is('debts.*') ? ' show' : '' }}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ Route::is('debts.journals.*') ? ' bg-secondary text-white font-weight-bold' : '' }}" href="{{route('debts.journals.index')}}">Jurnal</a>
                <a class="collapse-item {{ Route::is('debts.clients.*') ? ' bg-secondary text-white font-weight-bold' : '' }}" href="{{route('debts.clients.index')}}">Supplier</a>
            </div>
        </div>
    </li>
    @endrole

    @role('Divisi Akuntansi|Divisi Kas|supervisor')
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Route::is('cashes.*') ? ' active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCash" aria-expanded="true" aria-controls="collapseCash">
            <i class="fas fa-fw fa-university"></i>
            <span>Kas</span>
        </a>
        <div id="collapseCash" class="collapse {{ Route::is('cashes.*') ? ' show' : '' }}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ Route::is('cashes.entries.*') ? ' bg-secondary text-white font-weight-bold' : '' }}" href="{{route('cashes.entries.index')}}">Entry</a>
                <a class="collapse-item {{ Route::is('cashes.banks.*') ? ' bg-secondary text-white font-weight-bold' : '' }}" href="{{route('cashes.banks.index')}}">Bank</a>
            </div>
        </div>
    </li>
    @endrole

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
