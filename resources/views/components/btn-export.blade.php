@props([
    'route',
])

<form action="{{$route}}" {{ $attributes }}>
    <input type="hidden" name="range_type" value="{{request()->range_type ?? ''}}">
    <input type="hidden" name="start_date" value="{{request()->start_date ?? ''}}">
    <input type="hidden" name="end_date" value="{{request()->end_date ?? ''}}">
    <button class="btn btn-secondary" type="submit">
        <i class="fas fa-download"></i>
        <span class="d-none d-lg-inline">
            Export
        </span>
    </button>
</form>
