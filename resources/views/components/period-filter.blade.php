@props([
    'route',
])

<form action="{{$route}}" {{ $attributes }}>
    <button type="submit" name="range_type" value="all"  class="btn {{request()->range_type == 'all' || request()->range_type == null ? 'btn-primary' : 'btn-outline-primary'}} btn-sm rounded-pill mx-1 my-1">Semua waktu</button>
    <button type="submit" name="range_type" value="year" class="btn {{request()->range_type == 'year' ? 'btn-primary' : 'btn-outline-primary'}} btn-sm rounded-pill mx-1 my-1">Tahun ini</button>
    <button type="submit" name="range_type" value="month" class="btn {{request()->range_type == 'month' ? 'btn-primary' : 'btn-outline-primary'}} btn-sm rounded-pill mx-1 my-1">Bulan ini</button>
    <div class="dropright d-inline">
        <a class="mx-1 my-1 btn {{request()->range_type == 'custom' ? 'btn-primary' : 'btn-outline-primary'}} btn-sm rounded-pill dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-cog"></i>
        </a>

        <div class="dropdown-menu animated--fade-in shadow-sm mx-2" aria-labelledby="dropdownMenuLink">
            <div class="card-body">
                <x-inputs.input
                    label="Tanggal Mulai"
                    name="start_date"
                    class="form-control-sm"
                    type="date"
                    :value="request()->start_date ?? date('Y-m-d')"/>
                <x-inputs.input
                    label="Tanggal Akhir"
                    name="end_date"
                    class="form-control-sm"
                    type="date"
                    :value="request()->end_date ?? date('Y-m-d')"/>
                <button class="btn btn-sm btn-primary btn-block" type="submit" name="range_type" value="custom">Tetapkan</button>
            </div>
        </div>
    </div>
</form>
