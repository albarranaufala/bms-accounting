@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        @role('superadmin')
            <div class="mb-4 d-flex align-items-center justify-content-between">
                <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-th"></i> Data Perusahaan</h1>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Perusahaan</th>
                                    <th>Jenis Perusahaan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($companies as $company)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->company_type }}</td>
                                        <td class="text-center d-flex">
                                            <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v text-gray-500"></i>
                                            </a>
                                            <!-- Dropdown - Companies Information -->
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                            <a class="dropdown-item" href="{{ route('companies.edit', ['company' => $company]) }}">
                                                <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Lihat Data Perusahaan
                                            </a>
                                            <form action="{{ route('companies.destroy', ['company' => $company]) }}" method="POST">
                                                @csrf @method('DELETE')
                                                <button class="dropdown-item">
                                                    <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                    Hapus Data
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endrole
        @role('supervisor')
            <div class="p-3">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ route('receivables.journals.index') }}" class="card border-left-primary shadow h-100 py-2 card-link">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Data Piutang Belum Lengkap
                                        </div>
                                        <div class="h5 font-weight-bold text-gray-800 my-4">
                                            {{ $receivablesCount }} Data
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('debts.journals.index') }}" class="card border-left-info shadow h-100 py-2 card-link">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                            Data Hutang Belum Lengkap
                                        </div>
                                        <div class="h5 font-weight-bold text-gray-800 my-4">
                                            {{ $debtsCount }} Data
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('cashes.entries.index') }}" class="card border-left-danger shadow h-100 py-2 card-link">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                            Data Kas Belum Lengkap
                                        </div>
                                        <div class="h5 font-weight-bold text-gray-800 my-4">
                                            {{ $cashesCount }} Data
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        @endrole
        @role('Divisi Piutang')
        <div class="p-3">
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route('receivables.journals.index') }}" class="card border-left-primary shadow h-100 py-2 card-link">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Data Piutang Belum Lengkap
                                    </div>
                                    <div class="h5 font-weight-bold text-gray-800 my-4">
                                        {{ $receivablesCount }} Data
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        @endrole
        @role('Divisi Hutang')
        <div class="p-3">
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route('debts.journals.index') }}" class="card border-left-info shadow h-100 py-2 card-link">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                        Data Hutang Belum Lengkap
                                    </div>
                                    <div class="h5 font-weight-bold text-gray-800 my-4">
                                        {{ $debtsCount }} Data
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        @endrole
        @role('Divisi Kas')
        <div class="p-3">
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route('cashes.entries.index') }}" class="card border-left-danger shadow h-100 py-2 card-link">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                        Data Kas Belum Lengkap
                                    </div>
                                    <div class="h5 font-weight-bold text-gray-800 my-4">
                                        {{ $cashesCount }} Data
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        @endrole
    </div>
@endsection

@push('scripts')
@endpush
