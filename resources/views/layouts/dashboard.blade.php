@extends('layouts.app')

@section('content')
    <div id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <aside>
                <x-sidebar/>
            </aside>

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">
                    <header>
                        <x-topbar/>
                    </header>

                    <main>
                        @yield('dashboard-content')
                    </main>

                </div>
                <!-- End of Main Content -->

                <x-footer/>

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

    </div>
@endsection
