@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-users-cog"></i> Akses Superadmin</h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#createSuperadminModal" aria-expanded="false" aria-controls="collapseForm">
                <i class="fas fa-plus"></i> Tambah Superadmin
            </button>
        </div>

        {{-- data table --}}
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($superadmins as $s)
                                <tr>
                                    <td>{{ $s->id }}</td>
                                    <td>{{ $s->name }}</td>
                                    <td>{{ $s->username }}</td>
                                    <td>{{ $s->email }}</td>
                                    <td class="text-center d-flex">
                                        <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v text-gray-500"></i>
                                        </a>
                                        <!-- Dropdown - Companies Information -->
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        <button class="dropdown-item" data-toggle="modal" data-target="#editSuperadminModal">
                                            <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Edit Data
                                        </button>
                                        <form action="{{ route('users.destroy', ['user' => $s]) }}" method="POST">
                                            @csrf @method('DELETE')
                                            <button class="dropdown-item">
                                                <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                Hapus Data
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modals')
    <!-- Modal -->
    <div class="modal fade" id="createSuperadminModal" tabindex="-1" role="dialog" aria-labelledby="createSuperadminModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{ route('users.store') }}" method="POST" class="modal-content">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Superadmin Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <x-inputs.input
                        name="role_id"
                        value="1"
                        hidden
                    />
                    <x-inputs.input
                        label="Nama"
                        name="name"
                        placeholder="Masukkan Nama Superadmin"
                        required
                    />
                    <x-inputs.input
                        label="Username"
                        name="username"
                        placeholder="Masukkan Username Superadmin"
                        required
                    />
                    <x-inputs.input
                        required
                        label="Email"
                        name="email"
                        type="email"
                        placeholder="Masukkan Email Superadmin"
                    />
                    <x-inputs.input
                        label="Kata Sandi Default"
                        name="password"
                        disabled
                        value="SecurePassword2020"
                    />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>

    <!-- Edit Superadmin Model -->
    <div class="modal fade" id="editSuperadminModal" tabindex="-1" role="dialog" aria-labelledby="editSuperadminModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{ route('users.update') }}" method="POST" class="modal-content">
                @csrf @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Akses Superadmin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <x-inputs.input
                        name="role_id"
                        value="1"
                        hidden
                    />
                    <x-inputs.input
                        name="user_id"
                        id="user_id"
                        hidden
                    />
                    <x-inputs.input
                        label="Nama"
                        name="name"
                        id="name"
                        placeholder="Masukkan nama superadmin"
                        required
                    />
                    <x-inputs.input
                        label="Username"
                        name="username"
                        id="username"
                        placeholder="Masukkan username superadmin"
                        required
                    />
                    <x-inputs.input
                        required
                        label="Email"
                        name="email"
                        id="email"
                        type="email"
                        placeholder="Masukkan email Superadmin"
                    />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
@endpush

@push('scripts')
    <script>
        $(document).ready( function () {
            let table = $('#dataTable').DataTable();
            $('#dataTable tbody').on('click', 'tr', function () {
                data = table.row(this).data();
                $('#editSuperadminModal').on('show.bs.modal', function () {
                    $('#user_id').val(data[0]);
                    $('#name').val(data[1]);
                    $('#username').val(data[2]);
                    $('#email').val(data[3]);
                })
            })
        })
    </script>
@endpush