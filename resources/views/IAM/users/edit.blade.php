@extends('layouts.dashboard')

@section('dashboard-content')
<div class="p-4">
    <!-- Page Heading -->
    <div class="mb-2 d-flex align-items-center justify-content-between">
        <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-user"></i> Edit Data {{ $user->name }}</h1>
    </div>
    <a href="{{ route('users.index') }}">&#8592; Batal</a>
    
    <div class="card mt-4">
        <form action="{{ route('users.update',['user' => $user]) }}" method="post">
            @csrf @method('PUT')
            <div class="card-body">
                <div class="row">
                    <x-inputs.input
                        name="user_id"
                        value="{{ $user->id }}"
                        hidden
                    />
                    <div class="col-md-6">
                        <x-inputs.input
                            label="Nama"
                            name="name"
                            type="text"
                            placeholder="Masukkan Nama User Baru"
                            value="{{ old('name',$user->name) }}"
                            required
                        />
                        <x-inputs.input
                            label="Username"
                            name="username"
                            type="text"
                            placeholder="Username untuk user baru"
                            value="{{ old('username',$user->username) }}"
                            required
                        />
                    </div>
                    <div class="col-md-6">
                        <x-inputs.input
                            label="Email"
                            name="email"
                            type="text"
                            placeholder="Email untuk user baru"
                            value="{{ old('email',$user->email) }}"
                            required
                        />
                        <x-inputs.select
                            label="Role"
                            name="role_id"
                            placeholder="Pilih role user"
                            search
                            value="{{ $user->roles[0]->id }}"
                            :option-values="$roles->map(fn($role)=>$role->id)->filter(function($role, $key){return $key > 1;})"
                            :option-names="$roles->map(fn($role)=>$role->name)->filter(function($role, $key){return $key > 1;})"
                        />
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary float-right"> Perbarui Data</button>
                        <a class="btn btn-light float-right mx-2"> Reset Password User</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection