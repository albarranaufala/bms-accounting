@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-users"></i> Data Pegawai Perusahaan</h1>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#createUser" aria-expanded="false" aria-controls="collapseForm">
                <i class="fas fa-plus"></i> Tambah Pegawai Baru
            </button>

        </div>
        
        <div class="card mb-4 collapse" id="createUser">
            <div class="card-body">
                <form action="{{ route('users.store') }}" method="POST">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <h4>Tambah User Baru</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <x-inputs.input
                                label="Nama"
                                name="name"
                                type="text"
                                placeholder="Masukkan Nama User Baru"
                                required
                            />
                            <x-inputs.input
                                label="Username"
                                name="username"
                                type="text"
                                placeholder="Username untuk user baru"
                                required
                            />
                            <x-inputs.input
                                label="Email"
                                name="email"
                                type="text"
                                placeholder="Email untuk user baru"
                                required
                            />
                        </div>
                        <div class="col-md-6">
                            <x-inputs.input
                                disabled
                                label="Default password untuk user baru adalah:"
                                name="password"
                                type="password"
                                placeholder="SecurePassword2020"
                            />
                            <x-inputs.select
                                label="Role"
                                name="role_id"
                                placeholder="Pilih role user"
                                search
                                :option-values="$roles->map(fn($role)=>$role->id)->filter(function($role, $key){return $key > 1;})"
                                :option-names="$roles->map(fn($role)=>$role->name)->filter(function($role, $key){return $key > 1;})"
                            />
                            <x-inputs.input
                                name="company_id"
                                value="{{ Auth::user()->company_id }}"
                                hidden
                            />
                            <button class="btn btn-primary float-right"> Tambah User</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Company</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->roles->map(function ($role){return $role->name;})->implode(', ') }}</td>
                                    <td>{{ $user->company->name }}</td>
                                    <td class="text-center">
                                        <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v text-gray-500"></i>
                                        </a>
                                        <!-- Dropdown - User Information -->
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        <a class="dropdown-item" href="{{ route('users.edit', ['user' => $user]) }}">
                                            <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Edit Data
                                        </a>
                                        <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST">
                                            @csrf @method('DELETE')
                                            <button class="dropdown-item">
                                                <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @if ($requesting_reset->count() > 0)
            <div class="mb-4 d-flex align-items-center justify-content-between">
                <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-key"></i> Permintaan Reset Password</h1>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover data-table" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($requesting_reset as $req)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $req->name }}</td>
                                        <td>{{ $req->username }}</td>
                                        <td>{{ $req->email }}</td>
                                        <td>{{ $req->roles->map(function ($role){return $role->name;})->implode(', ') }}</td>
                                        <td class="text-center">
                                            <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v text-gray-500"></i>
                                            </a>
                                            <!-- Dropdown - User Information -->
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                                <form action="{{ route('users.reset-password', ['user' => $req->id]) }}" method="POST">
                                                    @csrf
                                                    <button class="dropdown-item">
                                                        <i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>
                                                        Reset Password
                                                    </button>
                                                </form>
                                                <form action="{{ route('users.reject-request', ['user' => $req->id]) }}" method="POST">
                                                    @csrf
                                                    <button class="dropdown-item">
                                                        <i class="fas fa-trash text-gray-400 mr-2"></i>
                                                        Abaikan
                                                    </button>
                                                </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
