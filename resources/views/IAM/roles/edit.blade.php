@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-key"></i> Ubah Akses Divisi</h1>
        </div>

        <div class="card">
            <form action="{{ route('roles.update',['role' => $role]) }}" method="POST">
                @csrf @method('PUT')
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <x-inputs.input
                            required
                            label="Nama Role"
                            name="name"
                            type="text"
                            value="{{old('name',$role->name)}}"
                            placeholder="Masukkan nama divisi"/>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="permissions">Akses <span class="text-danger">*</span></label>
                                <select name="permissions[]" id="permissions" class="form-control selectpicker" title="Akses untuk divisi" multiple>
                                    @foreach ($permissions as $permission)
                                        <option value="{{ $permission->name }}"{{ $rolePermissions->contains($permission->name) ? ' selected' : '' }}>{{ $permission->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-primary float-right" type="submit">Simpan Perubahan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="my-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-users"></i> User di Divisi {{ $role->name }}</h1>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>NOT SET</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection