@extends('layouts.dashboard')

@section('dashboard-content')
<div class="p-4">
    <!-- Page Heading -->
    <div class="mb-4 d-flex align-items-center justify-content-between">
        <h1 class="h3 text-primary font-weight-bold mb-0"><i class="fas fa-key"></i> Manajemen Akses</h1>
        <button class="btn btn-primary" data-toggle="collapse" data-target="#createRole" aria-expanded="false" aria-controls="collapseForm">
            <i class="fas fa-plus"></i> Tambah Divisi Baru
        </button>
    </div>

    {{-- create roles --}}
    <div class="card my-3 collapse" id="createRole">
        <div class="card-body">
            <form action="{{ route('roles.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <h4>Tambah Divisi Baru</h4>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <x-inputs.input
                            required
                            label="Nama Role"
                            name="name"
                            type="text"
                            value="{{old('name','')}}"
                            placeholder="Masukkan nama divisi"/>
                    </div>
                    <div class="col-md-6">
                        <x-inputs.select
                            required
                            label="Akses"
                            name="permissions"
                            placeholder="Pilih Akses untuk divisi"
                            search
                            multiple
                            :option-values="$permissions->map(fn($permission)=>$permission->name)"
                            :option-names="$permissions->map(fn($permission)=>$permission->name)"
                        />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary float-right">
                            Buat Divisi
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Card Table --}}
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Divisi</th>
                            <th>Akses</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->permissions->map(function ($permission) { return $permission->name; })->implode(', ') }}</td>
                                <td class="text-center">
                                    <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v text-gray-500"></i>
                                    </a>
                                    <!-- Dropdown - Role Information -->
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                    <a class="dropdown-item" href="{{ route('roles.edit', ['role' => $role]) }}">
                                        <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Lihat Role
                                    </a>
                                    <form action="{{ route('roles.destroy', ['role' => $role->id]) }}" method="POST">
                                        @csrf @method('DELETE')
                                        <button class="dropdown-item">
                                            <i class="fas fa-trash text-gray-400 mr-2"></i>
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
