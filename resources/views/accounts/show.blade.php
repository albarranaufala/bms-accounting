@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center">
            <h1 class="h3 text-primary font-weight-bold mb-0">Buku Besar</h1>
        </div>
        <x-period-filter :route="route('accounts.show', $account->id)" class="mb-4"/>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-12 d-flex justify-content-between flex-column flex-lg-row">
                        <div>
                            <h2 class="h4 font-weight-bold">{{$account->name}}</h2>
                            <p class="text-gray-500 mb-0">Kode akun: {{$account->account_num}}</p>
                        </div>
                        <div class="mt-3 mt-lg-0 text-left text-lg-right">
                            <a href="{{route('accounts.destroy', $account->id)}}" class="btn btn-danger delete" type="button">
                                <i class="fas fa-trash"></i>
                                <span class="d-none d-lg-inline">
                                    Hapus
                                </span>
                            </a>
                            <a class="btn btn-success" href="{{route('accounts.edit', $account->id)}}">
                                <i class="fas fa-edit"></i>
                                <span class="d-none d-lg-inline">
                                    Sunting Akun
                                </span>
                            </a>
                            <x-btn-export :route="route('receivables.customers.show.export.excel', $account->id)" class="mt-1 d-inline"/>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 text-right">
                        <p class="h5 font-weight-bold">Saldo Sebelumnya: Rp <span class="mask-money">{{$account->previousBalance()}}</span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" width="100%" cellspacing="0">
                                <thead class="text-uppercase small text-primary">
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal</th>
                                    <th>Transaksi</th>
                                    <th class="text-right">Debit</th>
                                    <th class="text-right">Kredit</th>
                                    <th class="text-right">Saldo</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $currentBalance = $account->previousBalance();
                                @endphp
                                @forelse ($account->currentTransactions() as $transaction)
                                    @php
                                        $currentBalance += ($transaction->bill_value ?? $transaction->payment_value) * ($transaction->type === 'debit' ? 1 : -1);
                                    @endphp
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>
                                            {{(new Carbon\Carbon($transaction->date))->format('d M Y')}}
                                        </td>
                                        <td>
                                            {{$transaction->description ?? '-'}}
                                        </td>
                                        <td class="text-right">
                                            @if ($transaction->type === 'debit')
                                            Rp <span class="mask-money">{{$transaction->bill_value ?? $transaction->payment_value}}</span>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if ($transaction->type === 'credit')
                                                Rp <span class="mask-money">{{$transaction->bill_value ?? $transaction->payment_value}}</span>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            Rp <span class="mask-money">{{$currentBalance}}</span>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="6">
                                            Belum ada transaksi
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Logout Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data ini dan data lain yang terhubung akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Topbar -->
    </div>
@endpush
