@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Tambah Akun Baru</h1>
        </div>

        <div class="card shadow-sm">
            <form class="card-body" action="{{route('accounts.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.select
                            label="Akun Header"
                            id="header"
                            name="header"
                            placeholder="Pilih akun header"
                            search
                            :option-values="$accounts->where('account_type', 'header')->map(fn($account)=>$account->id)"
                            :option-names="$accounts->where('account_type', 'header')->map(fn($account)=>$account->account_num.' - '.$account->name)"></x-inputs.select>
                        <x-inputs.input
                            label="Kode Akun"
                            id="account_num"
                            name="account_num"
                            required
                            placeholder="Masukkan kode akun"></x-inputs.input>
                        <x-inputs.input
                            label="Nama Akun"
                            name="name"
                            required
                            placeholder="Masukkan name akun"></x-inputs.input>
                    </div>
                    <div class="col-md-6">
                        <x-inputs.select
                            required
                            label="Jenis Akun"
                            name="account_type"
                            placeholder="Pilih jenis akun"
                            search
                            :option-values="['header', 'account', 'sub_total', 'total', 'grand_total']"
                            :option-names="['Header', 'Akun', 'Subjumlah', 'Jumlah', 'Total']"></x-inputs.select>
                        <x-inputs.select
                            label="Jenis Saldo"
                            name="balance_type"
                            placeholder="Pilih jenis saldo"
                            search
                            :option-values="['credit', 'debit']"
                            :option-names="['Kredit', 'Debit']"></x-inputs.select>
                        <x-inputs.input
                            label="Saldo Awal"
                            name="balance"
                            required
                            type="number"
                            step="0.001"
                            placeholder="Masukkan saldo awal"></x-inputs.input>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <a href="{{route('accounts.index')}}" class="btn btn-light mr-3">Batal</a>
                        <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah Akun</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready( function () {
            const accounts = @json($accounts);
            $('#header').change(function(){
                if($(this).val().length){
                    const headerAccountId = $('#header').val();
                    const account = accounts.find((acc)=> acc.id === parseInt(headerAccountId));
                    $('#account_num').val(account.account_num);
                }
            });
        });
    </script>
@endpush
