@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex flex-column align-items-start flex-md-row justify-content-md-between align-items-md-center">
            <h1 class="h3 text-primary font-weight-bold mb-0">Daftar Akun</h1>
            <div class="mt-3 mt-md-0 d-flex">
                <a href="{{route('accounts.create')}}" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah Akun Baru
                </a>
            </div>
        </div>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                        <tr class="text-uppercase small text-primary">
                            <th>Kode</th>
                            <th>Nama Akun</th>
                            <th>Jenis Akun</th>
                            <th>Jenis Saldo</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr class="text-uppercase small text-primary">
                            <th>Kode</th>
                            <th>Nama Akun</th>
                            <th>Jenis Akun</th>
                            <th>Jenis Saldo</th>
                            <th>Aksi</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach ($accounts as $account)
                            <tr>
                                <td>{{$account->account_num}}</td>
                                <td>
                                    {{$account->name}}
                                </td>
                                <td>
                                    {{\App\Models\Account::ACCOUNT_TYPE[$account->account_type] ?? '-'}}
                                </td>
                                <td>{{\App\Models\Account::BALANCE_TYPE[$account->balance_type] ?? '-'}}</td>
                                <td class="text-center">
                                    <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v text-gray-500"></i>
                                    </a>
                                    <!-- Dropdown - User Information -->
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        <a class="dropdown-item" href="{{route('accounts.show', $account->id)}}">
                                            <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Lihat Buku Besar
                                        </a>
                                        <a class="dropdown-item" href="{{route('accounts.edit', $account->id)}}">
                                            <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Sunting
                                        </a>
                                        <a class="dropdown-item delete" href="{{route('accounts.destroy', $account->id)}}">
                                            <i class="fas fa-trash fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Hapus
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Logout Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Topbar -->
    </div>
@endpush
