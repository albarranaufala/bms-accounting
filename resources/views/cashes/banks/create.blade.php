@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Tambah Data Bank</h1>
        </div>

        <div class="card shadow-sm">
            <form class="card-body" action="{{route('cashes.banks.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.input
                            label="Nama"
                            name="name"
                            placeholder="Masukkan nama bank"
                            required />
                        <x-inputs.select
                            required
                            label="Tipe"
                            name="type"
                            placeholder="Pilih tipe"
                            search
                            :option-values="$bankTypes->map(fn($type)=>$type->id)"
                            :option-names="$bankTypes->map(fn($type)=>$type->name)"
                        />
                        <div class="w-100 text-right mb-3">
                            <a class="small" href="" data-toggle="modal" data-target="#addTypeModal">
                                + Tambah Tipe
                            </a>
                        </div>
                        <x-inputs.input
                            label="Kode"
                            required
                            name="code"
                            placeholder="Masukkan kode bank"/>
                    </div>
                    <div class="col-md-6">
                        <x-inputs.input
                            label="No. Rekening"
                            name="bank_account"
                            placeholder="Masukkan no. rekening"/>
                        <x-inputs.input
                            label="Pemilik Rekening"
                            name="owner_name"
                            placeholder="Masukkan pemilik rekening"/>
                        <x-inputs.input
                            label="Kelas"
                            name="class"
                            placeholder="Masukkan kelas"/>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <x-inputs.input
                            required
                            label="Saldo Awal"
                            name="balance"
                            type="number"
                            step="0.001"
                            placeholder="Masukkan saldo awal bank"/>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <a href="{{route('cashes.banks.index')}}" class="btn btn-light mr-3">Batal</a>
                        <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah Data Bank</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal Add Type -->
    <div class="modal fade" id="addTypeModal" tabindex="-1" role="dialog" aria-labelledby="addTypeModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{route('cashes.banks.types.store')}}" class="modal-content" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="addTypeModalTitle">Tambah Tipe</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <x-inputs.input
                        required
                        label="Nama Tipe"
                        name="type_name"
                        placeholder="Masukkan nama tipe bank"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save mr-2"></i>Tambah Tipe
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
