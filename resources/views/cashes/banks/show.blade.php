@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center">
            <h1 class="h3 text-primary font-weight-bold mb-0">Kartu Bank</h1>
        </div>
        <x-period-filter :route="route('cashes.banks.show', $bank->id)" class="mb-4"/>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-12 d-flex justify-content-between flex-column flex-lg-row">
                        <div>
                            <h2 class="h4 font-weight-bold">{{$bank->name}} ({{$bank->code}})</h2>
                            <p class="text-gray-500 mb-0">{{$bank->type->name}}</p>
                        </div>
                        <div class="mt-3 mt-lg-0 text-left text-lg-right">
                            <a href="{{route('cashes.banks.destroy', $bank->id)}}" class="btn btn-danger delete" type="button">
                                <i class="fas fa-trash"></i>
                                <span class="d-none d-lg-inline">
                                    Hapus
                                </span>
                            </a>
                            <a class="btn btn-success" href="{{route('cashes.banks.edit', $bank->id)}}">
                                <i class="fas fa-edit"></i>
                                <span class="d-none d-lg-inline">
                                    Sunting Bank
                                </span>
                            </a>
                            <br>
                            <x-btn-export :route="route('cashes.banks.show.export.excel', $bank->id)" class="mt-1"/>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">No. Rekening</p>
                            <p class="font-weight-bold mb-0">{{$bank->bank_account ?? '-'}}</p>
                        </div>
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Pemilik Rekening</p>
                            <p class="font-weight-bold mb-0">{{$bank->owner_name ?? '-'}}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <p class="text-gray-500 mb-0">Kelas</p>
                            <p class="font-weight-bold mb-0">{{$bank->class ?? '-'}}</p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover data-table" width="100%" cellspacing="0">
                                <thead class="text-uppercase small text-primary">
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Uraian</th>
                                    <th>No. Bukti</th>
                                    <th>Masuk</th>
                                    <th>Keluar</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th class="text-right" colspan="3">Total Masuk/Keluar</th>
                                    <th class="text-right">Rp <span class="mask-money">{{$bank->totalBill ?? 0}}</span></th>
                                    <th class="text-right">Rp <span class="mask-money">{{$bank->totalPayment ?? 0}}</th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="3">Saldo Awal</th>
                                    <th class="text-right" colspan="2">Rp <span class="mask-money">{{$bank->billRemainingBefore ?? 0}}</span></th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="3">Saldo Berjalan</th>
                                    <th class="text-right" colspan="2">Rp <span class="mask-money">{{$bank->billRemaining ?? 0}}</span></th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="3">Saldo Akhir</th>
                                    <th class="text-right" colspan="2">Rp <span class="mask-money">{{($bank->billRemaining + $bank->billRemainingBefore) ?? 0}}</span></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach ($bank->cashes as $cash)
                                    <tr>
                                        <td>
                                            {{(new Carbon\Carbon($cash->date))->format('d M Y')}}
                                        </td>
                                        <td>
                                            {{$cash->description}}
                                        </td>
                                        <td>{{$cash->invoice_num ?? '-'}}</td>
                                        <td class="text-right">Rp <span class="mask-money">{{$cash->bill_value ?? 0}}</span></td>
                                        <td class="text-right">Rp <span class="mask-money">{{$cash->payment_value ?? 0}}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Logout Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data ini dan data lain yang terhubung akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Topbar -->
    </div>
@endpush
