<table>
    <thead>
        <tr>
            <th colspan="5">
                <strong>Kartu Bank/Kas</strong>
            </th>
        </tr>
        <tr>
            <th colspan="5">
                <strong>{{$rangeDate}}</strong>
            </th>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>Bank/Kas:</td>
            <td><strong>{{$bank->name}}</strong></td>
        </tr>
        <tr>
            <td>Code:</td>
            <td><strong>{{$bank->code}}</strong></td>
        </tr>
        <tr></tr>
        <tr>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>No. Bukti</th>
            <th>Masuk</th>
            <th>Keluar</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($bank->cashes as $cash)
        <tr>
            <td>
                {{(new Carbon\Carbon($cash->date))->format('d M Y')}}
            </td>
            <td>
                {{$cash->description}}
            </td>
            <td>{{$cash->invoice_num ?? '-'}}</td>
            <td>{{$cash->bill_value ?? 0}}</td>
            <td>{{$cash->payment_value ?? 0}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th>Total</th>
            <th>{{$bank->totalBill ?? 0}}</th>
            <th>{{$bank->totalPayment ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Saldo Berjalan</th>
            <th></th>
            <th>{{$bank->billRemaining ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Saldo Awal</th>
            <th></th>
            <th>{{$bank->billRemainingBefore ?? 0}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Saldo Akhir</th>
            <th></th>
            <th>{{($bank->billRemaining + $bank->billRemainingBefore) ?? 0}}</th>
        </tr>
    </tfoot>
</table>
