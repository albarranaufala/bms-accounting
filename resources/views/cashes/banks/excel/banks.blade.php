<table>
    <tbody>
        <tr>
            <td colspan="7">
                <strong>Rekapitulasi Kas/Bank</strong>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <strong>{{$rangeDate}}</strong>
            </td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Nama</td>
            <td>Pemilik Rekening</td>
            <td>Saldo Awal</td>
            <td>Masuk</td>
            <td>Keluar</td>
            <td>Saldo Akhir</td>
        </tr>
        @foreach ($banks as $bank)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$bank->name}} ({{$bank->code}})
                </td>
                <td>{{$bank->owner_name}}</td>
                <td>{{$bank->billRemainingBefore ?? 0}}</td>
                <td>{{$bank->totalBill ?? 0}}</td>
                <td>{{$bank->totalPayment ?? 0}}</td>
                <td>{{($bank->billRemainingBefore + $bank->billRemaining) ?? 0}}</td>
            </tr>
        @endforeach
        <tr>
            @php
                $billRemainingBefore = $banks->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                $totalBill = $banks->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                $totalPayment = $banks->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                $grandTotalBill = $banks->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
            @endphp
            <td></td>
            <td></td>
            <th>Total</th>
            <td>{{($billRemainingBefore) ?? 0}}</td>
            <td>{{($totalBill) ?? 0}}</td>
            <td>{{($totalPayment) ?? 0}}</td>
            <td>{{($grandTotalBill) ?? 0}}</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Nama Tipe</td>
            <td>Saldo Awal</td>
            <td>Masuk</td>
            <td>Keluar</td>
            <td>Saldo Akhir</td>
        </tr>
        @foreach ($bankTypes as $type)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$type->name}}
                </td>
                @php
                    $type->billRemainingBefore = $type->banks->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                    $type->totalBill = $type->banks->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                    $type->totalPayment = $type->banks->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                    $type->billRemaining = $type->banks->reduce(fn($sum, $item)=>$sum+$item->billRemaining);
                    $type->grandTotalBill = $type->billRemaining + $type->billRemainingBefore;
                @endphp
                <td>{{($type->billRemainingBefore) ?? 0}}</td>
                <td>{{($type->totalBill) ?? 0}}</td>
                <td>{{($type->totalPayment) ?? 0}}</td>
                <td>{{($type->grandTotalBill) ?? 0}}</td>
            </tr>
        @endforeach
        <tr>
            @php
                $billRemainingBefore = $bankTypes->reduce(fn($sum, $item)=>$sum+$item->billRemainingBefore);
                $totalBill = $bankTypes->reduce(fn($sum, $item)=>$sum+$item->totalBill);
                $totalPayment = $bankTypes->reduce(fn($sum, $item)=>$sum+$item->totalPayment);
                $grandTotalBill = $bankTypes->reduce(fn($sum, $item)=>$sum+$item->billRemaining) + $billRemainingBefore;
            @endphp
            <td></td>
            <td>Total</td>
            <td>{{($billRemainingBefore) ?? 0}}</td>
            <td>{{($totalBill) ?? 0}}</td>
            <td>{{($totalPayment) ?? 0}}</td>
            <td>{{($grandTotalBill) ?? 0}}</td>
        </tr>
    </tbody>
</table>
