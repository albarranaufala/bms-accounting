<table>
    <tbody>
    <tr>
        <td colspan="7">
            <strong>Entry Kas dan Bank</strong>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <strong>{{$rangeDate}}</strong>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <td>No.</td>
        <td>Tanggal</td>
        <td>Nama Bank</td>
        <td>Uraian</td>
        <td>Masuk</td>
        <td>Keluar</td>
    </tr>
    @foreach ($cashes as $cash)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>
                {{(new Carbon\Carbon($cash->date))->format('d M Y')}}
            </td>
            <td>
                @if ($cash->bank)
                    {{$cash->bank->name}} ({{$cash->bank->code}})
                @endif
            </td>
            <td>{{$cash->description}}</td>
            <td>{{$cash->bill_value ?? 0}}</td>
            <td>{{$cash->payment_value ?? 0}}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{$cashes->reduce(fn($sum,$item)=>$sum+$item->bill_value)}}</td>
        <td>{{$cashes->reduce(fn($sum,$item)=>$sum+$item->payment_value)}}</td>
    </tr>
    </tbody>
</table>
