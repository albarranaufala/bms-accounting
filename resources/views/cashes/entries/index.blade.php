@extends('layouts.dashboard')

@section('dashboard-content')
    <div class="p-4">
        <!-- Page Heading -->
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1 class="h3 text-primary font-weight-bold mb-0">Entry Kas dan Bank</h1>
        </div>

        <div class="mb-4 d-flex flex-column align-items-end flex-md-row justify-content-md-between align-items-md-center">
            <div class="align-self-start">
                <x-period-filter :route="route('cashes.entries.index')" />
            </div>
            <div class="mt-3 mt-md-0 d-flex">
                <x-btn-export :route="route('cashes.entries.index.export.excel')" class="mr-1"/>
                <a href="{{route('cashes.entries.create')}}" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah Transaksi Kas dan Bank
                </a>
            </div>
        </div>

        <div class="card shadow-sm">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover data-table" width="100%" cellspacing="0">
                        <thead>
                        <tr class="text-uppercase small text-primary">
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Nama Kas/Bank</th>
                            <th>Uraian</th>
                            <th>Masuk</th>
                            <th>Keluar</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th class="text-right" colspan="4">Total</th>
                            <th class="text-right">Rp <span class="mask-money">{{$cashes->reduce(fn($sum,$item)=>$sum+$item->bill_value)}}</span></th>
                            <th class="text-right">Rp <span class="mask-money">{{$cashes->reduce(fn($sum,$item)=>$sum+$item->payment_value)}}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach ($cashes as $cash)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    {{(new Carbon\Carbon($cash->date))->format('d M Y')}}
                                </td>
                                <td>
                                    @if ($cash->bank)
                                        {{$cash->bank->name}} <br>
                                        <small class="text-gray-500">{{$cash->bank->code}}</small>
                                    @else
                                        <div class="badge badge-danger rounded-pill px-2 py-1">Unknown</div>
                                    @endif
                                </td>
                                <td>{{$cash->description ?? '-'}}</td>
                                <td class="text-right">Rp <span class="mask-money">{{$cash->bill_value ?? 0}}</span></td>
                                <td class="text-right">Rp <span class="mask-money">{{$cash->payment_value ?? 0}}</span></td>
                                <td class="text-center">
                                    @if($cash->is_complete)
                                        <div class="badge badge-success rounded-pill px-2 py-1">Complete</div>
                                    @else
                                        <div class="badge badge-danger rounded-pill px-2 py-1">Incomplete</div>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v text-gray-500"></i>
                                    </a>
                                    <!-- Dropdown - User Information -->
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="actionDropdown">
                                        @if ($cash->bank)
                                            <a class="dropdown-item" href="{{route('cashes.banks.show', $cash->bank->id)}}">
                                                <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
                                                Lihat Bank
                                            </a>
                                        @endif
                                        <a class="dropdown-item" href="{{route('cashes.entries.edit', $cash->id)}}">
                                            <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Sunting
                                        </a>
                                        <a class="dropdown-item delete" href="{{route('cashes.entries.destroy', $cash->id)}}">
                                            <i class="fas fa-trash fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Hapus
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('body').on('click', '.delete', function (event) {
                event.preventDefault();
                let action = $(this).attr('href');
                $('#deleteForm').attr('action', action);
                $('#deleteModal').modal('show');
            });
        });
    </script>
@endpush

@push('modals')
    <div>
        <!-- Delete Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="deleteForm" class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Apakah Anda Yakin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Data akan terhapus permanen jika anda menghapusnya.</div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash mr-1"></i>
                            Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End of Modal -->
    </div>
@endpush
