<?php


namespace App\Repositories\Receivable\Customer;


use App\Models\Customer;
use App\Services\CustomerService;
use Illuminate\Support\Facades\Auth;

class CustomerRepository implements CustomerRepositoryInterface
{
    public function getAll()
    {
        return Customer::where('company_id', Auth::user()->company->id)->get();
    }

    public function getAllByRange($data)
    {
        return $this->getAll()
            ->map(function($customer) use ($data){
                return (new CustomerService())
                    ->setRangeType($data['range_type'])
                    ->setStartDate($data['start_date'])
                    ->setEndDate($data['end_date'])
                    ->setCustomer($customer)
                    ->getCustomerWithFinancialInfo();
            });
    }

    public function findById($id)
    {
        return Customer::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
    }

    public function findByIdAndRange($id, $data)
    {
        return (new CustomerService())
            ->setCustomer($this->findById($id))
            ->setRangeType($data['range_type'])
            ->setStartDate($data['start_date'])
            ->setEndDate($data['end_date'])
            ->getCustomerWithFinancialInfo();
    }

    public function create($data)
    {
        return Customer::create([
            'name' => $data['name'] ?? null,
            'code' => $data['code'] ?? null,
            'npwp' => $data['npwp'] ?? null,
            'address' => $data['address'] ?? null,
            'phone' => $data['phone'] ?? null,
            'email' => $data['email'] ?? null,
            'contact_person' => $data['contact_person'] ?? null,
            'balance' => $data['balance'] ?? null,
            'type_id' => $data['type'] ?? null,
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function update($id, $data)
    {
        return $this->findById($id)
            ->update([
                'name' => $data['name'] ?? null,
                'code' => $data['code'] ?? null,
                'npwp' => $data['npwp'] ?? null,
                'address' => $data['address'] ?? null,
                'phone' => $data['phone'] ?? null,
                'email' => $data['email'] ?? null,
                'contact_person' => $data['contact_person'] ?? null,
                'balance' => $data['balance'] ?? null,
                'type_id' => $data['type'] ?? null,
                'company_id' => Auth::user()->company->id,
            ]);
    }

    public function delete($id)
    {
        $customer = $this->findById($id);
        $customer->receivables()->delete();

        return $customer->delete();
    }

    public function forceDelete($id)
    {
        return $this->findById($id)
            ->forceDelete();
    }
}
