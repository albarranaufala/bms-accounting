<?php


namespace App\Repositories\Receivable\Customer;


interface CustomerRepositoryInterface
{
    public function getAll();

    public function getAllByRange($data);

    public function findById($id);

    public function findByIdAndRange($id, $data);

    public function create($data);

    public function update($id, $data);

    public function delete($id);

    public function forceDelete($id);
}
