<?php


namespace App\Repositories\Receivable\Customer;


use App\Models\CustomerType;
use App\Services\CustomerService;
use Illuminate\Support\Facades\Auth;

class CustomerTypeRepository implements CustomerTypeRepositoryInterface
{

    public function getAll()
    {
        return CustomerType::where('company_id', Auth::user()->company->id)->get();
    }

    public function getAllByRange($data)
    {
        return $this->getAll()
            ->map(function ($type) use ($data){
                $type->customers = $type->customers
                    ->map(function($customer) use ($data){
                        return (new CustomerService())
                            ->setRangeType($data['range_type'])
                            ->setStartDate($data['start_date'])
                            ->setEndDate($data['end_date'])
                            ->setCustomer($customer)
                            ->getCustomerWithFinancialInfo();
                    });
                return $type;
            });
    }

    public function create($data)
    {
        return CustomerType::create([
            'name' => $data['type_name'] ?? null,
            'company_id' => Auth::user()->company->id
        ]);
    }

    public function update($id, $data)
    {
        $customer = CustomerType::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
        $customer->update([
            'name' => $data['type_name'] ?? null
        ]);
    }
}
