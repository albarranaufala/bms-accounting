<?php


namespace App\Repositories\Receivable\Customer;


interface CustomerTypeRepositoryInterface
{
    public function getAll();

    public function getAllByRange($data);

    public function create($data);

    public function update($id, $data);
}
