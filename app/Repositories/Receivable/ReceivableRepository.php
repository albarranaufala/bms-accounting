<?php


namespace App\Repositories\Receivable;


use App\Models\Receivable;
use App\Services\ReceivableService;
use Illuminate\Support\Facades\Auth;

class ReceivableRepository implements ReceivableRepositoryInterface
{

    public function getAll()
    {
        return Receivable::where('company_id', Auth::user()->company->id)->get()
            ->map(function ($receivable) {
                $receivable->is_complete = $receivable->debit_account_id && $receivable->credit_account_id ? 1 : 0;
                return $receivable;
            });
    }

    public function getAllByRange($data)
    {
        return (new ReceivableService())
            ->setRangeType($data['range_type'])
            ->setStartDate($data['start_date'])
            ->setEndDate($data['end_date'])
            ->setReceivables($this->getAll())
            ->getReceivablesByRange();
    }

    public function getAllCompletedByRange($data)
    {
        return $this->getAllByRange($data)
            ->whereNull('cash_id')
            ->whereNotNull('debit_account_id')
            ->whereNotNull('credit_account_id');
    }

    public function findById($id)
    {
        return Receivable::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
    }

    public function create($data)
    {
        return Receivable::create([
            'customer_id' => $data['customer'] ?? null,
            'date' => $data['date'] ?? null,
            'description' => $data['description'] ?? null,
            'invoice_num' => $data['invoice_num'] ?? null,
            'bill_value' => $data['bill_value'] ?? null,
            'payment_value' => $data['payment_value'] ?? null,
            'information' => $data['information'] ?? null,
            'debit_account_id' => (
                                    isset($data['debit_account']) ?
                                        $data['debit_account'] :
                                        (
                                            isset($data['debit_account_id']) ?
                                                $data['debit_account_id'] :
                                                null
                                        )
                                ),
            'credit_account_id' => (
                                    isset($data['credit_account']) ?
                                        $data['credit_account'] :
                                        (
                                            isset($data['credit_account_id']) ?
                                                $data['credit_account_id'] :
                                                null
                                        )
                                ),
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function update($id, $data)
    {
        return $this->findById($id)
            ->update([
                'customer_id' => $data['customer'] ?? null,
                'date' => $data['date'] ?? null,
                'description' => $data['description'] ?? null,
                'invoice_num' => $data['invoice_num'] ?? null,
                'bill_value' => $data['bill_value'] ?? null,
                'payment_value' => $data['payment_value'] ?? null,
                'information' => $data['information'] ?? null,
                'debit_account_id' => (
                                        isset($data['debit_account']) ?
                                            $data['debit_account'] :
                                            (
                                                isset($data['debit_account_id']) ?
                                                    $data['debit_account_id'] :
                                                    null
                                            )
                                    ),
                'credit_account_id' => (
                                            isset($data['credit_account']) ?
                                                $data['credit_account'] :
                                                (
                                                    isset($data['credit_account_id']) ?
                                                        $data['credit_account_id'] :
                                                        null
                                                )
                                        ),
                'company_id' => Auth::user()->company->id,
            ]);
    }

    public function delete($id)
    {
        return $this->findById($id)
            ->delete();
    }

    public function forceDelete($id)
    {
        return $this->findById($id)
            ->forceDelete();
    }
}
