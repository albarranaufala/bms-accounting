<?php

namespace App\Repositories\User;

interface UserRepositoryInterface
{
    public function getAll();

    public function findById($id);

    public function create($data);
    
    public function update($id,$data);

    public function updatePassword($user,$newPassword);

    public function resetPasswordRequest($user);

    public function resetPassword($user);

    public function rejectResetRequest($user);

    public function delete($id);

    public function forceDelete($id);

}