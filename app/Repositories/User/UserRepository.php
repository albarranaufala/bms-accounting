<?php

namespace App\Repositories\User;

use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;


class UserRepository implements UserRepositoryInterface
{
    public function getAll()
    {
    }

    public function findById($id)
    {
    }

    public function create($data)
    {
        $userRole = Role::find($data['role_id']);

        $user = new User;
        $user->assignRole($userRole->name);
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = Hash::make('SecurePassword2020');
        if(array_key_exists('company_id',$data)){
            $user->company_id = $data['company_id'];
        }
        $user->save();
        return true;
    }

    public function update($id,$data)
    {
        $currentRole = User::findOrFail($id)->roles[0];
        $newRole = Role::findOrFail($data['role_id']);
        $user = User::findOrFail($id);

        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        if(array_key_exists('company_id',$data)){
            $user->company_id = $data['company_id'];
        }
        $user->save();
        $user->removeRole($currentRole->name);
        $user->assignRole($newRole->name);
        return true;
    }

    public function updatePassword($user,$newPassword)
    {
        $target = User::find($user->id);
        return $target->update([
            'password' => Hash::make($newPassword)
        ]);
    }

    public function resetPasswordRequest($user)
    {
        $target = User::where('username',$user)->first();
        // $target = User::find($userId);
        return $target->update([
            'is_requesting_password_reset' => 1
        ]);
    }

    public function resetPassword($user)
    {
        $target = User::find($user);
        return $target->update([
            'is_requesting_password_reset' => 0,
            'password' => Hash::make('SecurePassword2020')
        ]);;
    }

    public function rejectResetRequest($user)
    {
        $target = User::find($user);
        return $target->update([
            'is_requesting_password_reset' => 0
        ]);
    }

    public function delete($id)
    {
        return User::findOrFail($id)->forceDelete();
    }

    public function forceDelete($id)
    {
    }
}