<?php


namespace App\Repositories\Debt\Client;


use App\Models\ClientType;
use App\Services\ClientService;
use Illuminate\Support\Facades\Auth;

class ClientTypeRepository implements ClientTypeRepositoryInterface
{

    public function getAll()
    {
        return ClientType::where('company_id', Auth::user()->company->id)->get();
    }

    public function getAllByRange($data)
    {
        return $this->getAll()
            ->map(function ($type) use ($data){
                $type->clients = $type->clients
                    ->map(function($client) use ($data){
                        return (new ClientService())
                            ->setRangeType($data['range_type'])
                            ->setStartDate($data['start_date'])
                            ->setEndDate($data['end_date'])
                            ->setClient($client)
                            ->getClientWithFinancialInfo();
                    });
                return $type;
            });
    }

    public function create($data)
    {
        return ClientType::create([
            'name' => $data['type_name'] ?? null,
            'company_id' => Auth::user()->company->id
        ]);
    }

    public function update($id, $data)
    {
        $client = ClientType::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
        $client->update([
            'name' => $data['type_name'] ?? null
        ]);
    }
}
