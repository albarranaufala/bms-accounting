<?php


namespace App\Repositories\Debt\Client;


interface ClientRepositoryInterface
{
    public function getAll();

    public function getAllByRange($data);

    public function findById($id);

    public function findByIdAndRange($id, $data);

    public function create($data);

    public function update($id, $data);

    public function delete($id);

    public function forceDelete($id);
}
