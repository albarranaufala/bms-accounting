<?php


namespace App\Repositories\Debt\Client;


interface ClientTypeRepositoryInterface
{
    public function getAll();

    public function getAllByRange($data);

    public function create($data);

    public function update($id, $data);
}
