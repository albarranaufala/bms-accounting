<?php


namespace App\Repositories\Debt\Client;


use App\Models\Client;
use App\Services\ClientService;
use Illuminate\Support\Facades\Auth;

class ClientRepository implements ClientRepositoryInterface
{
    public function getAll()
    {
        return Client::where('company_id', Auth::user()->company->id)->get();
    }

    public function getAllByRange($data)
    {
        return $this->getAll()
            ->map(function($client) use ($data){
                return (new ClientService())
                    ->setRangeType($data['range_type'])
                    ->setStartDate($data['start_date'])
                    ->setEndDate($data['end_date'])
                    ->setClient($client)
                    ->getClientWithFinancialInfo();
            });
    }

    public function findById($id)
    {
        return Client::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
    }

    public function findByIdAndRange($id, $data)
    {
        return (new ClientService())
            ->setClient($this->findById($id))
            ->setRangeType($data['range_type'])
            ->setStartDate($data['start_date'])
            ->setEndDate($data['end_date'])
            ->getClientWithFinancialInfo();
    }

    public function create($data)
    {
        return Client::create([
            'name' => $data['name'] ?? null,
            'code' => $data['code'] ?? null,
            'npwp' => $data['npwp'] ?? null,
            'address' => $data['address'] ?? null,
            'phone' => $data['phone'] ?? null,
            'email' => $data['email'] ?? null,
            'contact_person' => $data['contact_person'] ?? null,
            'balance' => $data['balance'] ?? null,
            'type_id' => $data['type'] ?? null,
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function update($id, $data)
    {
        return $this->findById($id)
            ->update([
                'name' => $data['name'] ?? null,
                'code' => $data['code'] ?? null,
                'npwp' => $data['npwp'] ?? null,
                'address' => $data['address'] ?? null,
                'phone' => $data['phone'] ?? null,
                'email' => $data['email'] ?? null,
                'contact_person' => $data['contact_person'] ?? null,
                'balance' => $data['balance'] ?? null,
                'type_id' => $data['type'] ?? null,
                'company_id' => Auth::user()->company->id,
            ]);
    }

    public function delete($id)
    {
        $client = $this->findById($id);
        $client->debts()->delete();

        return $client->delete();
    }

    public function forceDelete($id)
    {
        return $this->findById($id)
            ->forceDelete();
    }
}
