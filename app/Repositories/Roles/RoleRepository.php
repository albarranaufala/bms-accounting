<?php

namespace App\Repositories\Roles;

use Spatie\Permission\Models\Role;

class RoleRepository implements RoleRepositoryInterface
{
    public function getAll()
    {
        return Role::with('permissions')->get();
    }

    public function findById($id)
    {
        return Role::findOrFail($id);
    }

    public function create($data)
    {
        $role = Role::create([
            'name' => $data->name
        ]);
        $role->syncPermissions($data->permissions);

        return true;
    }

    public function update($id, $data)
    {
        $role = Role::findOrFail($id);
        $role->update([
            'name' => $data['name']
        ]);
        $role->syncpermissions($data['permissions']);
        return true;
        
    }

    public function delete($id){
        return Role::findOrFail($id)
            ->delete();
    }

    public function forceDelete($id){
        return Role::findOrFail($id)
            ->forceDelete();
    }
}