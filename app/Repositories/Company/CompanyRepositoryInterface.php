<?php

namespace App\Repositories\Company;
use App\Models\Company;

interface CompanyRepositoryInterface {
    public function getAll();

    public function findById($id);

    public function getCompanyusers($id);

    public function getCompanySupervisors($id);

    public function create($data);

    public function update($id,$data);

    public function delete($id);

    public function forceDelete($id);

    public function createAccounts(Company $company);
}