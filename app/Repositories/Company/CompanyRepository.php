<?php

namespace App\Repositories\Company;
use App\Models\Company;
use App\Models\User;
use App\Models\Account;
use Spatie\Permission\Models\Role;
use Database\Seeds\CompanyAccountSeeder;
use Illuminate\Support\Facades\DB;

class CompanyRepository implements CompanyRepositoryInterface
{
    public function getAll()
    {
        return Company::all();
    }

    public function findById($id)
    {
        return Company::findOrFail($id);
    }

    public function getCompanyUsers($id)
    {
        return User::where('company_id',$id)->get();
    }

    public function getCompanySupervisors($id)
    {
        return User::where('company_id',$id)
            ->whereHas('roles', function ($q) {
                $q->where('name','supervisor');
            })->get();
    }

    public function create($data)
    {
        $company = Company::create([
            'name' => $data['name'],
            'company_type' => $data['company_type']
        ]);
        return $this->createAccounts($company);
    }

    public function update($id,$data)
    {
        return Company::findOrFail($id)
            ->update([
                'name' => $data['name']
            ]);
    }

    public function delete($data)
    {
        return Company::findOrFail($data)
            ->delete();
    }

    public function forceDelete($data)
    {
        return Company::findOrFail($data)
            ->delete();
    }

    public function createAccounts(Company $company)
    {
        $accounts = [];
        $accounts['1-0000'] = Account::create([
            'account_num' => '1-0000',
            'name' => 'Aset',
            'account_type' => 'header',
            'company_id' => $company->id
        ]);
        $accounts['1-1000'] = Account::create([
            'account_num' => '1-1000',
            'name' => 'Aset Lancar',
            'account_type' => 'header',
            'header_id' => $accounts['1-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1100'] = Account::create([
            'account_num' => '1-1100',
            'name' => 'Kas dan Bank',
            'account_type' => 'header',
            'header_id' => $accounts['1-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1101'] = Account::create([
            'account_num' => '1-1101',
            'name' => 'Kas Banjarmasin',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1102'] = Account::create([
            'account_num' => '1-1102',
            'name' => 'Kas Kecil',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1106'] = Account::create([
            'account_num' => '1-1106',
            'name' => 'Bank Mayapada 0007',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1107'] = Account::create([
            'account_num' => '1-1107',
            'name' => 'Bank Mandiri 00310088855880',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1200'] = Account::create([
            'account_num' => '1-1200',
            'name' => 'Piutang Usaha',
            'account_type' => 'header',
            'header_id' => $accounts['1-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1201'] = Account::create([
            'account_num' => '1-1201',
            'name' => 'Piutang Pihak Ketiga',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1200']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1202'] = Account::create([
            'account_num' => '1-1202',
            'name' => 'Akun Penyisihan Piutang',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['1-1200']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1203'] = Account::create([
            'account_num' => '1-1203',
            'name' => 'Piutang Hub Istimewa',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1200']->id,
            'company_id' => $company->id
        ]);$accounts['1-1300'] = Account::create([
            'account_num' => '1-1300',
            'name' => 'Piutang Lain-lain',
            'account_type' => 'header',
            'header_id' => $accounts['1-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1301'] = Account::create([
            'account_num' => '1-1301',
            'name' => 'Piutang Karyawan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1302'] = Account::create([
            'account_num' => '1-1302',
            'name' => 'Piutang Lainnya',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1400'] = Account::create([
            'account_num' => '1-1400',
            'name' => 'Persediaan',
            'account_type' => 'header',
            'header_id' => $accounts['1-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1401'] = Account::create([
            'account_num' => '1-1401',
            'name' => 'Persediaan A',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1400']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1500'] = Account::create([
            'account_num' => '1-1500',
            'name' => 'Uang Muka Pajak',
            'account_type' => 'header',
            'header_id' => $accounts['1-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1501'] = Account::create([
            'account_num' => '1-1501',
            'name' => 'UM PPh Pasal 21',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1500']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1502'] = Account::create([
            'account_num' => '1-1502',
            'name' => 'UM PPh Pasal 4 (2)',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-1500']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1503'] = Account::create([
            'account_num' => '1-1503',
            'name' => 'UM PPh Pasal 25',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1500']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1504'] = Account::create([
            'account_num' => '1-1504',
            'name' => 'UM PPh Pasal 22/23',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1500']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1599'] = Account::create([
            'account_num' => '1-1599',
            'name' => 'PPN Masukan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1500']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1600'] = Account::create([
            'account_num' => '1-1600',
            'name' => 'Biaya Dibayar Dimuka',
            'account_type' => 'Header',
            'header_id' => $accounts['1-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1601'] = Account::create([
            'account_num' => '1-1601',
            'name' => 'Sewa Bayar Dimuka',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1600']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-1604'] = Account::create([
            'account_num' => '1-1604',
            'name' => 'Biaya Dibayar Dimuka Lainnya',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-1600']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2000'] = Account::create([
            'account_num' => '1-2000',
            'name' => 'ASET TETAP',
            'account_type' => 'header',
            'header_id' => $accounts['1-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2100'] = Account::create([
            'account_num' => '1-2100',
            'name' => 'harga Perolehan',
            'account_type' => 'header',
            'header_id' => $accounts['1-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2101'] = Account::create([
            'account_num' => '1-2101',
            'name' => 'Tanah',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2102'] = Account::create([
            'account_num' => '1-2102',
            'name' => 'Bangunan dan Prasarana',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2103'] = Account::create([
            'account_num' => '1-2103',
            'name' => 'Mesin dan Peralatan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2104'] = Account::create([
            'account_num' => '1-2104',
            'name' => 'Kendaraan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2105'] = Account::create([
            'account_num' => '1-2105',
            'name' => 'Peralatan Kantor',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);$accounts['1-2106'] = Account::create([
            'account_num' => '1-2106',
            'name' => 'Aset Leasing',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2107'] = Account::create([
            'account_num' => '1-2107',
            'name' => 'Aset Dalam Penyelesaian',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2200'] = Account::create([
            'account_num' => '1-2200',
            'name' => 'Akumulasi Penyusutan',
            'account_type' => 'header',
            'header_id' => $accounts['1-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2201'] = Account::create([
            'account_num' => '1-2201',
            'name' => 'Ak Peny - Bangunan dan Prasarana',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['1-2200']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2202'] = Account::create([
            'account_num' => '1-2202',
            'name' => 'Ak Peny - Mesin dan Peralatan',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['1-2200']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2203'] = Account::create([
            'account_num' => '1-2203',
            'name' => 'Ak Peny - Kendaraan',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['1-2200']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-2204'] = Account::create([
            'account_num' => '1-2204',
            'name' => 'Ak Peny - Peralatan Kantor',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['1-2200']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-3000'] = Account::create([
            'account_num' => '1-3000',
            'name' => 'ASET LAIN-LAIN',
            'account_type' => 'header',
            'header_id' => $accounts['1-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-3001'] = Account::create([
            'account_num' => '1-3001',
            'name' => 'Goodwill/Aset Tak Berwujud',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-3002'] = Account::create([
            'account_num' => '1-3002',
            'name' => 'Beban Ditangguhkan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'balance' => '0',
            'header_id' => $accounts['1-3000']->id,
            'company_id' => $company->id
        ]);$accounts['1-3003'] = Account::create([
            'account_num' => '1-3003',
            'name' => 'Amortisasi Beban Tangguhan',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['1-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['1-3004'] = Account::create([
            'account_num' => '1-3004',
            'name' => 'Aset Lainnya',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['1-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-0000'] = Account::create([
            'account_num' => '2-0000',
            'name' => 'KEWAJIBAN',
            'account_type' => 'header',
            'company_id' => $company->id
        ]);
        $accounts['2-1000'] = Account::create([
            'account_num' => '2-1000',
            'name' => 'KEWAJIBAN LANCAR',
            'account_type' => 'header',
            'header_id' => $accounts['2-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1100'] = Account::create([
            'account_num' => '2-1100',
            'name' => 'Utang Usaha',
            'account_type' => 'header',
            'header_id' => $accounts['2-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1101'] = Account::create([
            'account_num' => '2-1101',
            'name' => 'Utang Pihak Ketiga',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-1100']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1102'] = Account::create([
            'account_num' => '2-1102',
            'name' => 'Utang Pihak Hub Istimewa',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1100']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1200'] = Account::create([
            'account_num' => '2-1200',
            'name' => 'Biaya YMH Dibayar',
            'account_type' => 'header',
            'header_id' => $accounts['2-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1201'] = Account::create([
            'account_num' => '2-1201',
            'name' => 'By YMH Gaji',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-1200']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1202'] = Account::create([
            'account_num' => '2-1202',
            'name' => 'By YMH Langganan & Jasa',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1200']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1205'] = Account::create([
            'account_num' => '2-1205',
            'name' => 'By YMH Dibayar Lainnya',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-1200']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1300'] = Account::create([
            'account_num' => '2-1300',
            'name' => 'Utang Pajak',
            'account_type' => 'header',
            'header_id' => $accounts['2-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1301'] = Account::create([
            'account_num' => '2-1301',
            'name' => 'Utang PPh 4 (2)',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1302'] = Account::create([
            'account_num' => '2-1302',
            'name' => 'Utang PPh 21',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1303'] = Account::create([
            'account_num' => '2-1303',
            'name' => 'Utang PPh 29',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1309'] = Account::create([
            'account_num' => '2-1309',
            'name' => 'Utang SKPKB/STP',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1399'] = Account::create([
            'account_num' => '2-1399',
            'name' => 'PPN Keluaran',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1300']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1400'] = Account::create([
            'account_num' => '2-1400',
            'name' => 'Utang Lancar Lainnya',
            'account_type' => 'header',
            'header_id' => $accounts['2-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1401'] = Account::create([
            'account_num' => '2-1401',
            'name' => 'Pendapatan Diterima Dimuka',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1400']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1402'] = Account::create([
            'account_num' => '2-1402',
            'name' => 'Titipan Pihak Ketiga',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-1400']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-1409'] = Account::create([
            'account_num' => '2-1409',
            'name' => 'Utang Lainnya',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-1400']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-2000'] = Account::create([
            'account_num' => '2-2000',
            'name' => 'KEWAJIBAN JANGKA PANJANG',
            'account_type' => 'header',
            'header_id' => $accounts['2-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-2100'] = Account::create([
            'account_num' => '2-2100',
            'name' => 'Utang Bank',
            'account_type' => 'header',
            'header_id' => $accounts['2-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-2101'] = Account::create([
            'account_num' => '2-2101',
            'name' => 'Utang Bank Mayapada',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-2100']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-2200'] = Account::create([
            'account_num' => '2-2200',
            'name' => 'Utang Leasing',
            'account_type' => 'header',
            'header_id' => $accounts['2-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-2201'] = Account::create([
            'account_num' => '2-2201',
            'name' => 'Utang Leasing BFI',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['2-2200']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-3000'] = Account::create([
            'account_num' => '2-3000',
            'name' => 'KEWAJIBAN LAIN-LAIN',
            'account_type' => 'header',
            'header_id' => $accounts['2-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-3001'] = Account::create([
            'account_num' => '2-3001',
            'name' => 'Utang Jaminan Pelanggan',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-3002'] = Account::create([
            'account_num' => '2-3002',
            'name' => 'Penyisihan Copr Social Resp',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['2-3003'] = Account::create([
            'account_num' => '2-3003',
            'name' => 'Hutang Subordinasi - Pemilik',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['2-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['3-0000'] = Account::create([
            'account_num' => '3-0000',
            'name' => 'EKUITAS',
            'account_type' => 'header',
            'company_id' => $company->id
        ]);
        $accounts['3-0001'] = Account::create([
            'account_num' => '3-0001',
            'name' => 'Modal Saham',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['3-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['3-0002'] = Account::create([
            'account_num' => '3-0002',
            'name' => 'Laba Ditahan',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'balance' => '0',
            'header_id' => $accounts['3-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['3-0003'] = Account::create([
            'account_num' => '3-0003',
            'name' => 'Laba Periode Berjalan',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['3-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-0000'] = Account::create([
            'account_num' => '4-0000',
            'name' => 'PENDAPATAN',
            'account_type' => 'header',
            'company_id' => $company->id
        ]);
        $accounts['4-1000'] = Account::create([
            'account_num' => '4-1000',
            'name' => 'PENDAPATAN USAHA',
            'account_type' => 'header',
            'header_id' => $accounts['4-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-1001'] = Account::create([
            'account_num' => '4-1001',
            'name' => 'Pendapatan Jasa Manajemen',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['4-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-1002'] = Account::create([
            'account_num' => '4-1002',
            'name' => 'Pendapatan Jasa Konsultasi',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['4-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-1009'] = Account::create([
            'account_num' => '4-1009',
            'name' => 'Pendapatan Usaha Lainnya',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['4-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-0000'] = Account::create([
            'account_num' => '5-0000',
            'name' => 'BIAYA',
            'account_type' => 'header',
            'company_id' => $company->id
        ]);
        $accounts['5-1000'] = Account::create([
            'account_num' => '5-1000',
            'name' => 'BIAYA LANGSUNG',
            'account_type' => 'header',
            'header_id' => $accounts['5-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1001'] = Account::create([
            'account_num' => '5-1001',
            'name' => 'By Gaji dan Upah - Ops',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1002'] = Account::create([
            'account_num' => '5-1002',
            'name' => 'By BBM/Pelumas - Ops',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1003'] = Account::create([
            'account_num' => '5-1003',
            'name' => 'By Transport Dlm/Luar Kota',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1004'] = Account::create([
            'account_num' => '5-1004',
            'name' => 'By ATK/Foto Copy/Jilid',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1005'] = Account::create([
            'account_num' => '5-1005',
            'name' => 'By Konsultan Tenaga Ahli',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1006'] = Account::create([
            'account_num' => '5-1006',
            'name' => 'By Kesejahteraan Karyawan - Ops',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1007'] = Account::create([
            'account_num' => '5-1007',
            'name' => 'By Perjalanan Dinas - Osp',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1008'] = Account::create([
            'account_num' => '5-1008',
            'name' => 'By Pemel Kendaraan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1009'] = Account::create([
            'account_num' => '5-1009',
            'name' => 'By Bagi Hasil',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1010'] = Account::create([
            'account_num' => '5-1990',
            'name' => 'By Promosi',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1980'] = Account::create([
            'account_num' => '5-1980',
            'name' => 'By Penyusutan-Operasional',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1990'] = Account::create([
            'account_num' => '5-1990',
            'name' => 'By Langsung Lainnya',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1998'] = Account::create([
            'account_num' => '5-1998',
            'name' => 'By Pajak & Retribusi',
            'account_type' => 'total',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-1999'] = Account::create([
            'account_num' => '5-1999',
            'name' => 'LABA (RUGI) KOTOR',
            'account_type' => 'grand_total',
            'header_id' => $accounts['5-1000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2000'] = Account::create([
            'account_num' => '5-2000',
            'name' => 'BIAYA UMUM & ADMINISTRASI',
            'account_type' => 'header',
            'header_id' => $accounts['5-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2001'] = Account::create([
            'account_num' => '5-2001',
            'name' => 'By Gaji dan Upah - Administrasi',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2002'] = Account::create([
            'account_num' => '5-2002',
            'name' => 'By Kantor',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2003'] = Account::create([
            'account_num' => '5-2003',
            'name' => 'By Sewa Kantor & Peralatan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2004'] = Account::create([
            'account_num' => '5-2004',
            'name' => 'By Langganan & Jasa',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2005'] = Account::create([
            'account_num' => '5-2005',
            'name' => 'By Perjalanan Dinas - Adm',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2006'] = Account::create([
            'account_num' => '5-2006',
            'name' => 'By BPJS',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2007'] = Account::create([
            'account_num' => '5-2007',
            'name' => 'By Pengobatan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2008'] = Account::create([
            'account_num' => '5-2008',
            'name' => 'By Pajak & Retribusi',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2009'] = Account::create([
            'account_num' => '5-2009',
            'name' => 'By Bahan Cetakan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2010'] = Account::create([
            'account_num' => '5-2010',
            'name' => 'By Representasi Manajemen',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2011'] = Account::create([
            'account_num' => '5-2011',
            'name' => 'By Sumbangan dan Biaya Tamu',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2012'] = Account::create([
            'account_num' => '5-2012',
            'name' => 'By Bunga Pinjaman',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2013'] = Account::create([
            'account_num' => '5-2013',
            'name' => 'By Penyusutan & Amort - Admin',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2014'] = Account::create([
            'account_num' => '5-2014',
            'name' => 'By Adm & Umum Lainnya',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-2999'] = Account::create([
            'account_num' => '5-2999',
            'name' => 'LABA (RUGI) USAHA',
            'account_type' => 'grand_total',
            'header_id' => $accounts['5-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-2000'] = Account::create([
            'account_num' => '4-2000',
            'name' => 'PENDAPATAN LAIN-LAIN',
            'account_type' => 'header',
            'header_id' => $accounts['4-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-2001'] = Account::create([
            'account_num' => '4-2001',
            'name' => 'Penjualan Aset',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['4-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-2002'] = Account::create([
            'account_num' => '4-2002',
            'name' => 'Jasa Giro',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['4-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['4-2003'] = Account::create([
            'account_num' => '4-2003',
            'name' => 'Pendapatan Lainnya',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['4-2000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-3000'] = Account::create([
            'account_num' => '5-3000',
            'name' => 'BIAYA LAIN-LAIN',
            'account_type' => 'header',
            'header_id' => $accounts['5-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-3001'] = Account::create([
            'account_num' => '5-3001',
            'name' => 'Biaya Admin Bank',
            'account_type' => 'account',
            'balance_type' => 'credit',
            'header_id' => $accounts['5-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-3002'] = Account::create([
            'account_num' => '5-3002',
            'name' => 'Kerugian Selisih Kurs',
            'account_type' => 'account',
            'header_id' => $accounts['5-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-8000'] = Account::create([
            'account_num' => '5-8000',
            'name' => 'LABA (RUGI) SEBELUM PAJAK',
            'account_type' => 'grand_total',
            'header_id' => $accounts['5-0000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-3999'] = Account::create([
            'account_num' => '5-3999',
            'name' => 'PPh Badan',
            'account_type' => 'account',
            'balance_type' => 'debit',
            'header_id' => $accounts['5-3000']->id,
            'company_id' => $company->id
        ]);
        $accounts['5-9999'] = Account::create([
            'account_num' => '5-9999',
            'name' => 'LABA (RUGI) SETELAH PAJAK',
            'account_type' => 'grand_total',
            'header_id' => $accounts['5-0000']->id,
            'company_id' => $company->id
        ]);
        return true;
    }
}