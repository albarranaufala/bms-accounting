<?php


namespace App\Repositories\Cash;


use App\Models\Cash;
use App\Services\CashService;
use Illuminate\Support\Facades\Auth;

class CashRepository implements CashRepositoryInterface
{

    public function getAll()
    {
        return Cash::where('company_id', Auth::user()->company->id)->get()
            ->map(function ($cash) {
                $cash->is_complete = $cash->debit_account_id && $cash->credit_account_id ? 1 : 0;
                return $cash;
            });
    }

    public function getAllByRange($data)
    {
        return (new CashService())
            ->setRangeType($data['range_type'])
            ->setStartDate($data['start_date'])
            ->setEndDate($data['end_date'])
            ->setCashes($this->getAll())
            ->getCashesByRange();
    }

    public function getAllCompletedByRange($data)
    {
        return $this->getAllByRange($data)
            ->whereNotNull('debit_account_id')
            ->whereNotNull('credit_account_id');
    }

    public function findById($id)
    {
        return Cash::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
    }

    public function create($data)
    {
        return Cash::create([
            'bank_id' => $data['bank'] ?? null,
            'date' => $data['date'] ?? null,
            'description' => $data['description'] ?? null,
            'invoice_num' => $data['invoice_num'] ?? null,
            'bill_value' => $data['bill_value'] ?? null,
            'payment_value' => $data['payment_value'] ?? null,
            'information' => $data['information'] ?? null,
            'debit_account_id' => (
                                    isset($data['debit_account']) ?
                                        $data['debit_account'] :
                                        (
                                            isset($data['debit_account_id']) ?
                                                $data['debit_account_id'] :
                                                null
                                        )
                                ),
            'credit_account_id' => (
                                    isset($data['credit_account']) ?
                                        $data['credit_account'] :
                                        (
                                            isset($data['credit_account_id']) ?
                                                $data['credit_account_id'] :
                                                null
                                        )
                                ),
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function update($id, $data)
    {
        return $this->findById($id)
            ->update([
                'bank_id' => $data['bank'] ?? null,
                'date' => $data['date'] ?? null,
                'description' => $data['description'] ?? null,
                'invoice_num' => $data['invoice_num'] ?? null,
                'bill_value' => $data['bill_value'] ?? null,
                'payment_value' => $data['payment_value'] ?? null,
                'information' => $data['information'] ?? null,
                'debit_account_id' => (
                                        isset($data['debit_account']) ?
                                            $data['debit_account'] :
                                            (
                                                isset($data['debit_account_id']) ?
                                                    $data['debit_account_id'] :
                                                    null
                                            )
                                    ),
                'credit_account_id' => (
                                            isset($data['credit_account']) ?
                                                $data['credit_account'] :
                                                (
                                                    isset($data['credit_account_id']) ?
                                                        $data['credit_account_id'] :
                                                        null
                                                )
                                        ),
                'company_id' => Auth::user()->company->id,
            ]);
    }

    public function delete($id)
    {
        return $this->findById($id)
            ->delete();
    }

    public function forceDelete($id)
    {
        return $this->findById($id)
            ->forceDelete();
    }
}
