<?php


namespace App\Repositories\Cash;


interface CashRepositoryInterface
{
    public function getAll();

    public function getAllByRange($data);

    public function getAllCompletedByRange($data);

    public function findById($id);

    public function create($data);

    public function update($id, $data);

    public function delete($id);

    public function forceDelete($id);
}
