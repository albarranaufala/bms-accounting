<?php


namespace App\Repositories\Cash\Bank;

use App\Models\BankType;
use App\Services\BankService;
use Illuminate\Support\Facades\Auth;

class BankTypeRepository implements BankTypeRepositoryInterface
{
    public function getAll()
    {
        return BankType::where('company_id', Auth::user()->company->id)->get();
    }

    public function getAllByRange($data)
    {
        return $this->getAll()
            ->map(function ($type) use ($data){
                $type->banks = $type->banks
                    ->map(function($bank) use ($data){
                        return (new BankService())
                            ->setRangeType($data['range_type'])
                            ->setStartDate($data['start_date'])
                            ->setEndDate($data['end_date'])
                            ->setBank($bank)
                            ->getBankWithFinancialInfo();
                    });
                return $type;
            });
    }

    public function create($data)
    {
        return BankType::create([
            'name' => $data['type_name'] ?? null,
            'company_id' => Auth::user()->company->id
        ]);
    }

    public function update($id, $data)
    {
        $bank = BankType::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
        $bank->update([
            'name' => $data['type_name'] ?? null
        ]);
    }
}
