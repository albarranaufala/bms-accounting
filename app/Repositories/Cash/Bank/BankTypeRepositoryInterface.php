<?php


namespace App\Repositories\Cash\Bank;


interface BankTypeRepositoryInterface
{
    public function getAll();

    public function getAllByRange($data);

    public function create($data);

    public function update($id, $data);
}
