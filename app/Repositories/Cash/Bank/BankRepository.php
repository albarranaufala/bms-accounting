<?php


namespace App\Repositories\Cash\Bank;


use App\Models\Bank;
use App\Services\BankService;
use Illuminate\Support\Facades\Auth;

class BankRepository implements BankRepositoryInterface
{
    public function getAll()
    {
        return Bank::where('company_id', Auth::user()->company->id)->get();
    }

    public function getAllByRange($data)
    {
        return $this->getAll()
            ->map(function($bank) use ($data){
                return (new BankService())
                    ->setRangeType($data['range_type'])
                    ->setStartDate($data['start_date'])
                    ->setEndDate($data['end_date'])
                    ->setBank($bank)
                    ->getBankWithFinancialInfo();
            });
    }

    public function findById($id)
    {
        return Bank::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
    }

    public function findByIdAndRange($id, $data)
    {
        return (new BankService())
            ->setBank($this->findById($id))
            ->setRangeType($data['range_type'])
            ->setStartDate($data['start_date'])
            ->setEndDate($data['end_date'])
            ->getBankWithFinancialInfo();
    }

    public function create($data)
    {
        return Bank::create([
            'name' => $data['name'] ?? null,
            'code' => $data['code'] ?? null,
            'bank_account' => $data['bank_account'] ?? null,
            'owner_name' => $data['owner_name'] ?? null,
            'class' => $data['class'] ?? null,
            'balance' => $data['balance'] ?? null,
            'type_id' => $data['type'] ?? null,
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function update($id, $data)
    {
        return $this->findById($id)
            ->update([
                'name' => $data['name'] ?? null,
                'code' => $data['code'] ?? null,
                'bank_account' => $data['bank_account'] ?? null,
                'owner_name' => $data['owner_name'] ?? null,
                'class' => $data['class'] ?? null,
                'balance' => $data['balance'] ?? null,
                'type_id' => $data['type'] ?? null,
                'company_id' => Auth::user()->company->id,
            ]);
    }

    public function delete($id)
    {
        $bank = $this->findById($id);
        $bank->cashes()->delete();

        return $bank->delete();
    }

    public function forceDelete($id)
    {
        return $this->findById($id)
            ->forceDelete();
    }
}
