<?php


namespace App\Repositories\Account;


interface AccountRepositoryInterface
{
    public function getAll();

    
    public function findById($id);
    
    public function findByAccountNum($account_num);

    public function create($data);

    public function update($id, $data);

    public function delete($id);

    public function forceDelete($id);
}
