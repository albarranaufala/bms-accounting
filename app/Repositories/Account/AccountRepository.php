<?php


namespace App\Repositories\Account;


use App\Models\Account;
use Illuminate\Support\Facades\Auth;

class AccountRepository implements AccountRepositoryInterface
{

    public function getAll()
    {
        return Account::where('company_id', Auth::user()->company->id)->get();
    }

    public function findById($id)
    {
        return Account::where('id', $id)->where('company_id', Auth::user()->company->id)->firstOrFail();
    }

    public function findByAccountNum($account_num)
    {
        return Account::where('company_id', Auth::user()->company->id)
            ->where('account_num', $account_num)
            ->firstOrFail();
    }

    public function create($data)
    {
        return Account::create([
            'account_num' => $data['account_num'],
            'name' => $data['name'],
            'balance' => $data['balance'],
            'account_type' => $data['account_type'],
            'balance_type' => $data['balance_type'],
            'header_id' => $data['header'] ?? null,
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function update($id, $data)
    {
        return $this->findById($id)->update([
            'account_num' => $data['account_num'],
            'name' => $data['name'],
            'balance' => $data['balance'],
            'account_type' => $data['account_type'],
            'balance_type' => $data['balance_type'],
            'header_id' => $data['header'] ?? null,
            'company_id' => Auth::user()->company->id,
        ]);
    }

    public function delete($id)
    {
        return $this->findById($id)
            ->delete();
    }

    public function forceDelete($id)
    {
        return $this->findById($id)
            ->forceDelete();
    }
}
