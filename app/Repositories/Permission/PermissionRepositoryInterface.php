<?php

namespace App\Repositories\Permission;

interface PermissionRepositoryInterface
{
    public function getAll();

    public function findById($id);

    public function create($data);

    public function delete($id);

    public function forceDelete($id);
}