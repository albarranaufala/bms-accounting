<?php

namespace App\Repositories\Permission;

use Spatie\Permission\Models\Permission;

class PermissionRepository implements PermissionRepositoryInterface
{
    public function getAll()
    {
        return Permission::all();
    }

    public function findById($id)
    {
        return Permission::findOrFail($id);
    }

    public function create($data)
    {
        return Permission::create([
            'name' => $data->name
        ]);
    }

    public function delete($id)
    {
        return Permission::findOrFail($id)
            ->delete();
    }

    public function forceDelete($id)
    {
        return Permission::findOrFail($id)
            ->forceDelete();
    }
}