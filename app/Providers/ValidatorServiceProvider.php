<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('unique_by_company', function($attribute, $value, $parameters, $validator){
            $data = DB::table($parameters[0])->where('company_id', Auth::user()->company->id)->whereNull('deleted_at')->get();
            $column = $parameters[1] ?? null;
            foreach ($data as $item){
                if(($item->$column ?? $item->$attribute) == $value){
                    if(($parameters[2] ?? null) == $item->id){
                        continue;
                    }
                    return false;
                }
            }
            return true;
        });
    }
}
