<?php

namespace App\Providers;

use App\Models\Cash;
use App\Models\Debt;
use App\Models\Receivable;
use App\Observers\CashObserver;
use App\Observers\DebtObserver;
use App\Observers\ReceivableObserver;
use App\Repositories\Cash\Bank\BankRepository;
use App\Repositories\Cash\Bank\BankRepositoryInterface;
use App\Repositories\Cash\Bank\BankTypeRepository;
use App\Repositories\Cash\Bank\BankTypeRepositoryInterface;
use App\Repositories\Cash\CashRepository;
use App\Repositories\Cash\CashRepositoryInterface;
use App\Repositories\Debt\DebtRepository;
use App\Repositories\Debt\DebtRepositoryInterface;
use Carbon\Carbon;
use App\Repositories\Account\AccountRepository;
use App\Repositories\Account\AccountRepositoryInterface;
use App\Repositories\Receivable\Customer\CustomerRepository;
use App\Repositories\Receivable\Customer\CustomerRepositoryInterface;
use App\Repositories\Receivable\Customer\CustomerTypeRepository;
use App\Repositories\Receivable\Customer\CustomerTypeRepositoryInterface;
use App\Repositories\Receivable\ReceivableRepository;
use App\Repositories\Receivable\ReceivableRepositoryInterface;
use App\Repositories\Roles\RoleRepository;
use App\Repositories\Roles\RoleRepositoryInterface;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Company\CompanyRepository;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\Debt\Client\ClientRepository;
use App\Repositories\Debt\Client\ClientRepositoryInterface;
use App\Repositories\Debt\Client\ClientTypeRepository;
use App\Repositories\Debt\Client\ClientTypeRepositoryInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AccountRepositoryInterface::class, AccountRepository::class);
        $this->app->bind(ReceivableRepositoryInterface::class, ReceivableRepository::class);
        $this->app->bind(CustomerRepositoryInterface::class, CustomerRepository::class);
        $this->app->bind(CustomerTypeRepositoryInterface::class, CustomerTypeRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(PermissionRepositoryInterface::class,PermissionRepository::class);
        $this->app->bind(UserRepositoryInterface::class,UserRepository::class);
        $this->app->bind(CompanyRepositoryInterface::class,CompanyRepository::class);
        $this->app->bind(DebtRepositoryInterface::class,DebtRepository::class);
        $this->app->bind(ClientRepositoryInterface::class,ClientRepository::class);
        $this->app->bind(ClientTypeRepositoryInterface::class,ClientTypeRepository::class);
        $this->app->bind(CashRepositoryInterface::class,CashRepository::class);
        $this->app->bind(BankRepositoryInterface::class,BankRepository::class);
        $this->app->bind(BankTypeRepositoryInterface::class,BankTypeRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Receivable::observe(ReceivableObserver::class);
        Debt::observe(DebtObserver::class);
        Cash::observe(CashObserver::class);

        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');

        Validator::extend('current_password',function ($attribute,$value,$parameters,$validator) {
            $user = User::find($parameters[0]);
            return $user && Hash::check($value, $user->password);
        }, 'Password Salah!');
    }
}
