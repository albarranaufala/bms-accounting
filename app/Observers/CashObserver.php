<?php

namespace App\Observers;

use App\Models\Cash;
use App\Repositories\Account\AccountRepository;
use App\Repositories\Debt\DebtRepository;
use App\Repositories\Receivable\ReceivableRepository;

class CashObserver
{
    /**
     * Handle the cash "created" event.
     *
     * @param Cash $cash
     * @return void
     */
    public function created(Cash $cash)
    {
        $mainReceivableAccounts = (new AccountRepository())->findByAccountNum('1-1200')->accounts;
        $otherReceivableAccounts = (new AccountRepository())->findByAccountNum('1-1300')->accounts;
        $receivableAccounts = $mainReceivableAccounts->merge($otherReceivableAccounts);

        $accountsPayable = (new AccountRepository())->findByAccountNum('2-1100')->accounts;
        $taxDebts = (new AccountRepository())->findByAccountNum('2-1300')->accounts;
        $otherDebts = (new AccountRepository())->findByAccountNum('2-1400')->accounts;
        $debtAccounts = $accountsPayable->merge($taxDebts)->merge($otherDebts);

        if($receivableAccounts->where('id', $cash->debit_account_id)->count() || $receivableAccounts->where('id', $cash->credit_account_id)->count()){
            $receivable = (new ReceivableRepository())->create($cash->toArray());
            $cash->receivable()->save($receivable);
        } 
        if($debtAccounts->where('id', $cash->credit_account_id)->count() || $debtAccounts->where('id', $cash->debit_account_id)->count()){
            $debt = (new DebtRepository())->create($cash->toArray());
            $cash->debt()->save($debt);
        }
    }

    /**
     * Handle the cash "updated" event.
     *
     * @param Cash $cash
     * @return void
     */
    public function updated(Cash $cash)
    {
        if($cash->receivable){
            $cash->receivable()->delete();
        }
        if($cash->debt){
            $cash->debt()->delete();
        }

        $mainReceivableAccounts = (new AccountRepository())->findByAccountNum('1-1200')->accounts;
        $otherReceivableAccounts = (new AccountRepository())->findByAccountNum('1-1300')->accounts;
        $receivableAccounts = $mainReceivableAccounts->merge($otherReceivableAccounts);

        $accountsPayable = (new AccountRepository())->findByAccountNum('2-1100')->accounts;
        $taxDebts = (new AccountRepository())->findByAccountNum('2-1300')->accounts;
        $otherDebts = (new AccountRepository())->findByAccountNum('2-1400')->accounts;
        $debtAccounts = $accountsPayable->merge($taxDebts)->merge($otherDebts);

        if($receivableAccounts->where('id', $cash->debit_account_id)->count() || $receivableAccounts->where('id', $cash->credit_account_id)->count()){
            $receivable = (new ReceivableRepository())->create($cash->toArray());
            $cash->receivable()->save($receivable);
        } 
        if($debtAccounts->where('id', $cash->credit_account_id)->count() || $debtAccounts->where('id', $cash->debit_account_id)->count()){
            $debt = (new DebtRepository())->create($cash->toArray());
            $cash->debt()->save($debt);
        }
    }

    /**
     * Handle the cash "deleting" event.
     *
     * @param Cash $cash
     * @return void
     */
    public function deleting(Cash $cash)
    {
        if($cash->receivable){
            $cash->receivable()->delete();
        }
        if($cash->debt){
            $cash->debt()->delete();
        }
    }

    /**
     * Handle the cash "restored" event.
     *
     * @param Cash $cash
     * @return void
     */
    public function restored(Cash $cash)
    {
        //
    }

    /**
     * Handle the cash "force deleted" event.
     *
     * @param Cash $cash
     * @return void
     */
    public function forceDeleted(Cash $cash)
    {
        //
    }
}
