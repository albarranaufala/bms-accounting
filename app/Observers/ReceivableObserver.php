<?php

namespace App\Observers;

use App\Models\Receivable;
use App\Repositories\Account\AccountRepository;
use App\Repositories\Cash\CashRepository;

class ReceivableObserver
{
    /**
     * Handle the receivable "created" event.
     *
     * @param Receivable $receivable
     * @return void
     */
    public function created(Receivable $receivable)
    {
        // $cashAccounts = (new AccountRepository())->findByAccountNum('1-1100')->accounts;
        // if($cashAccounts->where('id', $receivable->debit_account_id)->count()){
        //     $cash = (new CashRepository())->create($receivable->toArray());
        //     $receivable->cash()->associate($cash);
        //     $receivable->save();
        // }
    }

    /**
     * Handle the receivable "updated" event.
     *
     * @param Receivable $receivable
     * @return void
     */
    public function updated(Receivable $receivable)
    {
        // $cashAccounts = (new AccountRepository())->findByAccountNum('1-1100')->accounts;
        // if($receivable->cash){
        //     if($cashAccounts->where('id', $receivable->debit_account_id)->count()){
        //         (new CashRepository())->update($receivable->cash->id, $receivable->toArray());
        //     } else {
        //         $receivable->cash()->delete();
        //         $receivable->cash()->dissociate();
        //         $receivable->save();
        //     }
        // } else {
        //     if($cashAccounts->where('id', $receivable->debit_account_id)->count()){
        //         $cash = (new CashRepository())->create($receivable->toArray());
        //         $receivable->cash()->associate($cash);
        //         $receivable->save();
        //     }
        // }
    }

    /**
     * Handle the receivable "deleting" event.
     *
     * @param Receivable $receivable
     * @return void
     */
    public function deleting(Receivable $receivable)
    {
        // if($receivable->cash){
        //     $receivable->cash()->delete();
        // }
    }

    /**
     * Handle the receivable "restored" event.
     *
     * @param Receivable $receivable
     * @return void
     */
    public function restored(Receivable $receivable)
    {
        //
    }

    /**
     * Handle the receivable "force deleted" event.
     *
     * @param Receivable $receivable
     * @return void
     */
    public function forceDeleted(Receivable $receivable)
    {
        //
    }
}
