<?php

namespace App\Observers;

use App\Models\Debt;
use App\Repositories\Account\AccountRepository;
use App\Repositories\Cash\CashRepository;

class DebtObserver
{
    /**
     * Handle the debt "created" event.
     *
     * @param Debt $debt
     * @return void
     */
    public function created(Debt $debt)
    {
        // $cashAccounts = (new AccountRepository())->findByAccountNum('1-1100')->accounts;
        // if($cashAccounts->where('id', $debt->credit_account_id)->count()){
        //     $cash = (new CashRepository())->create($debt->toArray());
        //     $debt->cash()->associate($cash);
        //     $debt->save();
        // }
    }

    /**
     * Handle the debt "updated" event.
     *
     * @param Debt $debt
     * @return void
     */
    public function updated(Debt $debt)
    {
        // $cashAccounts = (new AccountRepository())->findByAccountNum('1-1100')->accounts;
        // if($debt->cash){
        //     if($cashAccounts->where('id', $debt->credit_account_id)->count()){
        //         (new CashRepository())->update($debt->cash->id, $debt->toArray());
        //     } else {
        //         // $debt->cash()->delete();
        //         // $debt->cash()->dissociate();
        //         // $debt->save();
        //     }
        // } else {
        //     if($cashAccounts->where('id', $debt->credit_account_id)->count()){
        //         $cash = (new CashRepository())->create($debt->toArray());
        //         $debt->cash()->associate($cash);
        //         $debt->save();
        //     }
        // }
    }

    /**
     * Handle the debt "deleting" event.
     *
     * @param Debt $debt
     * @return void
     */
    public function deleting(Debt $debt)
    {
        // if($debt->cash){
        //     $debt->cash()->delete();
        // }
    }

    /**
     * Handle the debt "restored" event.
     *
     * @param Debt $debt
     * @return void
     */
    public function restored(Debt $debt)
    {
        //
    }

    /**
     * Handle the debt "force deleted" event.
     *
     * @param Debt $debt
     * @return void
     */
    public function forceDeleted(Debt $debt)
    {
        //
    }
}
