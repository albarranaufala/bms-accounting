<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function debts(){
        return $this->hasMany(Debt::class, 'client_id');
    }

    public function type(){
        return $this->belongsTo(ClientType::class, 'type_id');
    }
}
