<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function receivables(){
        return $this->hasMany(Receivable::class, 'customer_id');
    }

    public function type(){
        return $this->belongsTo(CustomerType::class, 'type_id');
    }
}
