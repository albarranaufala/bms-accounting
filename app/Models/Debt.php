<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Debt extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function debitAccount(){
        return $this->belongsTo(Account::class, 'debit_account_id');
    }

    public function creditAccount(){
        return $this->belongsTo(Account::class, 'credit_account_id');
    }

    public function client(){
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function cash(){
        return $this->belongsTo(Cash::class, 'cash_id');
    }
}
