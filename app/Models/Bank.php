<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function cashes(){
        return $this->hasMany(Cash::class, 'bank_id');
    }

    public function type(){
        return $this->belongsTo(BankType::class, 'type_id');
    }
}
