<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Account;

class Company extends Model
{
    protected $fillable = ['name','company_type'];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
