<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankType extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function banks(){
        return $this->hasMany(Bank::class, 'type_id');
    }
}
