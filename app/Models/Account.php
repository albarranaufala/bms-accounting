<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    const ACCOUNT_TYPE = [
        'header' => 'Header',
        'account' => 'Akun',
        'sub_total' => 'Subjumlah',
        'total' => 'Jumlah',
        'grand_total' => 'Total'
    ];

    const BALANCE_TYPE = [
        'debit' => 'Debit',
        'credit' => 'Kredit',
    ];

    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    public function header(){
        return $this->belongsTo(Account::class, 'header_id');
    }

    public function accounts(){
        return $this->hasMany(Account::class, 'header_id');
    }

    public function debitReceivables(){
        return $this->hasMany(Receivable::class, 'debit_account_id')->whereNull('cash_id');
    }

    public function creditReceivables(){
        return $this->hasMany(Receivable::class, 'credit_account_id')->whereNull('cash_id');
    }

    public function debitDebts(){
        return $this->hasMany(Debt::class, 'debit_account_id')->whereNull('cash_id');
    }

    public function creditDebts(){
        return $this->hasMany(Debt::class, 'credit_account_id')->whereNull('cash_id');
    }

    public function debitCashes(){
        return $this->hasMany(Cash::class, 'debit_account_id');
    }

    public function creditCashes(){
        return $this->hasMany(Cash::class, 'credit_account_id');
    }

    public function debitTransactions(){
        return $this->debitReceivables->merge($this->debitDebts)->merge($this->debitCashes);
    }

    public function creditTransactions(){
        return $this->creditReceivables->merge($this->creditDebts)->merge($this->creditCashes);
    }

    public function currentTransactions(){
        $currentTransactions = $this->debitTransactions()
            ->each(function($item){
                $item->type = 'debit';
            })
            ->merge(
                $this->creditTransactions()
                    ->each(function($item){
                        $item->type = 'credit';
                    })
            )
            ->sortByDesc('created_at')
            ->sortByDesc('date');

        switch (request()->range_type){
            case $this::RANGE_TYPE_MONTH:
                return $currentTransactions->filter(function($transaction){
                            return ((new Carbon($transaction->date))->format('m') == Carbon::now()->month) &&
                                ((new Carbon($transaction->date))->format('Y') == Carbon::now()->year);
                        });
                break;
            case $this::RANGE_TYPE_YEAR:
                return $currentTransactions->filter(function($transaction){
                            return ((new Carbon($transaction->date))->format('Y') == Carbon::now()->year);
                        });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                return $currentTransactions
                            ->whereBetween('date', [date(request()->start_date.' 00:00:00'), date(request()->end_date.' 23:59:59')]);
                break;
            default:
                return $currentTransactions;
        }
    }

    public function currentBalance(){
        switch (request()->range_type) {
            case $this::RANGE_TYPE_MONTH:
                return ($this->debitTransactions()->filter(function($transaction){
                    return ((new Carbon($transaction->date))->format('m') == Carbon::now()->month) &&
                    ((new Carbon($transaction->date))->format('Y') == Carbon::now()->year);
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                }) - $this->creditTransactions()->filter(function($transaction){
                    return ((new Carbon($transaction->date))->format('m') == Carbon::now()->month) &&
                    ((new Carbon($transaction->date))->format('Y') == Carbon::now()->year);
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                })) * ($this->balance_type === 'debit' ? 1 : -1);
                break;
            case $this::RANGE_TYPE_YEAR:
                return ($this->debitTransactions()->filter(function($transaction){
                    return ((new Carbon($transaction->date))->format('Y') == Carbon::now()->year);
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                }) - $this->creditTransactions()->filter(function($transaction){
                    return ((new Carbon($transaction->date))->format('Y') == Carbon::now()->year);
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                })) * ($this->balance_type === 'debit' ? 1 : -1);
                break;
            case $this::RANGE_TYPE_CUSTOM:
                return ($this->debitTransactions()
                    ->whereBetween('date', [date(request()->start_date.' 00:00:00'), date(request()->end_date.' 23:59:59')])
                    ->reduce(function($sum, $transaction){
                        return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                    }) - $this->creditTransactions()
                    ->whereBetween('date', [date(request()->start_date.' 00:00:00'), date(request()->end_date.' 23:59:59')])
                    ->reduce(function($sum, $transaction){
                        return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                    })) * ($this->balance_type === 'debit' ? 1 : -1);
                break;
            default:
                return ($this->debitTransactions()
                    ->reduce(function($sum, $transaction){
                        return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                    }) - $this->creditTransactions()
                    ->reduce(function($sum, $transaction){
                        return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                    })) * ($this->balance_type === 'debit' ? 1 : -1);
        }
    }

    public function previousBalance(){
        switch (request()->range_type) {
            case $this::RANGE_TYPE_MONTH:
                return ($this->debitTransactions()->filter(function($transaction){
                    if((new Carbon($transaction->date))->format('Y') == Carbon::now()->year){
                        return (new Carbon($transaction->date))->format('m') < Carbon::now()->month;
                    }
                    return (new Carbon($transaction->date))->format('Y') < Carbon::now()->year;
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                }) - $this->creditTransactions()->filter(function($transaction){
                    if((new Carbon($transaction->date))->format('Y') == Carbon::now()->year){
                        return (new Carbon($transaction->date))->format('m') < Carbon::now()->month;
                    }
                    return (new Carbon($transaction->date))->format('Y') < Carbon::now()->year;
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                })) * ($this->balance_type === 'debit' ? 1 : -1) + $this->balance;
                break;
            case $this::RANGE_TYPE_YEAR:
                return ($this->debitTransactions()->filter(function($transaction){
                    return ((new Carbon($transaction->date))->format('Y') < Carbon::now()->year);
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                }) - $this->creditTransactions()->filter(function($transaction){
                    return ((new Carbon($transaction->date))->format('Y') < Carbon::now()->year);
                })->reduce(function($sum, $transaction){
                    return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                })) * ($this->balance_type === 'debit' ? 1 : -1) + $this->balance;
                break;
            case $this::RANGE_TYPE_CUSTOM:
                return ($this->debitTransactions()
                    ->where('date', '<',date(request()->start_date.' 00:00:00'))
                    ->reduce(function($sum, $transaction){
                        return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                    }) - $this->creditTransactions()
                    ->where('date', '<',date(request()->start_date.' 00:00:00'))
                    ->reduce(function($sum, $transaction){
                        return $sum + ($transaction->bill_value ?? $transaction->payment_value);
                    })) * ($this->balance_type === 'debit' ? 1 : -1) + $this->balance;
                break;
            default:
                return $this->balance;
        }
    }

    public function totalBalance(){
        return $this->previousBalance() + $this->currentBalance();
    }
}
