<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cash extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function debitAccount(){
        return $this->belongsTo(Account::class, 'debit_account_id');
    }

    public function creditAccount(){
        return $this->belongsTo(Account::class, 'credit_account_id');
    }

    public function bank(){
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function receivable(){
        return $this->hasOne(Receivable::class, 'cash_id');
    }

    public function debt(){
        return $this->hasOne(Debt::class, 'cash_id');
    }
}
