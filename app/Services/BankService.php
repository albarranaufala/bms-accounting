<?php


namespace App\Services;


use Carbon\Carbon;

class BankService
{
    private $bank, $rangeType, $startDate, $endDate;
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    public function __construct($startDate = null, $endDate = null, $bank = null, $rangeType = null)
    {
        $this->bank = $bank;
        $this->rangeType = $rangeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function setBank($bank): BankService
    {
        $this->bank = $bank;
        return $this;
    }

    public function setRangeType($rangeType): BankService
    {
        $this->rangeType = $rangeType;
        return $this;
    }

    public function setStartDate($startDate): BankService
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setEndDate($endDate): BankService
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getBankWithFinancialInfo(){
        $this->bank->billRemainingBefore = $this->getBillRemainingBefore($this->bank);

        // mengubah hutang bank menurut range waktu
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                $this->bank->cashes = $this->bank->cashes->filter(function ($cash) {
                    return ((new Carbon($cash->date))->format('m') == Carbon::now()->month) &&
                        ((new Carbon($cash->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                $this->bank->cashes = $this->bank->cashes->filter(function ($cash) {
                    return ((new Carbon($cash->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $this->bank->cashes = $this->bank->cashes->whereBetween('date', [date($this->startDate.' 00:00:00'), date($this->endDate.' 23:59:59')]);
                break;
            default:
                $this->bank->cashes;
        }
        $this->bank->totalBill = $this->bank->cashes
            ->reduce(fn($sum, $item)=>$sum+$item->bill_value);
        $this->bank->totalPayment = $this->bank->cashes
            ->reduce(fn($sum, $item)=>$sum+$item->payment_value);
        $this->bank->billRemaining = $this->bank->totalBill - $this->bank->totalPayment;

        return $this->bank;
    }

    private function getBillRemainingBefore($cus){
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                $cashes = $cus->cashes->filter(function ($cash) {
                    if((new Carbon($cash->date))->format('Y') == Carbon::now()->year){
                        return (new Carbon($cash->date))->format('m') < Carbon::now()->month;
                    }
                    return (new Carbon($cash->date))->format('Y') < Carbon::now()->year;
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                $cashes = $cus->cashes->filter(function ($cash) {
                    return ((new Carbon($cash->date))->format('Y') < Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $cashes = $cus->cashes->where('date', '<',date($this->startDate.' 00:00:00'));
                break;
            default:
                $cashes = collect([]);
        }

        $totalBill = $cashes
            ->reduce(fn($sum, $item)=>$sum+$item->bill_value);
        $totalPayment = $cashes
            ->reduce(fn($sum, $item)=>$sum+$item->payment_value);

        return $totalBill - $totalPayment + $this->bank->balance;
    }
}
