<?php


namespace App\Services;


use Carbon\Carbon;

class CashService
{
    private $cashes, $rangeType, $startDate, $endDate;
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    public function __construct($startDate = null, $endDate = null, $cashes = null, $rangeType = null)
    {
        $this->cashes = $cashes;
        $this->rangeType = $rangeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function setCashes($cashes): CashService
    {
        $this->cashes = $cashes;
        return $this;
    }

    public function setRangeType($rangeType): CashService
    {
        $this->rangeType = $rangeType;
        return $this;
    }

    public function setStartDate($startDate): CashService
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setEndDate($endDate): CashService
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCashesByRange(){
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                return $this->cashes->filter(function ($cash) {
                    return ((new Carbon($cash->date))->format('m') == Carbon::now()->month) &&
                        ((new Carbon($cash->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                return $this->cashes->filter(function ($cash) {
                    return ((new Carbon($cash->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                return $this->cashes->whereBetween('date', [date($this->startDate.' 00:00:00'), date($this->endDate.' 23:59:59')]);
                break;
            default:
                return $this->cashes;
        }
    }
}
