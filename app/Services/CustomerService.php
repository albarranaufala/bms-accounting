<?php


namespace App\Services;


use Carbon\Carbon;

class CustomerService
{
    private $customer, $rangeType, $startDate, $endDate;
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    /**
     * CustomerService constructor.
     * @param null $startDate
     * @param null $endDate
     * @param null $customer
     * @param null $rangeType
     */
    public function __construct($startDate = null, $endDate = null, $customer = null, $rangeType = null)
    {
        $this->customer = $customer;
        $this->rangeType = $rangeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param mixed $customer
     * @return CustomerService
     */
    public function setCustomer($customer): CustomerService
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @param mixed $rangeType
     * @return CustomerService
     */
    public function setRangeType($rangeType): CustomerService
    {
        $this->rangeType = $rangeType;
        return $this;
    }

    /**
     * @param null $startDate
     * @return CustomerService
     */
    public function setStartDate($startDate): CustomerService
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @param null $endDate
     * @return CustomerService
     */
    public function setEndDate($endDate): CustomerService
    {
        $this->endDate = $endDate;

        return $this;
    }


    /**
     * @return null
     */
    public function getCustomerWithFinancialInfo(){
        $this->customer->billRemainingBefore = $this->getBillRemainingBefore($this->customer);

        // mengubah piutang customer menurut range waktu
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                $this->customer->receivables = $this->customer->receivables->filter(function ($receivable) {
                    return ((new Carbon($receivable->date))->format('m') == Carbon::now()->month) &&
                        ((new Carbon($receivable->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                $this->customer->receivables = $this->customer->receivables->filter(function ($receivable) {
                    return ((new Carbon($receivable->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $this->customer->receivables = $this->customer->receivables->whereBetween('date', [date($this->startDate.' 00:00:00'), date($this->endDate.' 23:59:59')]);
                break;
            default:
                $this->customer->receivables;
        }
        $this->customer->totalBill = $this->customer->receivables
            ->reduce(fn($sum, $item)=>$sum+$item->bill_value);
        $this->customer->totalPayment = $this->customer->receivables
            ->reduce(fn($sum, $item)=>$sum+$item->payment_value);
        $this->customer->billRemaining = $this->customer->totalPayment - $this->customer->totalBill;

        return $this->customer;
    }

    private function getBillRemainingBefore($cus){
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                $receivables = $cus->receivables->filter(function ($receivable) {
                    if((new Carbon($receivable->date))->format('Y') == Carbon::now()->year){
                        return (new Carbon($receivable->date))->format('m') < Carbon::now()->month;
                    }
                    return (new Carbon($receivable->date))->format('Y') < Carbon::now()->year;
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                $receivables = $cus->receivables->filter(function ($receivable) {
                    return ((new Carbon($receivable->date))->format('Y') < Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $receivables = $cus->receivables->where('date', '<',date($this->startDate.' 00:00:00'));
                break;
            default:
                $receivables = collect([]);
        }

        $totalBill = $receivables
            ->reduce(fn($sum, $item)=>$sum+$item->bill_value);
        $totalPayment = $receivables
            ->reduce(fn($sum, $item)=>$sum+$item->payment_value);

        return $totalPayment - $totalBill + $this->customer->balance;
    }
}
