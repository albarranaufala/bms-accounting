<?php


namespace App\Services;


use Carbon\Carbon;

class ReceivableService
{
    private $receivables, $rangeType, $startDate, $endDate;
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    public function __construct($startDate = null, $endDate = null, $receivables = null, $rangeType = null)
    {
        $this->receivables = $receivables;
        $this->rangeType = $rangeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function setReceivables($receivables): ReceivableService
    {
        $this->receivables = $receivables;
        return $this;
    }

    public function setRangeType($rangeType): ReceivableService
    {
        $this->rangeType = $rangeType;
        return $this;
    }

    public function setStartDate($startDate): ReceivableService
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setEndDate($endDate): ReceivableService
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getReceivablesByRange(){
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                return $this->receivables->filter(function ($receivable) {
                    return ((new Carbon($receivable->date))->format('m') == Carbon::now()->month) &&
                        ((new Carbon($receivable->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                return $this->receivables->filter(function ($receivable) {
                    return ((new Carbon($receivable->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                return $this->receivables->whereBetween('date', [date($this->startDate.' 00:00:00'), date($this->endDate.' 23:59:59')]);
                break;
            default:
                return $this->receivables;
        }
    }
}
