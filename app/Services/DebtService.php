<?php


namespace App\Services;


use Carbon\Carbon;

class DebtService
{
    private $debts, $rangeType, $startDate, $endDate;
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    public function __construct($startDate = null, $endDate = null, $debts = null, $rangeType = null)
    {
        $this->debts = $debts;
        $this->rangeType = $rangeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function setDebts($debts): DebtService
    {
        $this->debts = $debts;
        return $this;
    }

    public function setRangeType($rangeType): DebtService
    {
        $this->rangeType = $rangeType;
        return $this;
    }

    public function setStartDate($startDate): DebtService
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setEndDate($endDate): DebtService
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDebtsByRange(){
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                return $this->debts->filter(function ($debt) {
                    return ((new Carbon($debt->date))->format('m') == Carbon::now()->month) &&
                        ((new Carbon($debt->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                return $this->debts->filter(function ($debt) {
                    return ((new Carbon($debt->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                return $this->debts->whereBetween('date', [date($this->startDate.' 00:00:00'), date($this->endDate.' 23:59:59')]);
                break;
            default:
                return $this->debts;
        }
    }
}
