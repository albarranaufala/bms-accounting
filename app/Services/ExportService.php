<?php


namespace App\Services;


use Carbon\Carbon;

class ExportService
{
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    private $data;

    /**
     * @param mixed $data
     */
    public function setData($data): ExportService
    {
        $this->data = $data;

        return $this;
    }


    public function getRangeDateText(){
        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('M');
        switch ($this->data['range_type']){
            case $this::RANGE_TYPE_YEAR:
                $rangeDate = "Per tahun ${year}";
                break;
            case $this::RANGE_TYPE_MONTH:
                $rangeDate = "Per ${month} ${year}";
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $startDate =  $this->data['start_date'] ?? '-';
                $endDate = $this->data['end_date'] ?? '-';
                $rangeDate = "Per dari ${startDate} sampai ${endDate}";
                break;
            default:
                $rangeDate = "Semua waktu";
        }
        return $rangeDate;
    }
}
