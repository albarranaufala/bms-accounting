<?php


namespace App\Services;


use Carbon\Carbon;

class ClientService
{
    private $client, $rangeType, $startDate, $endDate;
    const RANGE_TYPE_MONTH = 'month';
    const RANGE_TYPE_YEAR = 'year';
    const RANGE_TYPE_CUSTOM = 'custom';

    public function __construct($startDate = null, $endDate = null, $client = null, $rangeType = null)
    {
        $this->client = $client;
        $this->rangeType = $rangeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function setClient($client): ClientService
    {
        $this->client = $client;
        return $this;
    }

    public function setRangeType($rangeType): ClientService
    {
        $this->rangeType = $rangeType;
        return $this;
    }

    public function setStartDate($startDate): ClientService
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setEndDate($endDate): ClientService
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getClientWithFinancialInfo(){
        $this->client->billRemainingBefore = $this->getBillRemainingBefore($this->client);

        // mengubah hutang client menurut range waktu
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                $this->client->debts = $this->client->debts->filter(function ($debt) {
                    return ((new Carbon($debt->date))->format('m') == Carbon::now()->month) &&
                        ((new Carbon($debt->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                $this->client->debts = $this->client->debts->filter(function ($debt) {
                    return ((new Carbon($debt->date))->format('Y') == Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $this->client->debts = $this->client->debts->whereBetween('date', [date($this->startDate.' 00:00:00'), date($this->endDate.' 23:59:59')]);
                break;
            default:
                $this->client->debts;
        }
        $this->client->totalBill = $this->client->debts
            ->reduce(fn($sum, $item)=>$sum+$item->bill_value);
        $this->client->totalPayment = $this->client->debts
            ->reduce(fn($sum, $item)=>$sum+$item->payment_value);
        $this->client->billRemaining = $this->client->totalBill - $this->client->totalPayment;

        return $this->client;
    }

    private function getBillRemainingBefore($cus){
        switch ($this->rangeType) {
            case $this::RANGE_TYPE_MONTH:
                $debts = $cus->debts->filter(function ($debt) {
                    if((new Carbon($debt->date))->format('Y') == Carbon::now()->year){
                        return (new Carbon($debt->date))->format('m') < Carbon::now()->month;
                    }
                    return (new Carbon($debt->date))->format('Y') < Carbon::now()->year;
                });
                break;
            case $this::RANGE_TYPE_YEAR:
                $debts = $cus->debts->filter(function ($debt) {
                    return ((new Carbon($debt->date))->format('Y') < Carbon::now()->year);
                });
                break;
            case $this::RANGE_TYPE_CUSTOM:
                $debts = $cus->debts->where('date', '<',date($this->startDate.' 00:00:00'));
                break;
            default:
                $debts = collect([]);
        }

        $totalBill = $debts
            ->reduce(fn($sum, $item)=>$sum+$item->bill_value);
        $totalPayment = $debts
            ->reduce(fn($sum, $item)=>$sum+$item->payment_value);

        return $totalBill - $totalPayment + $this->client->balance;
    }
}
