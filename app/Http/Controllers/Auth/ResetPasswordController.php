<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\PasswordResetRequestValidator;
use App\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    private $userRepository;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function config()
    {
        return view('users.config');
    }

    public function updatePassword(ChangePasswordRequest $request,User $user)
    {
        $this->userRepository->updatePassword($user,$request->validated()['password']);
        return redirect(route('users.config',['user' => $user]));
    }

    public function resetPassword(Request $target)
    {
        if (Auth::user()->hasRole('supervisor')) {
            $this->userRepository->resetPassword($target->user);
        }
        return redirect()->back();
    }

    public function rejectResetRequest($user)
    {
        if (Auth::user()->hasRole('supervisor')) {
            $this->userRepository->rejectResetRequest($user);
        }
        return redirect()->back();
    }

    public function forgotPassword()
    {
        return view('auth.forgot-password');
    }

    public function passwordResetRequest(PasswordResetRequestValidator $request)
    {
        // return User::where('username',$request->validated())->get();
        if ($this->userRepository->resetPasswordRequest($request->validated())) {
            return redirect()->back()->with('success','Permintaan reset password akun anda berhasil');
        }
    }
}
