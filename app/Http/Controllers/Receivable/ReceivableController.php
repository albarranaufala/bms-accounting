<?php

namespace App\Http\Controllers\Receivable;

use App\Exports\ReceivablesReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReceivableStoreRequest;
use App\Http\Requests\ReceivableUpdateRequest;
use App\Repositories\Account\AccountRepositoryInterface;
use App\Repositories\Receivable\Customer\CustomerRepositoryInterface;
use App\Repositories\Receivable\ReceivableRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class ReceivableController extends Controller
{
    private $receivableRepository;

    public function __construct(ReceivableRepositoryInterface $receivableRepository)
    {
        $this->receivableRepository = $receivableRepository;
    }

    public function index(Request $request)
    {
        $receivables = $this->receivableRepository
            ->getAllByRange($request);

        return view('receivables.journals.index', compact('receivables'));
    }

    public function create(CustomerRepositoryInterface $customerRepository, AccountRepositoryInterface $accountRepository)
    {
        $customers = $customerRepository->getAll();
        $accounts = $accountRepository->getAll();

        return view('receivables.journals.create', compact('customers', 'accounts'));
    }

    public function store(ReceivableStoreRequest $request)
    {
        $this->receivableRepository->create($request->validated());

        return redirect(route('receivables.journals.index'));
    }

    public function edit($id, CustomerRepositoryInterface $customerRepository, AccountRepositoryInterface $accountRepository)
    {
        $receivable = $this->receivableRepository->findById($id);
        $customers = $customerRepository->getAll();
        $accounts = $accountRepository->getAll();
        return view('receivables.journals.edit', compact('receivable', 'customers', 'accounts'));
    }

    public function update(ReceivableUpdateRequest $request, $id)
    {
        $this->receivableRepository->update($id, $request->validated());

        return redirect(route('receivables.journals.index'));
    }

    public function destroy($id)
    {
        $this->receivableRepository->delete($id);

        return redirect(route('receivables.journals.index'));
    }

    public function exportExcel(Request $request, ExportService $exportService){
        $receivables = $this->receivableRepository
            ->getAllByRange($request);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new ReceivablesReport($receivables, $rangeDate))->download("Jurnal Piutang_${rangeDate}.xlsx");
    }
}
