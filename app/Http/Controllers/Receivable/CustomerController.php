<?php

namespace App\Http\Controllers\Receivable;

use App\Exports\CustomerReport;
use App\Exports\CustomersReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerStoreRequest;
use App\Http\Requests\CustomerTypeStoreRequest;
use App\Http\Requests\CustomerTypeUpdateRequest;
use App\Http\Requests\CustomerUpdateRequest;
use App\Repositories\Receivable\Customer\CustomerRepositoryInterface;
use App\Repositories\Receivable\Customer\CustomerTypeRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    private $customerRepository;
    private $customerTypeRepository;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerTypeRepositoryInterface $customerTypeRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, CustomerTypeRepositoryInterface $customerTypeRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->customerTypeRepository = $customerTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $customerTypes = $this->customerTypeRepository->getAllByRange($request);
        $customers = $this->customerRepository->getAllByRange($request);

        return view('receivables.customers.index', compact('customers', 'customerTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $customerTypes = $this->customerTypeRepository->getAll();

        return view('receivables.customers.create', compact('customerTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(CustomerStoreRequest $request)
    {
        $this->customerRepository->create($request->validated());

        return redirect(route('receivables.customers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function show(Request $request, $id)
    {
        $customer = $this->customerRepository->findByIdAndRange($id, $request);

        return view('receivables.customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->findById($id);
        $customerTypes = $this->customerTypeRepository->getAll();

        return view('receivables.customers.edit', compact('customer', 'customerTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(CustomerUpdateRequest $request, $id)
    {
        $this->customerRepository->update($id, $request->validated());

        return redirect(route('receivables.customers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->customerRepository->delete($id);

        return redirect(route('receivables.customers.index'));
    }

    /**
     * @param CustomerTypeStoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeType(CustomerTypeStoreRequest $request){
        $this->customerTypeRepository->create($request->validated());

        return redirect(route('receivables.customers.create'));
    }

    /**
     * @param CustomerTypeUpdateRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateType(CustomerTypeUpdateRequest $request, $id){
        $this->customerTypeRepository->update($id, $request->validated());

        return redirect(route('receivables.customers.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @param ExportService $exportService
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function customerExportExcel(Request $request, $id, ExportService $exportService){
        $customer = $this->customerRepository->findByIdAndRange($id, $request);
        $customerCode = $customer->code;
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new CustomerReport($customer, $rangeDate))->download("Kartu_${customerCode}_${rangeDate}.xlsx");
    }

    /**
     * @param Request $request
     * @param ExportService $exportService
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function customersExportExcel(Request $request, ExportService $exportService){
        $customerTypes = $this->customerTypeRepository->getAllByRange($request);
        $customers = $this->customerRepository->getAllByRange($request);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new CustomersReport($customers, $customerTypes, $rangeDate))->download("Rekapitulasi Piutang_${rangeDate}.xlsx");
    }
}
