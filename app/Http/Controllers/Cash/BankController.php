<?php

namespace App\Http\Controllers\Cash;

use App\Exports\BankReport;
use App\Exports\BanksReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankStoreRequest;
use App\Http\Requests\BankTypeStoreRequest;
use App\Http\Requests\BankTypeUpdateRequest;
use App\Http\Requests\BankUpdateRequest;
use App\Repositories\Cash\Bank\BankRepositoryInterface;
use App\Repositories\Cash\Bank\BankTypeRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class BankController extends Controller
{
    private BankRepositoryInterface $bankRepository;
    private BankTypeRepositoryInterface $bankTypeRepository;

    public function __construct(BankRepositoryInterface $bankRepository, BankTypeRepositoryInterface $bankTypeRepository)
    {
        $this->bankRepository = $bankRepository;
        $this->bankTypeRepository = $bankTypeRepository;
    }

    public function index(Request $request)
    {
        $bankTypes = $this->bankTypeRepository->getAllByRange($request);
        $banks = $this->bankRepository->getAllByRange($request);

        return view('cashes.banks.index', compact('banks', 'bankTypes'));
    }

    public function create()
    {
        $bankTypes = $this->bankTypeRepository->getAll();

        return view('cashes.banks.create', compact('bankTypes'));
    }

    public function store(BankStoreRequest $request)
    {
        $this->bankRepository->create($request->validated());

        return redirect(route('cashes.banks.index'));
    }

    public function show(Request $request, $id)
    {
        $bank = $this->bankRepository->findByIdAndRange($id, $request);

        return view('cashes.banks.show', compact('bank'));
    }

    public function edit($id)
    {
        $bank = $this->bankRepository->findById($id);
        $bankTypes = $this->bankTypeRepository->getAll();

        return view('cashes.banks.edit', compact('bank', 'bankTypes'));
    }

    public function update(BankUpdateRequest $request, $id)
    {
        $this->bankRepository->update($id, $request->validated());

        return redirect(route('cashes.banks.index'));
    }

    public function destroy($id)
    {
        $this->bankRepository->delete($id);

        return redirect(route('cashes.banks.index'));
    }

    public function storeType(BankTypeStoreRequest $request){
        $this->bankTypeRepository->create($request->validated());

        return redirect(route('cashes.banks.create'));
    }

    public function updateType(BankTypeUpdateRequest $request, $id){
        $this->bankTypeRepository->update($id, $request->validated());

        return redirect(route('cashes.banks.index'));
    }

    public function bankExportExcel(Request $request, $id, ExportService $exportService){
        $bank = $this->bankRepository->findByIdAndRange($id, $request);
        $bankCode = $bank->code;
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new BankReport($bank, $rangeDate))->download("Kartu_${bankCode}_${rangeDate}.xlsx");
    }

    public function banksExportExcel(Request $request, ExportService $exportService){
        $bankTypes = $this->bankTypeRepository->getAllByRange($request);
        $banks = $this->bankRepository->getAllByRange($request);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new BanksReport($banks, $bankTypes, $rangeDate))->download("Rekapitulasi Kas_${rangeDate}.xlsx");
    }
}
