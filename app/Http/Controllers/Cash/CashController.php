<?php

namespace App\Http\Controllers\Cash;

use App\Exports\CashesReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CashStoreRequest;
use App\Http\Requests\CashUpdateRequest;
use App\Repositories\Account\AccountRepositoryInterface;
use App\Repositories\Cash\Bank\BankRepositoryInterface;
use App\Repositories\Cash\CashRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class CashController extends Controller
{
    private CashRepositoryInterface $cashRepository;

    public function __construct(CashRepositoryInterface $cashRepository)
    {
        $this->cashRepository = $cashRepository;
    }

    public function index(Request $request)
    {
        $cashes = $this->cashRepository
            ->getAllByRange($request);

        return view('cashes.entries.index', compact('cashes'));
    }

    public function create(BankRepositoryInterface $bankRepository, AccountRepositoryInterface $accountRepository)
    {
        $banks = $bankRepository->getAll();
        $accounts = $accountRepository->getAll();

        return view('cashes.entries.create', compact('banks', 'accounts'));
    }

    public function store(CashStoreRequest $request)
    {
        $this->cashRepository->create($request->validated());

        return redirect(route('cashes.entries.index'));
    }

    public function storeAndCreate(CashStoreRequest $request)
    {
        $this->cashRepository->create($request->validated());
        
        return redirect(route('cashes.entries.create'))->with('status', 'Menambah entry baru berhasil!');
    }

    public function edit($id, BankRepositoryInterface $bankRepository, AccountRepositoryInterface $accountRepository)
    {
        $cash = $this->cashRepository->findById($id);
        $banks = $bankRepository->getAll();
        $accounts = $accountRepository->getAll();
        return view('cashes.entries.edit', compact('cash', 'banks','accounts'));
    }

    public function update(CashUpdateRequest $request, $id)
    {
        $this->cashRepository->update($id, $request->validated());

        return redirect(route('cashes.entries.index'));
    }

    public function destroy($id)
    {
        $this->cashRepository->delete($id);

        return redirect(route('cashes.entries.index'));
    }

    public function exportExcel(Request $request, ExportService $exportService){
        $cashes = $this->cashRepository
            ->getAllByRange($request);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new CashesReport($cashes, $rangeDate))->download("Entry Kas_${rangeDate}.xlsx");
    }
}
