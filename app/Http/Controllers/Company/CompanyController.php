<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Models\Company;

class CompanyController extends Controller
{
    private $companyRepository;

    /**
     * Identity and Access Management Controller Constructor
     * @param CompanyRepositoryInterface $rolerepository
     */

     public function __construct(CompanyRepositoryInterface $companyRepository)
     {
         $this->companyRepository = $companyRepository;
     }

    public function index()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'superadmin',403);
        $companies = $this->companyRepository->getAll();
        return view('companies.index',compact('companies'));
    }

    public function store(CompanyStoreRequest $request)
    {
        $this->companyRepository->create($request->validated());
        return redirect(route('companies.index'));
    }

    public function edit(Company $company)
    {
        if(Auth::user()->hasRole('supervisor') && Auth::user()->company_id !== $company->id) return redirect(route('companies.edit',['company' => Auth::user()->company_id]));
        $users = $this->companyRepository->getCompanyusers($company->id);
        $supervisors = $this->companyRepository->getCompanySupervisors($company->id);
        return view('companies.edit',compact('company','users','supervisors'));
    }

    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $this->companyRepository->update($company->id,$request->validated());
        return redirect()->back();
    }

    public function destroy(Company $company)
    {
        $this->companyRepository->delete($company->id);
        return redirect(route('companies.index'));
    }
}
