<?php

namespace App\Http\Controllers\IAM;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Roles\RoleRepositoryInterface;
use App\Repositories\Permission\PermissionRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Company;
use Spatie\Permission\Models\Role;

class IAMController extends Controller
{
    private $roleRepository;
    private $permissionRepository;
    private $userRepository;

    /**
     * Identity and Access Management Controller Constructor
     * @param RoleRepositoryInterface $roleRepository
     * @param PermissionRepositoryInterface $permissionRepository
     */
    public function __construct(RoleRepositoryInterface $roleRepository, PermissionRepositoryInterface $permissionRepository, UserRepositoryInterface $userRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
        $this->userRepository = $userRepository;
    }

    public function userIndex()
    {
        $users = User::where('company_id',Auth::user()->company_id)
            ->whereHas('roles', function($q) {
                $q->where('id','>',2);
            })->get();
        $companies = Company::all();
        $requesting_reset = User::where('company_id',Auth::user()->company_id)
            ->where('is_requesting_password_reset',1)->get();
        $roles = Role::all();

        return view('IAM.index', compact('users','companies','roles','requesting_reset'));
    }

    public function userStore(UserStoreRequest $request)
    {
        $this->userRepository->create($request->validated());
        return redirect()->back();
    }

    public function userEdit(User $user)
    {
        $companies = Company::all();
        $roles = Role::all();
        return view('iam.users.edit',compact('user','companies','roles'));
    }

    public function userUpdate(UserUpdateRequest $request)
    {
        if($this->userRepository->update($request->validated()['user_id'],$request->validated())) return redirect()->back();
    }

    public function userDestroy(User $user)
    {
        if($this->userRepository->delete($user->id)) return redirect()->back();
    }

    /**
     * Superadmin methods
     */

    public function superadminIndex()
    {
        $superadmins = Role::find(1)->users()->get();
        return view('IAM.superadmins.index',compact('superadmins'));
    }

    /**
     * Display a listing of ROLE RESOURCE
     * 
     */
    public function roleIndex()
    {
        $roles = $this->roleRepository->getAll();
        $permissions = $this->permissionRepository->getAll();
        return view('IAM.roles.index', compact('roles','permissions'));
    }

    public function roleStore(RoleStoreRequest $request)
    {
        $storeNewRole = $this->roleRepository->create($request);
        if($storeNewRole) return redirect(route('roles.index'));
    }

    public function roleEdit(Role $role)
    {
        $rolePermissions = $role->permissions->pluck('name');
        $users = User::role($role)->get();
        $permissions = $this->permissionRepository->getAll();
        return view('IAM.roles.edit', compact('role','rolePermissions','users','permissions'));
    }

    public function roleUpdate(RoleUpdateRequest $request,Role $role)
    {
        $this->roleRepository->update($role->id,$request->validated());
        return redirect(route('roles.index'));
    }

    public function roleDestroy(Role $role)
    {
        $this->roleRepository->delete($role->id);
        return redirect(route('roles.index'));
    }
}
