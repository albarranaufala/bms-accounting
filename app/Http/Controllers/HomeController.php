<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;
use App\Models\Receivable;
use App\Models\Debt;
use App\Models\Cash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = Auth::user()->getRoleNames()[0];
        switch($role){
            case 'superadmin': 
                return $this->superadminDashboard();
            break;
            case 'supervisor':
                return $this->supervisorDashboard();
            break;
            case 'Divisi Piutang':
                return $this->receivablesDashboard();
            break;
            case 'Divisi Hutang':
                return $this->debtsDashboard();
            break;
            case 'Divisi Kas':
                return $this->cashesDashboard();
            case 'Divisi Akuntansi':
                return $this->accountDashboard();
            break;
        }
    }

    public function superadminDashboard()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'superadmin',403);
        $companies = Company::all();
        return view('home',compact('companies'));
    }

    public function supervisorDashboard()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'supervisor',403);
        $receivablesCount = Receivable::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();

        $debtsCount = Debt::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();

        $cashesCount = Cash::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();

        return view('home',compact('receivablesCount','debtsCount','cashesCount'));
    }

    public function accountDashboard()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'Divisi Akuntansi', 403);
        $receivablesCount = Receivable::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();

        $debtsCount = Debt::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();

        $cashesCount = Cash::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();

        return view('home',compact('receivablesCount','debtsCount','cashesCount'));
    }

    public function receivablesDashboard()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'Divisi Piutang',403);
        $receivablesCount = Receivable::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();
        return view('home',compact('receivablesCount'));
    }

    public function debtsDashboard()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'Divisi Hutang',403);
        $debtsCount = Debt::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();
        return view('home',compact('debtsCount'));
    }

    public function cashesDashboard()
    {
        abort_if(Auth::user()->getRoleNames()[0] !== 'Divisi Kas',403);
        $cashesCount = Cash::where('company_id',Auth::user()->company_id)
        ->where( function ($query){
            $query->where('debit_account_id',null)
                    ->orWhere('credit_account_id',null);
        })
        ->get()->count();
        return view('home',compact('cashesCount'));
    }
}
