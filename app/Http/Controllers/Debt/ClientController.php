<?php

namespace App\Http\Controllers\Debt;

use App\Exports\ClientReport;
use App\Exports\ClientsReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientTypeStoreRequest;
use App\Http\Requests\ClientTypeUpdateRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Repositories\Debt\Client\ClientRepositoryInterface;
use App\Repositories\Debt\Client\ClientTypeRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private ClientRepositoryInterface $clientRepository;
    private ClientTypeRepositoryInterface $clientTypeRepository;

    public function __construct(ClientRepositoryInterface $clientRepository, ClientTypeRepositoryInterface $clientTypeRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->clientTypeRepository = $clientTypeRepository;
    }

    public function index(Request $request)
    {
        $clientTypes = $this->clientTypeRepository->getAllByRange($request);
        $clients = $this->clientRepository->getAllByRange($request);

        return view('debts.clients.index', compact('clients', 'clientTypes'));
    }

    public function create()
    {
        $clientTypes = $this->clientTypeRepository->getAll();

        return view('debts.clients.create', compact('clientTypes'));
    }

    public function store(ClientStoreRequest $request)
    {
        $this->clientRepository->create($request->validated());

        return redirect(route('debts.clients.index'));
    }

    public function show(Request $request, $id)
    {
        $client = $this->clientRepository->findByIdAndRange($id, $request);

        return view('debts.clients.show', compact('client'));
    }

    public function edit($id)
    {
        $client = $this->clientRepository->findById($id);
        $clientTypes = $this->clientTypeRepository->getAll();

        return view('debts.clients.edit', compact('client', 'clientTypes'));
    }

    public function update(ClientUpdateRequest $request, $id)
    {
        $this->clientRepository->update($id, $request->validated());

        return redirect(route('debts.clients.index'));
    }

    public function destroy($id)
    {
        $this->clientRepository->delete($id);

        return redirect(route('debts.clients.index'));
    }

    public function storeType(ClientTypeStoreRequest $request){
        $this->clientTypeRepository->create($request->validated());

        return redirect(route('debts.clients.create'));
    }

    public function updateType(ClientTypeUpdateRequest $request, $id){
        $this->clientTypeRepository->update($id, $request->validated());

        return redirect(route('debts.clients.index'));
    }

    public function clientExportExcel(Request $request, $id, ExportService $exportService){
        $client = $this->clientRepository->findByIdAndRange($id, $request);
        $clientCode = $client->code;
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new ClientReport($client, $rangeDate))->download("Kartu_${clientCode}_${rangeDate}.xlsx");
    }

    public function clientsExportExcel(Request $request, ExportService $exportService){
        $clientTypes = $this->clientTypeRepository->getAllByRange($request);
        $clients = $this->clientRepository->getAllByRange($request);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new ClientsReport($clients, $clientTypes, $rangeDate))->download("Rekapitulasi Hutang_${rangeDate}.xlsx");
    }
}
