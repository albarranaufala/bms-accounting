<?php

namespace App\Http\Controllers\Debt;

use App\Exports\DebtsReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\DebtStoreRequest;
use App\Http\Requests\DebtUpdateRequest;
use App\Repositories\Account\AccountRepositoryInterface;
use App\Repositories\Debt\Client\ClientRepositoryInterface;
use App\Repositories\Debt\DebtRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class DebtController extends Controller
{
    private DebtRepositoryInterface $debtRepository;

    public function __construct(DebtRepositoryInterface $debtRepository)
    {
        $this->debtRepository = $debtRepository;
    }

    public function index(Request $request)
    {
        $debts = $this->debtRepository
            ->getAllByRange($request);

        return view('debts.journals.index', compact('debts'));
    }

    public function create(ClientRepositoryInterface $clientRepository, AccountRepositoryInterface $accountRepository)
    {
        $clients = $clientRepository->getAll();
        $accounts = $accountRepository->getAll();

        return view('debts.journals.create', compact('clients', 'accounts'));
    }

    public function store(DebtStoreRequest $request)
    {
        $this->debtRepository->create($request->validated());

        return redirect(route('debts.journals.index'));
    }

    public function edit($id, ClientRepositoryInterface $clientRepository, AccountRepositoryInterface $accountRepository)
    {
        $debt = $this->debtRepository->findById($id);
        $clients = $clientRepository->getAll();
        $accounts = $accountRepository->getAll();
        return view('debts.journals.edit', compact('debt', 'clients', 'accounts'));
    }

    public function update(DebtUpdateRequest $request, $id)
    {
        $this->debtRepository->update($id, $request->validated());

        return redirect(route('debts.journals.index'));
    }

    public function destroy($id)
    {
        $this->debtRepository->delete($id);

        return redirect(route('debts.journals.index'));
    }

    public function exportExcel(Request $request, ExportService $exportService){
        $debts = $this->debtRepository
            ->getAllByRange($request);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new DebtsReport($debts, $rangeDate))->download("Jurnal Hutang_${rangeDate}.xlsx");
    }
}
