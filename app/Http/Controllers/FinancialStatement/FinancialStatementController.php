<?php

namespace App\Http\Controllers\FinancialStatement;

use App\Exports\BalanceSheetReport;
use App\Exports\GeneralEntriesReport;
use App\Exports\IncomeStatementReport;
use App\Http\Controllers\Controller;
use App\Repositories\Account\AccountRepositoryInterface;
use App\Repositories\Cash\CashRepositoryInterface;
use App\Repositories\Debt\DebtRepositoryInterface;
use App\Repositories\Receivable\ReceivableRepositoryInterface;
use App\Services\ExportService;
use Illuminate\Http\Request;

class FinancialStatementController extends Controller
{
    private AccountRepositoryInterface $accountRepository;

    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function index(Request $request, ReceivableRepositoryInterface $receivableRepository, DebtRepositoryInterface $debtRepository, CashRepositoryInterface $cashRepository)
    {
        $receivables = $receivableRepository->getAllCompletedByRange($request);
        $debts = $debtRepository->getAllCompletedByRange($request);
        $cashes = $cashRepository->getAllCompletedByRange($request);
        $transactions = $receivables->merge($debts)->merge($cashes);
        return view('financial_statements.index', compact('transactions'));
    }

    public function show($type)
    {
        switch ($type) {
            case 'balance_sheet':
                //Aset Lancar
                $currentAssets = $this->accountRepository->findByAccountNum('1-1000');
                $sumPreviousBalanceCurrentAsset = $currentAssets->accounts->reduce($this->calculateSum('previousBalance'));
                $sumCurrentBalanceCurrentAsset = $currentAssets->accounts->reduce($this->calculateSum('currentBalance'));
                $sumTotalBalanceCurrentAsset = $sumPreviousBalanceCurrentAsset + $sumCurrentBalanceCurrentAsset;

                //Aset Tetap
                $fixedAssets = $this->accountRepository->findByAccountNum('1-2000');
                $sumPreviousBalanceFixedAsset = $fixedAssets->accounts->reduce($this->calculateSum('previousBalance'));
                $sumCurrentBalanceFixedAsset = $fixedAssets->accounts->reduce($this->calculateSum('currentBalance'));
                $sumTotalBalanceFixedAsset = $sumPreviousBalanceFixedAsset + $sumCurrentBalanceFixedAsset;

                //Aset Lain-Lain
                $otherAssets = $this->accountRepository->findByAccountNum('1-3000');
                $sumPreviousBalanceOtherAsset = $otherAssets->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceOtherAsset = $otherAssets->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceOtherAsset = $sumPreviousBalanceOtherAsset + $sumCurrentBalanceOtherAsset;

                $sumPreviousBalanceAsset = $sumPreviousBalanceCurrentAsset + $sumPreviousBalanceFixedAsset + $sumPreviousBalanceOtherAsset;
                $sumCurrentBalanceAsset = $sumCurrentBalanceCurrentAsset + $sumCurrentBalanceFixedAsset + $sumCurrentBalanceOtherAsset;
                $sumTotalBalanceAsset = $sumTotalBalanceCurrentAsset + $sumTotalBalanceFixedAsset + $sumTotalBalanceOtherAsset;

                //Kewajiban Lancar
                $currentLiabilities = $this->accountRepository->findByAccountNum('2-1000');
                $sumPreviousBalanceCurrentLiability = $currentLiabilities->accounts->reduce($this->calculateSum('previousBalance'));
                $sumCurrentBalanceCurrentLiability = $currentLiabilities->accounts->reduce($this->calculateSum('currentBalance'));
                $sumTotalBalanceCurrentLiability = $sumPreviousBalanceCurrentLiability + $sumCurrentBalanceCurrentLiability;

                //Kewajiban Jangka Panjang
                $longTermLiabilities = $this->accountRepository->findByAccountNum('2-2000');
                $sumPreviousBalanceLongTermLiability = $longTermLiabilities->accounts->reduce($this->calculateSum('previousBalance'));
                $sumCurrentBalanceLongTermLiability = $longTermLiabilities->accounts->reduce($this->calculateSum('currentBalance'));
                $sumTotalBalanceLongTermLiability = $sumPreviousBalanceLongTermLiability + $sumCurrentBalanceLongTermLiability;

                //Kewajiban Lain-Lain
                $otherLiabilities = $this->accountRepository->findByAccountNum('2-3000');
                $sumPreviousBalanceOtherLiability = $otherLiabilities->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceOtherLiability = $otherLiabilities->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceOtherLiability = $sumPreviousBalanceOtherLiability + $sumCurrentBalanceOtherLiability;

                $sumPreviousBalanceLiability = $sumPreviousBalanceCurrentLiability + $sumPreviousBalanceLongTermLiability + $sumPreviousBalanceOtherLiability;
                $sumCurrentBalanceLiability = $sumCurrentBalanceCurrentLiability + $sumCurrentBalanceLongTermLiability + $sumCurrentBalanceOtherLiability;
                $sumTotalBalanceLiability = $sumTotalBalanceCurrentLiability + $sumTotalBalanceLongTermLiability + $sumTotalBalanceOtherLiability;

                //Ekuitas
                $equity = $this->accountRepository->findByAccountNum('3-0000');
                $sumPreviousBalanceEquity = $equity->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceEquity = $equity->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceEquity = $sumPreviousBalanceEquity + $sumCurrentBalanceEquity;

                $sumPreviousBalanceLiabilityAndEquity = $sumPreviousBalanceLiability + $sumPreviousBalanceEquity;
                $sumCurrentBalanceLiabilityAndEquity = $sumCurrentBalanceLiability + $sumCurrentBalanceEquity;
                $sumTotalBalanceLiabilityAndEquity = $sumTotalBalanceLiability + $sumTotalBalanceEquity;

                return view('financial_statements.balance_sheet', compact(
                    'currentAssets',
                    'sumPreviousBalanceCurrentAsset',
                    'sumCurrentBalanceCurrentAsset',
                    'sumTotalBalanceCurrentAsset',
                    'fixedAssets',
                    'sumPreviousBalanceFixedAsset',
                    'sumCurrentBalanceFixedAsset',
                    'sumTotalBalanceFixedAsset',
                    'otherAssets',
                    'sumPreviousBalanceOtherAsset',
                    'sumCurrentBalanceOtherAsset',
                    'sumTotalBalanceOtherAsset',
                    'sumPreviousBalanceAsset',
                    'sumCurrentBalanceAsset',
                    'sumTotalBalanceAsset',
                    'currentLiabilities',
                    'sumPreviousBalanceCurrentLiability',
                    'sumCurrentBalanceCurrentLiability',
                    'sumTotalBalanceCurrentLiability',
                    'longTermLiabilities',
                    'sumPreviousBalanceLongTermLiability',
                    'sumCurrentBalanceLongTermLiability',
                    'sumTotalBalanceLongTermLiability',
                    'otherLiabilities',
                    'sumPreviousBalanceOtherLiability',
                    'sumCurrentBalanceOtherLiability',
                    'sumTotalBalanceOtherLiability',
                    'sumPreviousBalanceLiability',
                    'sumCurrentBalanceLiability',
                    'sumTotalBalanceLiability',
                    'equity',
                    'sumPreviousBalanceEquity',
                    'sumCurrentBalanceEquity',
                    'sumTotalBalanceEquity',
                    'sumPreviousBalanceLiabilityAndEquity',
                    'sumCurrentBalanceLiabilityAndEquity',
                    'sumTotalBalanceLiabilityAndEquity',
                ));
            case 'income_statement':
                $operatingRevenue = $this->accountRepository->findByAccountNum('4-1000');
                $sumPreviousBalanceOperatingRevenue = $operatingRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceOperatingRevenue = $operatingRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceOperatingRevenue = $sumPreviousBalanceOperatingRevenue + $sumCurrentBalanceOperatingRevenue;

                $directCost = $this->accountRepository->findByAccountNum('5-1000');
                $sumPreviousBalanceDirectCost = $directCost->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceDirectCost = $directCost->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceDirectCost = $sumPreviousBalanceDirectCost + $sumCurrentBalanceDirectCost;

                $previousGrossProfit = $sumPreviousBalanceOperatingRevenue - $sumPreviousBalanceDirectCost;
                $currentGrossProfit = $sumCurrentBalanceOperatingRevenue - $sumCurrentBalanceDirectCost;
                $totalGrossProfit = $previousGrossProfit + $currentGrossProfit;

                $generalCost = $this->accountRepository->findByAccountNum('5-2000');
                $sumPreviousBalanceGeneralCost = $generalCost->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceGeneralCost = $generalCost->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceGeneralCost = $sumPreviousBalanceGeneralCost + $sumCurrentBalanceGeneralCost;

                $previousOperatingProfit = $previousGrossProfit - $sumPreviousBalanceGeneralCost;
                $currentOperatingProfit = $currentGrossProfit - $sumCurrentBalanceGeneralCost;
                $totalOperatingProfit = $previousOperatingProfit + $currentOperatingProfit;

                $otherRevenue = $this->accountRepository->findByAccountNum('4-2000');
                $sumPreviousBalanceOtherRevenue = $otherRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceOtherRevenue = $otherRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceOtherRevenue = $sumPreviousBalanceOtherRevenue + $sumCurrentBalanceOtherRevenue;

                $otherCost = $this->accountRepository->findByAccountNum('5-3000');
                $sumPreviousBalanceOtherCost = $otherCost->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
                $sumCurrentBalanceOtherCost = $otherCost->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
                $sumTotalBalanceOtherCost = $sumPreviousBalanceOtherCost + $sumCurrentBalanceOtherCost;

                $previousProfitBeforeTax = $previousOperatingProfit + $sumPreviousBalanceOtherRevenue - $sumPreviousBalanceOtherCost;
                $currentProfitBeforeTax = $currentOperatingProfit + $sumCurrentBalanceOtherRevenue - $sumCurrentBalanceOtherCost;
                $totalProfitBeforeTax = $previousProfitBeforeTax + $currentProfitBeforeTax;

                $tax = $this->accountRepository->findByAccountNum('5-3999');

                $previousProfitAfterTax = $previousProfitBeforeTax - $tax->previousBalance();
                $currentProfitAfterTax = $currentProfitBeforeTax - $tax->currentBalance();
                $totalProfitAfterTax = $previousProfitAfterTax + $currentProfitAfterTax;

                return view('financial_statements.income_statement', compact(
                    'operatingRevenue',
                    'sumPreviousBalanceOperatingRevenue',
                    'sumCurrentBalanceOperatingRevenue',
                    'sumTotalBalanceOperatingRevenue',
                    'directCost',
                    'sumPreviousBalanceDirectCost',
                    'sumCurrentBalanceDirectCost',
                    'sumTotalBalanceDirectCost',
                    'previousGrossProfit',
                    'currentGrossProfit',
                    'totalGrossProfit',
                    'generalCost',
                    'sumPreviousBalanceGeneralCost',
                    'sumCurrentBalanceGeneralCost',
                    'sumTotalBalanceGeneralCost',
                    'previousOperatingProfit',
                    'currentOperatingProfit',
                    'totalOperatingProfit',
                    'otherRevenue',
                    'sumPreviousBalanceOtherRevenue',
                    'sumCurrentBalanceOtherRevenue',
                    'sumTotalBalanceOtherRevenue',
                    'otherCost',
                    'sumPreviousBalanceOtherCost',
                    'sumCurrentBalanceOtherCost',
                    'sumTotalBalanceOtherCost',
                    'previousProfitBeforeTax',
                    'currentProfitBeforeTax',
                    'totalProfitBeforeTax',
                    'tax',
                    'previousProfitAfterTax',
                    'currentProfitAfterTax',
                    'totalProfitAfterTax',
                ));
            default:
                return abort(404);
        }
    }

    private function calculateSum($function){
        return function($sum, $item) use ($function){
            return $sum + $item->accounts->reduce(function($sum2, $item2) use ($function){
                    return $sum2 + $item2->$function();
                });
        };
    }

    public function exportGeneralEntriesExcel(Request $request, ExportService $exportService, ReceivableRepositoryInterface $receivableRepository, DebtRepositoryInterface $debtRepository, CashRepositoryInterface $cashRepository){
        $receivables = $receivableRepository->getAllCompletedByRange($request);
        $debts = $debtRepository->getAllCompletedByRange($request);
        $cashes = $cashRepository->getAllCompletedByRange($request);
        $transactions = $receivables->merge($debts)->merge($cashes);
        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        return (new GeneralEntriesReport($transactions, $rangeDate))->download("Jurnal Umum_${rangeDate}.xlsx");
    }

    public function exportBalanceSheetExcel(Request $request, ExportService $exportService){
        //Aset Lancar
        $currentAssets = $this->accountRepository->findByAccountNum('1-1000');
        $sumPreviousBalanceCurrentAsset = $currentAssets->accounts->reduce($this->calculateSum('previousBalance'));
        $sumCurrentBalanceCurrentAsset = $currentAssets->accounts->reduce($this->calculateSum('currentBalance'));
        $sumTotalBalanceCurrentAsset = $sumPreviousBalanceCurrentAsset + $sumCurrentBalanceCurrentAsset;

        //Aset Tetap
        $fixedAssets = $this->accountRepository->findByAccountNum('1-2000');
        $sumPreviousBalanceFixedAsset = $fixedAssets->accounts->reduce($this->calculateSum('previousBalance'));
        $sumCurrentBalanceFixedAsset = $fixedAssets->accounts->reduce($this->calculateSum('currentBalance'));
        $sumTotalBalanceFixedAsset = $sumPreviousBalanceFixedAsset + $sumCurrentBalanceFixedAsset;

        //Aset Lain-Lain
        $otherAssets = $this->accountRepository->findByAccountNum('1-3000');
        $sumPreviousBalanceOtherAsset = $otherAssets->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceOtherAsset = $otherAssets->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceOtherAsset = $sumPreviousBalanceOtherAsset + $sumCurrentBalanceOtherAsset;

        $sumPreviousBalanceAsset = $sumPreviousBalanceCurrentAsset + $sumPreviousBalanceFixedAsset + $sumPreviousBalanceOtherAsset;
        $sumCurrentBalanceAsset = $sumCurrentBalanceCurrentAsset + $sumCurrentBalanceFixedAsset + $sumCurrentBalanceOtherAsset;
        $sumTotalBalanceAsset = $sumTotalBalanceCurrentAsset + $sumTotalBalanceFixedAsset + $sumTotalBalanceOtherAsset;

        //Kewajiban Lancar
        $currentLiabilities = $this->accountRepository->findByAccountNum('2-1000');
        $sumPreviousBalanceCurrentLiability = $currentLiabilities->accounts->reduce($this->calculateSum('previousBalance'));
        $sumCurrentBalanceCurrentLiability = $currentLiabilities->accounts->reduce($this->calculateSum('currentBalance'));
        $sumTotalBalanceCurrentLiability = $sumPreviousBalanceCurrentLiability + $sumCurrentBalanceCurrentLiability;

        //Kewajiban Jangka Panjang
        $longTermLiabilities = $this->accountRepository->findByAccountNum('2-2000');
        $sumPreviousBalanceLongTermLiability = $longTermLiabilities->accounts->reduce($this->calculateSum('previousBalance'));
        $sumCurrentBalanceLongTermLiability = $longTermLiabilities->accounts->reduce($this->calculateSum('currentBalance'));
        $sumTotalBalanceLongTermLiability = $sumPreviousBalanceLongTermLiability + $sumCurrentBalanceLongTermLiability;

        //Kewajiban Lain-Lain
        $otherLiabilities = $this->accountRepository->findByAccountNum('2-3000');
        $sumPreviousBalanceOtherLiability = $otherLiabilities->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceOtherLiability = $otherLiabilities->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceOtherLiability = $sumPreviousBalanceOtherLiability + $sumCurrentBalanceOtherLiability;

        $sumPreviousBalanceLiability = $sumPreviousBalanceCurrentLiability + $sumPreviousBalanceLongTermLiability + $sumPreviousBalanceOtherLiability;
        $sumCurrentBalanceLiability = $sumCurrentBalanceCurrentLiability + $sumCurrentBalanceLongTermLiability + $sumCurrentBalanceOtherLiability;
        $sumTotalBalanceLiability = $sumTotalBalanceCurrentLiability + $sumTotalBalanceLongTermLiability + $sumTotalBalanceOtherLiability;

        //Ekuitas
        $equity = $this->accountRepository->findByAccountNum('3-0000');
        $sumPreviousBalanceEquity = $equity->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceEquity = $equity->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceEquity = $sumPreviousBalanceEquity + $sumCurrentBalanceEquity;

        $sumPreviousBalanceLiabilityAndEquity = $sumPreviousBalanceLiability + $sumPreviousBalanceEquity;
        $sumCurrentBalanceLiabilityAndEquity = $sumCurrentBalanceLiability + $sumCurrentBalanceEquity;
        $sumTotalBalanceLiabilityAndEquity = $sumTotalBalanceLiability + $sumTotalBalanceEquity;

        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        $data = compact(
            'currentAssets',
            'sumPreviousBalanceCurrentAsset',
            'sumCurrentBalanceCurrentAsset',
            'sumTotalBalanceCurrentAsset',
            'fixedAssets',
            'sumPreviousBalanceFixedAsset',
            'sumCurrentBalanceFixedAsset',
            'sumTotalBalanceFixedAsset',
            'otherAssets',
            'sumPreviousBalanceOtherAsset',
            'sumCurrentBalanceOtherAsset',
            'sumTotalBalanceOtherAsset',
            'sumPreviousBalanceAsset',
            'sumCurrentBalanceAsset',
            'sumTotalBalanceAsset',
            'currentLiabilities',
            'sumPreviousBalanceCurrentLiability',
            'sumCurrentBalanceCurrentLiability',
            'sumTotalBalanceCurrentLiability',
            'longTermLiabilities',
            'sumPreviousBalanceLongTermLiability',
            'sumCurrentBalanceLongTermLiability',
            'sumTotalBalanceLongTermLiability',
            'otherLiabilities',
            'sumPreviousBalanceOtherLiability',
            'sumCurrentBalanceOtherLiability',
            'sumTotalBalanceOtherLiability',
            'sumPreviousBalanceLiability',
            'sumCurrentBalanceLiability',
            'sumTotalBalanceLiability',
            'equity',
            'sumPreviousBalanceEquity',
            'sumCurrentBalanceEquity',
            'sumTotalBalanceEquity',
            'sumPreviousBalanceLiabilityAndEquity',
            'sumCurrentBalanceLiabilityAndEquity',
            'sumTotalBalanceLiabilityAndEquity',
        );

        return (new BalanceSheetReport($data, $rangeDate))->download("Laporan Neraca_${rangeDate}.xlsx");
    }

    public function exportIncomeStatementExcel(Request $request, ExportService $exportService){
        $operatingRevenue = $this->accountRepository->findByAccountNum('4-1000');
        $sumPreviousBalanceOperatingRevenue = $operatingRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceOperatingRevenue = $operatingRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceOperatingRevenue = $sumPreviousBalanceOperatingRevenue + $sumCurrentBalanceOperatingRevenue;

        $directCost = $this->accountRepository->findByAccountNum('5-1000');
        $sumPreviousBalanceDirectCost = $directCost->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceDirectCost = $directCost->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceDirectCost = $sumPreviousBalanceDirectCost + $sumCurrentBalanceDirectCost;

        $previousGrossProfit = $sumPreviousBalanceOperatingRevenue - $sumPreviousBalanceDirectCost;
        $currentGrossProfit = $sumCurrentBalanceOperatingRevenue - $sumCurrentBalanceDirectCost;
        $totalGrossProfit = $previousGrossProfit + $currentGrossProfit;

        $generalCost = $this->accountRepository->findByAccountNum('5-2000');
        $sumPreviousBalanceGeneralCost = $generalCost->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceGeneralCost = $generalCost->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceGeneralCost = $sumPreviousBalanceGeneralCost + $sumCurrentBalanceGeneralCost;

        $previousOperatingProfit = $previousGrossProfit - $sumPreviousBalanceGeneralCost;
        $currentOperatingProfit = $currentGrossProfit - $sumCurrentBalanceGeneralCost;
        $totalOperatingProfit = $previousOperatingProfit + $currentOperatingProfit;

        $otherRevenue = $this->accountRepository->findByAccountNum('4-2000');
        $sumPreviousBalanceOtherRevenue = $otherRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceOtherRevenue = $otherRevenue->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceOtherRevenue = $sumPreviousBalanceOtherRevenue + $sumCurrentBalanceOtherRevenue;

        $otherCost = $this->accountRepository->findByAccountNum('5-3000');
        $sumPreviousBalanceOtherCost = $otherCost->accounts->reduce(fn($sum, $item) => $sum + $item->previousBalance());
        $sumCurrentBalanceOtherCost = $otherCost->accounts->reduce(fn($sum, $item) => $sum + $item->currentBalance());
        $sumTotalBalanceOtherCost = $sumPreviousBalanceOtherCost + $sumCurrentBalanceOtherCost;

        $previousProfitBeforeTax = $previousOperatingProfit + $sumPreviousBalanceOtherRevenue - $sumPreviousBalanceOtherCost;
        $currentProfitBeforeTax = $currentOperatingProfit + $sumCurrentBalanceOtherRevenue - $sumCurrentBalanceOtherCost;
        $totalProfitBeforeTax = $previousProfitBeforeTax + $currentProfitBeforeTax;

        $tax = $this->accountRepository->findByAccountNum('5-3999');

        $previousProfitAfterTax = $previousProfitBeforeTax - $tax->previousBalance();
        $currentProfitAfterTax = $currentProfitBeforeTax - $tax->currentBalance();
        $totalProfitAfterTax = $previousProfitAfterTax + $currentProfitAfterTax;

        $rangeDate = $exportService->setData($request)
            ->getRangeDateText();

        $data = compact(
            'operatingRevenue',
            'sumPreviousBalanceOperatingRevenue',
            'sumCurrentBalanceOperatingRevenue',
            'sumTotalBalanceOperatingRevenue',
            'directCost',
            'sumPreviousBalanceDirectCost',
            'sumCurrentBalanceDirectCost',
            'sumTotalBalanceDirectCost',
            'previousGrossProfit',
            'currentGrossProfit',
            'totalGrossProfit',
            'generalCost',
            'sumPreviousBalanceGeneralCost',
            'sumCurrentBalanceGeneralCost',
            'sumTotalBalanceGeneralCost',
            'previousOperatingProfit',
            'currentOperatingProfit',
            'totalOperatingProfit',
            'otherRevenue',
            'sumPreviousBalanceOtherRevenue',
            'sumCurrentBalanceOtherRevenue',
            'sumTotalBalanceOtherRevenue',
            'otherCost',
            'sumPreviousBalanceOtherCost',
            'sumCurrentBalanceOtherCost',
            'sumTotalBalanceOtherCost',
            'previousProfitBeforeTax',
            'currentProfitBeforeTax',
            'totalProfitBeforeTax',
            'tax',
            'previousProfitAfterTax',
            'currentProfitAfterTax',
            'totalProfitAfterTax',
        );

        return (new IncomeStatementReport($data, $rangeDate))->download("Laporan Laba Rugi_${rangeDate}.xlsx");
    }
}
