<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankTypeUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'type_name' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
