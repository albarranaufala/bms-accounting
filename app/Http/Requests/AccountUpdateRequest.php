<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('account');

        return [
            'account_num' => "required|string|unique_by_company:accounts,account_num,$id",
            'name' => 'required|string',
            'balance' => 'required',
            'account_type' => 'required|in:header,account,sub_total,total,grand_total',
            'balance_type' => 'nullable|in:debit,credit',
            'header' => 'nullable|integer',
        ];
    }
}
