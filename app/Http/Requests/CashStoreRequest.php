<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'bank' => 'nullable|integer',
            'date' => 'required|date',
            'description' => 'nullable|string',
            'invoice_num' => 'nullable|string',
            'bill_value' => 'nullable',
            'payment_value' => 'nullable',
            'information' => 'nullable|string',
            'debit_account' => 'nullable|integer',
            'credit_account' => 'nullable|integer',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
