<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'code' => 'required|string|unique_by_company:banks,code',
            'bank_account' => 'nullable|string',
            'owner_name' => 'nullable|string',
            'class' => 'nullable|string',
            'balance' => 'required',
            'type' => 'required|integer'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
