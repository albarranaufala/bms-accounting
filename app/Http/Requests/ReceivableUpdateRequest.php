<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReceivableUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer' => 'required|integer',
            'date' => 'required|date',
            'description' => 'nullable|string',
            'invoice_num' => 'nullable|string',
            'bill_value' => 'nullable',
            'payment_value' => 'nullable',
            'information' => 'nullable|string',
            'debit_account' => 'nullable|integer',
            'credit_account' => 'nullable|integer',
        ];
    }
}
