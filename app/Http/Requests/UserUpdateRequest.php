<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required'],
            'name' => ['required','string','max:255'],
            'email' => ['required','string','email','max:255','unique:users,email,' . $this->user_id],
            'username' => ['required','string','max:255','unique:users,username,' . $this->user_id],
            'role_id' => ['required','exists:roles,id'],
            'company_id' => ['nullable','exists:companies,id']
        ];
    }
}
