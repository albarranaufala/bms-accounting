<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class CashesReport implements FromView
{
    use Exportable;
    private $cashes, $rangeDate;

    public function __construct($cashes, $rangeDate)
    {
        $this->cashes = $cashes;
        $this->rangeDate = $rangeDate;
    }

    public function view(): View
    {
        return view('cashes.entries.excel.cashes', [
            'cashes' => $this->cashes,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
