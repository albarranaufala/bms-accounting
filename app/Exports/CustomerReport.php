<?php


namespace App\Exports;


use App\Models\Customer;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class CustomerReport implements FromView
{
    use Exportable;
    private $customer, $rangeDate;

    /**
     * CustomerReport constructor.
     * @param Customer $customer
     * @param $rangeDate
     */
    public function __construct(Customer $customer, $rangeDate)
    {
        $this->customer = $customer;
        $this->rangeDate = $rangeDate;
    }


    public function view(): View
    {
        return view('receivables.customers.excel.customer', [
            'customer' => $this->customer,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
