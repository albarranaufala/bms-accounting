<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class DebtsReport implements FromView
{
    use Exportable;
    private $debts, $rangeDate;

    public function __construct($debts, $rangeDate)
    {
        $this->debts = $debts;
        $this->rangeDate = $rangeDate;
    }

    public function view(): View
    {
        return view('debts.journals.excel.debts', [
            'debts' => $this->debts,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
