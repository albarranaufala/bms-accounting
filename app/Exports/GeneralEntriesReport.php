<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class GeneralEntriesReport implements FromView
{
    use Exportable;
    private $transactions, $rangeDate;

    public function __construct($transactions, $rangeDate)
    {
        $this->transactions = $transactions;
        $this->rangeDate = $rangeDate;
    }

    public function view(): View
    {
        return view('financial_statements.excel.general_entries', [
            'transactions' => $this->transactions,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
