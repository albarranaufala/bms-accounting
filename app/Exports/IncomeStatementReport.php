<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class IncomeStatementReport implements FromView
{
    use Exportable;
    private $data, $rangeDate;

    public function __construct($data, $rangeDate)
    {
        $this->data = $data;
        $this->rangeDate = $rangeDate;
    }

    public function view(): View
    {
        return view('financial_statements.excel.income_statement', [
            'data' => $this->data,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
