<?php


namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class CustomersReport implements FromView
{
    use Exportable;
    private $customers, $customerTypes, $rangeDate;

    /**
     * CustomerReport constructor.
     * @param Collection $customers
     * @param Collection $customerTypes
     * @param $rangeDate
     */
    public function __construct(Collection $customers, Collection $customerTypes, $rangeDate)
    {
        $this->customers = $customers;
        $this->rangeDate = $rangeDate;
        $this->customerTypes = $customerTypes;
    }


    public function view(): View
    {
        return view('receivables.customers.excel.customers', [
            'customers' => $this->customers,
            'customerTypes' => $this->customerTypes,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
