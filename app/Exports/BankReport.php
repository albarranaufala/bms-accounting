<?php


namespace App\Exports;


use App\Models\Bank;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class BankReport implements FromView
{
    use Exportable;
    private $bank, $rangeDate;

    public function __construct(Bank $bank, $rangeDate)
    {
        $this->bank = $bank;
        $this->rangeDate = $rangeDate;
    }


    public function view(): View
    {
        return view('cashes.banks.excel.bank', [
            'bank' => $this->bank,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
