<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ReceivablesReport implements FromView
{
    use Exportable;
    private $receivables, $rangeDate;

    public function __construct($receivables, $rangeDate)
    {
        $this->receivables = $receivables;
        $this->rangeDate = $rangeDate;
    }

    public function view(): View
    {
        return view('receivables.journals.excel.receivables', [
            'receivables' => $this->receivables,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
