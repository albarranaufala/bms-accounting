<?php


namespace App\Exports;


use App\Models\Client;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ClientReport implements FromView
{
    use Exportable;
    private $client, $rangeDate;

    public function __construct(Client $client, $rangeDate)
    {
        $this->client = $client;
        $this->rangeDate = $rangeDate;
    }


    public function view(): View
    {
        return view('debts.clients.excel.client', [
            'client' => $this->client,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
