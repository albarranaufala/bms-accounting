<?php


namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ClientsReport implements FromView
{
    use Exportable;
    private $clients, $clientTypes, $rangeDate;

    public function __construct(Collection $clients, Collection $clientTypes, $rangeDate)
    {
        $this->clients = $clients;
        $this->rangeDate = $rangeDate;
        $this->clientTypes = $clientTypes;
    }

    public function view(): View
    {
        return view('debts.clients.excel.clients', [
            'clients' => $this->clients,
            'clientTypes' => $this->clientTypes,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
