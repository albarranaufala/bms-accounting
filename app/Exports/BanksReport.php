<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class BanksReport implements FromView
{
    use Exportable;
    private $banks, $bankTypes, $rangeDate;

    public function __construct(Collection $banks, Collection $bankTypes, $rangeDate)
    {
        $this->banks = $banks;
        $this->rangeDate = $rangeDate;
        $this->bankTypes = $bankTypes;
    }

    public function view(): View
    {
        return view('cashes.banks.excel.banks', [
            'banks' => $this->banks,
            'bankTypes' => $this->bankTypes,
            'rangeDate' => $this->rangeDate,
        ]);
    }
}
