<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes([
    'register' => false
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'role:Divisi Akuntansi|supervisor'])->group(function() {
    Route::resource('financial_statements', 'FinancialStatement\FinancialStatementController')
        ->only(['index', 'show']);
    Route::get('financial_statements/general_entries/export/excel', 'FinancialStatement\FinancialStatementController@exportGeneralEntriesExcel')->name('financial_statements.general_entries.export.excel');
    Route::get('financial_statements/balance_sheet/export/excel', 'FinancialStatement\FinancialStatementController@exportBalanceSheetExcel')->name('financial_statements.balance_sheet.export.excel');
    Route::get('financial_statements/income_statement/export/excel', 'FinancialStatement\FinancialStatementController@exportIncomeStatementExcel')->name('financial_statements.income_statement.export.excel');
});

Route::resource('accounts', 'Account\AccountController')->middleware(['auth', 'role:Divisi Akuntansi|supervisor']);

Route::middleware(['auth', 'role:Divisi Akuntansi|Divisi Piutang|supervisor'])->prefix('receivables')->name('receivables.')->namespace('Receivable')->group( function () {
    Route::resource('journals', 'ReceivableController')->except(['show']);
    Route::get('journals/export/excel', 'ReceivableController@exportExcel')->name('journals.index.export.excel');
    Route::resource('customers', 'CustomerController');
    Route::get('customers/export/excel', 'CustomerController@customersExportExcel')->name('customers.index.export.excel');
    Route::get('customers/{id}/export/excel', 'CustomerController@customerExportExcel')->name('customers.show.export.excel');
    Route::post('customers/types', 'CustomerController@storeType')->name('customers.types.store');
    Route::put('customers/types/{type}', 'CustomerController@updateType')->name('customers.types.update');
});

Route::middleware(['auth', 'role:Divisi Akuntansi|Divisi Hutang|supervisor'])->prefix('debts')->name('debts.')->namespace('Debt')->group(function () {
    Route::resource('journals', 'DebtController')->except(['show']);
    Route::get('journals/export/excel', 'DebtController@exportExcel')->name('journals.index.export.excel');
    Route::resource('clients', 'ClientController');
    Route::get('clients/export/excel', 'ClientController@clientsExportExcel')->name('clients.index.export.excel');
    Route::get('clients/{id}/export/excel', 'ClientController@clientExportExcel')->name('clients.show.export.excel');
    Route::post('clients/types', 'ClientController@storeType')->name('clients.types.store');
    Route::put('clients/types/{type}', 'ClientController@updateType')->name('clients.types.update');
});

Route::middleware(['auth', 'role:Divisi Akuntansi|Divisi Kas|supervisor'])->prefix('cashes')->name('cashes.')->namespace('Cash')->group(function () {
    Route::resource('entries', 'CashController')->except(['show']);
    Route::post('entries/create', 'CashController@storeAndCreate')->name('entries.store_and_create');
    Route::get('entries/export/excel', 'CashController@exportExcel')->name('entries.index.export.excel');
    Route::resource('banks', 'BankController');
    Route::get('banks/export/excel', 'BankController@banksExportExcel')->name('banks.index.export.excel');
    Route::get('banks/{id}/export/excel', 'BankController@bankExportExcel')->name('banks.show.export.excel');
    Route::post('banks/types', 'BankController@storeType')->name('banks.types.store');
    Route::put('banks/types/{type}', 'BankController@updateType')->name('banks.types.update');
});

// Route::prefix('roles')->name('roles.')->group(function () {
//     Route::get('/', 'IAM\IAMController@roleIndex')->name('index');
//     Route::post('/', 'IAM\IAMController@roleStore')->name('store');
//     Route::get('/{role}/edit','IAM\IAMController@roleEdit')->name('edit');
//     Route::put('/{role}','IAM\IAMController@roleUpdate')->name('update');
//     Route::delete('/{role}', 'IAM\IAMController@roleDestroy')->name('destroy');
// });

Route::prefix('users')->name('users.')->group(function () {
    Route::get('/', 'IAM\IAMController@userIndex')->name('index')->middleware('role:supervisor');
    Route::post('/', 'IAM\IAMController@userStore')->name('store')->middleware('role:superadmin|supervisor');
    Route::put('/update', 'IAM\IAMController@userUpdate')->name('update')->middleware('role:superadmin|supervisor');
    Route::get('/{user}/edit', 'IAM\IAMController@userEdit')->name('edit')->middleware('role:superadmin|supervisor');
    Route::delete('/{user}', 'IAM\IAMController@userDestroy')->name('destroy')->middleware('role:superadmin|supervisor');

    Route::get('/{user}/setting', 'Auth\ResetPasswordController@config')->name('config')->middleware('auth');
    Route::put('/{user}/update/password','Auth\ResetPasswordController@updatePassword')->name('update.password')->middleware('auth');

    Route::get('/forgot','Auth\ResetPasswordController@forgotPassword')->name('forgot-password')->middleware('guest');
    Route::post('/resetRequest','Auth\ResetPasswordController@passwordResetRequest')->name('reset-request')->middleware('guest');
    Route::post('/{user}/reset', 'Auth\ResetPasswordController@resetPassword')->name('reset-password')->middleware('role:supervisor');
    Route::post('/{user}/reject', 'Auth\ResetPasswordController@rejectResetRequest')->name('reject-request')->middleware('role:supervisor');
});

Route::middleware(['auth', 'role:superadmin'])->prefix('superadmins')->name('superadmins.')->group(function () {
    Route::get('/','IAM\IAMController@superadminIndex')->name('index');
});

//Company Routes

Route::middleware(['auth', 'role:superadmin|supervisor'])->prefix('companies')->name('companies.')->group(function () {
    Route::resource('/','Company\CompanyController')->parameters(['' => 'company']);
});
