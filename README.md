# BMS Accounting

## Framework

- [Laravel 7.x](https://laravel.com/docs/7.x).

## Dependency, Packages, and Libraries

- Spatie
- Bootstrap
- snapappointments/bootstrap-select
- Maatwebsite/Laravel-Excel
- Sb Admin 2
- Datatables
- Chart.JS
- jQuery
- Fontawesome

## Installation

0. Membaca bismillahirrahmanirrahim.
1. Clone Proyek ini.
    `git clone https://gitlab.com/albarranaufala/bms-accounting.git`
2. Jalankan `composer install` untuk menginstall depedency.
3. Copy file `.env.example` menjadi `.env`
4. Konfigurasi isi `.env` seperti:
    - Application key. Jalankan `php artisan key:generate`
    - Database. Sesuaikan `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD`
5. Jalankan `npm install` dan `npm run dev`
6. Jalankan `php artisan migrate` atau `php artisan migrate --seed`
7. Finally, jalankan `php artisan serve` untuk menjalankan website.
8. Tiap ada perubahan di JS atau Style, menjalankan `npm run dev`

## Kombinasi Command Guide
Silakan menjalankan `composer dump-autoload` terlebih dahulu.

### Membuat Model
`php artisan make:model Models/{Nama Model}`

### Membuat Controller
`php artisan make:controller {Nama Folder/Modul}/{Nama Controller}`


### ERROR DOCUMENTATION
solve 'Allowed memory limit' with 'COMPOSER_MEMORY_LIMIT' command prefix

